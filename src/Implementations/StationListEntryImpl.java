package Implementations;

import Interfaces.StationListEntry;

public class StationListEntryImpl implements StationListEntry {

	private String meshPointLocalInterfaceName;
	
	private String meshPointLocalMacAddress;
	private String meshPointRemoteMacAddress;
	
	private String meshPlinkState;
	private long inactiveTime;
	
	private int signalStrengthAvg;
	private int txBitrate;
	private int rxBitrate;
	
	private int rxBytes;
	private int txBytes;
	private int txRetries;
	private int txFailed;
	
	private int meshLLID;
	private int meshPLID;
	
	// TODO: add mesh_power_mode
	
	
	public String getMeshPointLocalInterfaceName() {
		return meshPointLocalInterfaceName;
	}
	
	public void setMeshPointLocalMacAddress(String mac) {
		meshPointLocalMacAddress = mac;
	}
	public String getMeshPointLocalMacAddress() {
		return meshPointLocalMacAddress;
	}
	
	public String getMeshPointRemoteMacAddress() {
		return meshPointRemoteMacAddress;
	}
	public String getMeshPlinkState() {
		return meshPlinkState;
	}
	public long getInactiveTime() {
		return inactiveTime;
	}
	public int getSignalStrengthAvg() {
		return signalStrengthAvg;
	}
	public int getTxBitrate() {
		return txBitrate;
	}
	public int getRxBitrate() {
		return rxBitrate;
	}
	public int getRxBytes() {
		return rxBytes;
	}
	public int getTxBytes() {
		return txBytes;
	}
	public int getTxRetries() {
		return txRetries;
	}
	public int getTxFailed() {
		return txFailed;
	}
	public int getMeshLLID() {
		return meshLLID;
	}
	public int getMeshPLID() {
		return meshPLID;
	}
	
}
