package Implementations;

import Global.*;
import Interfaces.MeshInterface;
import Interfaces.MeshNode;
import Implementations.GetMeshInfo;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MeshInterfaceImpl implements MeshInterface
{
	
	
	/* Public: */
	
	
	public MeshInterfaceImpl(String ifName, String ethIfName, String meshID, MeshNode parent, Integer UPDATE_THREAD_PERIOD)
	{
		myParent = parent;
		
		myExecutor = new ScheduledThreadPoolExecutor(2);
		myExecutor.setRemoveOnCancelPolicy(true);
		
		myIFName = ifName;
		myMeshID = meshID;
		
		myEthIfName = ethIfName;
		
		// set channel on interface
		// TODO: nodes are already initialized on IF1 (Mini-Mesh / ViPMesh)
		myChannel = GlobalDefines.COMMON_CHANNEL;
		
		enableRootMode(false);
		
		if (parent != null)
			myMeshInfo = new GetMeshInfo(ifName, UPDATE_THREAD_PERIOD);
		
		while (true)
		{
			// Wait for mesh connections to establish and retrieve MAC/IP address info from system
			// TODO: this requires that mesh interfaces are already initialized in advance
			// TODO: we could initialize IF2 on a dummy channel (e.g., 36) with valid IP to use a MeshInterface object for it as well
			try
			{
				Scanner scan = ProcessCall.runCommandWithOutput("./iw_mod", GlobalDefines.toolPath, "dev", myIFName, "info");
				String mod;
				while (!(mod=scan.nextLine()).contains("addr"));
				scan.close();
				
				myMAC = mod.trim().substring(4).trim();
				
				// calculate MAC sum
				myMACSum = Util.calculateMACSum(getMAC());
				
				scan = ProcessCall.runCommandWithOutput("./ifconfig", GlobalDefines.toolPath, myIFName);
				while (!(mod=scan.nextLine()).contains("inet"));
				mod = mod.trim();
				
				myIP = mod.substring(mod.indexOf("inet"), mod.indexOf("Bcast")-1).split(":")[1].trim();
				
				
				scan = ProcessCall.runCommandWithOutput("./ifconfig", GlobalDefines.toolPath, myEthIfName);
				while (!(mod=scan.nextLine()).contains("inet"));
				mod = mod.trim();
				
				myEthernetIP = mod.substring(mod.indexOf("inet"), mod.indexOf("Bcast")-1).split(":")[1].trim();
				
				break;
				
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
			}
		}
			
		try {
			Thread.sleep(10000);	// TODO: as cmdline / config parameter
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (parent != null)
		{
			try
			{
				receiveUDPSocket = new DatagramSocket(GlobalDefines.UDP_RECEIVE_PORT, InetAddress.getByName("0.0.0.0"));
				receiveUDPSocket.setBroadcast( true );
				receiveTCPSocket = new ServerSocket(GlobalDefines.TCP_RECEIVE_PORT, 50, InetAddress.getByName(myIP));	// TODO: configurable TCP backlog parameter
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			
			receiverTCP = new Runnable()
			{
				@Override
				public void run()
				{
					while (true)
					{
						try
						{
							final Socket client = receiveTCPSocket.accept();
							
							Runnable run = new Runnable()
							{
								@Override
								public void run()
								{
									try
									{
										Scanner in = new Scanner( client.getInputStream() );
										
										// Handle received command
										myParent.handleReceivedCommand(Util.IPToMAC(client.getInetAddress().getHostAddress()), new String(in.nextLine()));
										
								    	in.close();
								    	client.close();
										
									} catch (IOException e) {
										e.printStackTrace();
									}
								}
							};
							
							myExecutor.schedule(run, 0, TimeUnit.MILLISECONDS);
							
						} catch ( IOException e ) {
							e.printStackTrace();
						}
					}
				}
			};
			
			receiverUDP = new Runnable()
			{
				@Override
				public void run()
				{
				    while (true)
				    {
			    		// wait for incoming request	
						try
						{
							final DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);	// TODO: configurable UDP receive buffer size
							
							receiveUDPSocket.receive( packet );
							
							// ignore packets that we sent ourselves
							if(!packet.getAddress().getHostAddress().equals(myIP))
							{
								Runnable run = new Runnable()
								{
									@Override
									public void run()
									{	
										// Handle received command
										myParent.handleReceivedCommand(Util.IPToMAC(packet.getAddress().getHostAddress()),
																	   new String(packet.getData()));
									};
								};
								
								myExecutor.schedule(run, 0, TimeUnit.MILLISECONDS);
							}
							
						} catch (IOException e) {
							receiveUDPSocket.close();
							e.printStackTrace();
						}
				    }
				}
			};
			
			new Thread(receiverTCP).start();
			new Thread(receiverUDP).start();
		}
	}
	
	
	public void setChannel(int channel)
	{
		// TODO: prevent configuration of unsupported channels
//		if(GlobalDefines.CHANNEL_SEQUENCE.contains(channel))
//		{
			// Set channel on interface
			changeMeshParameter(GlobalDefines.CHANNEL_COMMAND, String.valueOf(channel));
			myChannel = channel;
//		}
//		else
//		{
//			System.out.println(Util.getCurrentTime() + ": Wrong channel assignment with value: %d", channel);
//		}
	}
	
	
	public int getChannel() {
		return myChannel;
	}
	
	public String getIFName() {
		return myIFName;
	}
	
	public String getMeshID() {
		return myMeshID;
	}
	
	public String getMAC() {
		return myMAC;
	}
	
	public long getMACSum() {
		return myMACSum;
	}
	
	public String getIP() {
		return myIP;
	}
	
	public int getNeighbourCount() {
		return getMeshPeers().size();
	}
	
	public List<List<String>> getMeshPeers() {
		return new ArrayList<List<String>>(myMeshInfo.getMeshPeers());
	}
	
	public List<List<String>> getMeshPaths() {
		return new ArrayList<List<String>>(myMeshInfo.getMeshPaths());
	}
	
	
	// TODO: let this function return a boolean (false in error case)
	public void enableRootMode(boolean enable) {
		changeMeshParameter(GlobalDefines.ROOTMODE_COMMAND, enable ? (String.valueOf(GlobalDefines.HWMP_ROOTMODE)) : ("0"));
	}
	
	
	public void unicast(String destMAC, String command, List<String> data)
	{
		Socket sendTCPSocket = null;
		System.out.println("My MeshIP is: " + myIP);
		
		try
		{
			try {
				sendTCPSocket = new Socket(
						InetAddress.getByName(Util.MACToIP(destMAC)),
						GlobalDefines.TCP_RECEIVE_PORT,
						InetAddress.getByName(myIP),
						GlobalDefines.TCP_SEND_PORT);
					
				PrintWriter out = new PrintWriter(sendTCPSocket.getOutputStream(), true);
				
				String message = new String(command);
				message = message.concat(GlobalDefines.MESSAGE_SEPARATOR);
				
				if (data != null)
				{
					for (String s: data) {
						message = message.concat(s);
						message = message.concat(GlobalDefines.MESSAGE_SEPARATOR);
					}
				}
				
				out.println(message); // send to stream
				
				System.out.println(Util.getCurrentTime() + ": Sent " + message + " to " + Util.MACToName(destMAC));
				
		         if (sendTCPSocket != null)
		        	 sendTCPSocket.close();
	         
			} catch (ConnectException e) {
				System.out.println("Message to " + destMAC + " can not be sent (Connection refused)");
			}
		} 
		catch (IOException e)
		{
		    String message = new String(command);
			message = message.concat(GlobalDefines.MESSAGE_SEPARATOR);
			
			if (data != null)
			{
				for (String s: data) {
					message = message.concat(s);
					message = message.concat(GlobalDefines.MESSAGE_SEPARATOR);
				}
			}
			
		    System.out.println(Util.getCurrentTime() + ": Caught IOException. Tried sending " + message + " to " + Util.MACToName(destMAC));
			e.printStackTrace();
		}
	}
	
	
	public void unicastEthernet(String destMAC, String command, List<String> data)
	{
		Socket sendTCPEthernetSocket = null;
		System.out.println("My EthernetIP is: " + myEthernetIP);
		
		try
		{
			sendTCPEthernetSocket = new Socket(
					InetAddress.getByName(Util.MACToIP(destMAC)),
					GlobalDefines.TCP_ETHERNET_RECEIVE_PORT,
					InetAddress.getByName(myEthernetIP),
					GlobalDefines.TCP_ETHERNET_SEND_PORT);
			
			PrintWriter out = new PrintWriter(sendTCPEthernetSocket.getOutputStream(), true);
			
			String message = new String(command);
			
			out.println(message); // send to stream
			
			System.out.println(Util.getCurrentTime() + ": Sent " + message + " via Ethernet to  " + Util.MACToName(destMAC));
			
	         if (sendTCPEthernetSocket != null)
	        	 sendTCPEthernetSocket.close();
		} 
		catch (IOException e)
		{
		    String message = new String(command);
			
			if (data != null)
			{
				for (String s: data) {
					message = message.concat(s);
					message = message.concat(GlobalDefines.MESSAGE_SEPARATOR);
				}
			}
			
		    System.out.println(Util.getCurrentTime() + ": Caught IOException. Tried sending " + message + " via Ethernet to " + Util.MACToName(destMAC));
			e.printStackTrace();
		}
	}
	
	
	public void broadcast(String message)
	{
		InetAddress inetAddress = null;
		
		try {
			inetAddress = InetAddress.getByName(new String(GlobalDefines.IF1_IPBODY+"255"));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		DatagramPacket packet = new DatagramPacket( message.getBytes(), message.length(),
													inetAddress, GlobalDefines.UDP_RECEIVE_PORT);
		
		try
		{
			DatagramSocket sendUDPSocket = new DatagramSocket(GlobalDefines.UDP_SEND_PORT, InetAddress.getByName(myIP));
			sendUDPSocket.setBroadcast(true);
			sendUDPSocket.send( packet );
			sendUDPSocket.close();
			
			System.out.println(Util.getCurrentTime() + ": Broadcast " + message);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
//	public void unicastUDP(String destMAC, String command, List<String> data)
//	{
//		InetAddress inetAddress = null;
//		
//		try {
//			inetAddress = InetAddress.getByName(new String(GlobalDefines.IF1_IPBODY+"255"));
//		} catch (UnknownHostException e) {
//			e.printStackTrace();
//		}
//		
//		DatagramPacket packet = new DatagramPacket( message.getBytes(), message.length(),
//													inetAddress, GlobalDefines.UDP_RECEIVE_PORT);
//		
//		try
//		{
//			DatagramSocket sendUDPSocket = new DatagramSocket(GlobalDefines.UDP_SEND_PORT, InetAddress.getByName(myIP));
//			sendUDPSocket.setBroadcast(true);
//			sendUDPSocket.send( packet );
//			sendUDPSocket.close();
//			
//			System.out.println(Util.getCurrentTime() + ": Broadcast " + message);
//			
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
	
	
	public void broadcastEthernet(String message)
	{
		InetAddress inetAddress = null;
		
		try {
			inetAddress = InetAddress.getByName(new String(GlobalDefines.IF1_ETH_IPBODY+"255"));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		DatagramPacket packet = new DatagramPacket( message.getBytes(), message.length(),
													inetAddress, GlobalDefines.UDP_RECEIVE_PORT);
		
		try
		{
			DatagramSocket sendUDPSocket = new DatagramSocket(GlobalDefines.UDP_SEND_PORT, InetAddress.getByName(myEthernetIP));
			sendUDPSocket.setBroadcast(true);
			sendUDPSocket.send( packet );
			sendUDPSocket.close();
			
			System.out.println(Util.getCurrentTime() + ": Ethernet Broadcast " + message);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/* Private: */
	
	
	private void changeMeshParameter(String parameter, String value)
	{
		switch(parameter)
		{
			case GlobalDefines.CHANNEL_COMMAND:
			{
				/* apply channel switch command sequence
				 * 
				 * -> (re-)apply TX power (14dBm IF1/Atheros, XXdBm IF2/Ralink)
				 * -> set HT20 channel (resolve to freq) and fixed MCS/mcast-/basic-rate
				 * -> (re-)apply mesh parameters
				 * 
				 * -> (re-)apply IP address configuration
				 * (if1Name == if2Name) -> only IF1 -> IF1 will stay on 192.168.123.X ("myIP")
				 * (if1Name != if2Name) -> IF1+IF2	-> IF1 will stay on 192.168.123.X ("myIP"), IF2 is initialized externally
				 */
				
				/* extract from meshnodeinit:
				 * 
				 * (iw phy ${PHYNAME_ATH} set rts 500)
				 * (iw dev ${DEVNAME_ATH} set power_save off)
				 * 
				 * iw dev ${DEVNAME_ATH} set txpower fixed 1400
				 * 
				 * iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT20 basic-rates 24 mcast-rate 24
				 * 
				 * iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 3 lgi-5
				 * 
				 * iw dev ${DEVNAME_ATH} set mesh_param mesh_max_peer_links=50
				 * iw dev ${DEVNAME_ATH} set mesh_param mesh_plink_timeout=20
				 * 
				 * ifconfig ${DEVNAME_ATH} 192.168.123.X netmask 255.255.255.0 broadcast 192.168.123.255
				 */
				
				int channelFreq = Util.channel2Freq(Integer.parseInt(value));
				
				// leave previous mesh
				ProcessCall.runCommand("./iw", GlobalDefines.toolPath,
						"dev", myIFName, "mesh", "leave");
				
				// TODO: configurable params for power, rate, ...
				
				// set tx power
				// TODO: not necessary, power is kept between leave and join
				// TODO: ralink chipset would need tx power 1dBm (value 100)
//				ProcessCall.runCommand("./iw", GlobalDefines.toolPath,
//						"dev", myIFName, "set", "txpower", "fixed", "1400");
				
				// join mesh "myMeshID" on new channel freq with fixed basic/mcast rate
				// TODO: MCS0 vs. MCS3 -> use rate that is achievable both by Atheros mPCIe and Ralink USB
				ProcessCall.runCommand("./iw", GlobalDefines.toolPath,
						"dev", myIFName, "mesh", "join", myMeshID, "freq", String.valueOf(channelFreq),
						"HT20", "basic-rates", "6", "mcast-rate", "6");
//				ProcessCall.runCommand("./iw", GlobalDefines.toolPath,
//						"dev", myIFName, "mesh", "join", myMeshID, "freq", String.valueOf(channelFreq),
//						"HT20", "basic-rates", "24", "mcast-rate", "24");
				
				// set fixed data frame rate
				// TODO: MCS0 vs. MCS3 -> use rate that is achievable both by Atheros mPCIe and Ralink USB
				// TODO: ralink chipset would ignore LGI and use SGI instead
				ProcessCall.runCommand("./iw", GlobalDefines.toolPath,
						"dev", myIFName, "set", "bitrates", "ht-mcs-5", "0", "lgi-5");
//				ProcessCall.runCommand("./iw", GlobalDefines.toolPath,
//						"dev", myIFName, "set", "bitrates", "ht-mcs-5", "3", "lgi-5");
				
				// set IP address configuration
				// TODO: derive broadcast address from "myIP"
				ProcessCall.runCommand("./ifconfig", GlobalDefines.toolPath,
						myIFName, myIP, "netmask", "255.255.255.0", "broadcast", new String(GlobalDefines.IF1_IPBODY+"255"));
			}
			break;
			
			case GlobalDefines.ROOTMODE_COMMAND:
			{
				ProcessCall.runCommand("./iw", GlobalDefines.toolPath,
						"dev", myIFName, "set", "mesh_param", "mesh_hwmp_rootmode", value);
			}
			break;
			
			default: { }
			break;
		}
	}
	
	
	private MeshNode myParent;
	
	private ScheduledThreadPoolExecutor myExecutor;
	
	private int myChannel;
	
	private String myIFName;
	private String myMeshID;
	
	private String myEthIfName;
	
	private String myMAC;
	private long myMACSum;
	private String myIP;
	private String myEthernetIP;
	
	private GetMeshInfo myMeshInfo;
	
	private DatagramSocket receiveUDPSocket;
	private ServerSocket receiveTCPSocket; 
	
	private Runnable receiverTCP;
	private Runnable receiverUDP;
	
}
