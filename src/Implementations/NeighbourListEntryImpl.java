package Implementations;

import java.util.ArrayList;
import java.util.List;

import Global.GlobalDefines;
import Interfaces.NeighbourListEntry;

public class NeighbourListEntryImpl implements NeighbourListEntry
{

	/* Public: */
	
	public NeighbourListEntryImpl(List<String> meshPeer)
	{
		myMeshPeer = new ArrayList<String>(meshPeer);

		myMeshPeer.add("00:00:00:00:00:00");		// dummy CH MAC (cluster ID)
		myMeshPeer.add(GlobalDefines.CLUSTERFREE);	// initial membership CF
		myMeshPeer.add(String.valueOf(0));			// dummy PLC
		myMeshPeer.add(String.valueOf(0.0));		// dummy NPR
	}
	
	public String getMAC() {
		return myMeshPeer.get(2);
	}
	
	public long getMACSum() {
		long ret = 0;
		String[] splitString = myMeshPeer.get(2).split(":");
		
		for(int i = 0;i < splitString.length;i++) {
			ret += ((long)Integer.parseInt("+" + splitString[i].replaceAll("\\s+", ""), 16))<<((splitString.length - i - 1)*8);
		}
		
		return ret;
	}
	
	public void setClusterheadMAC(String clusterheadMAC) {
		myMeshPeer.set(7, clusterheadMAC);
	}
	
	public String getClusterheadMAC() {
		return myMeshPeer.get(7);
	}
	
	public void setMembership(String membership) {
		myMeshPeer.set(8, membership);
	}
	
	public String getMembership() {
		return myMeshPeer.get(8);
	}
	
	public void setNeighbourCount(int nc) {
		myMeshPeer.set(9, String.valueOf(nc));
	}
	
	public int getNeighbourCount() {
		return Integer.parseInt(myMeshPeer.get(9));
	}
	
	public void setWNPR(float wnpr) {
		myMeshPeer.set(10, Float.toString(wnpr));
	}
	
	public float getWNPR() {
		return Float.parseFloat(myMeshPeer.get(10));
	}
	
	
	/* Private: */
	
	private List<String> myMeshPeer;
	
}
