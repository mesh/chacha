package Implementations;

import Global.*;
import Interfaces.MeshInterface;
import Interfaces.MeshNode;
import Interfaces.NeighbourListEntry;

import java.util.*;
import java.util.concurrent.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.InvalidParameterException;


public class MeshNodeImpl implements MeshNode
{

	/* Public: */

	public MeshNodeImpl(String[] args) throws InvalidParameterException
	{
		// delete any previous "done" files -> needed for test automation
		File doneFile = new File(GlobalDefines.DONE_FILE_PATH + "clustering.done");
		doneFile.delete();
		doneFile = new File(GlobalDefines.DONE_FILE_PATH + "monitoring.done");
		doneFile.delete();
		
		
		// check for arguments, require at least <IF1Name,IF1meshID,IF2Name,IF2meshID>
		if ((args == null) || (args.length < 4))
			throw new InvalidParameterException("Not enough IF parameters!");

		System.out.print(Util.getCurrentTime() + ": args: ");
		for (String arg : args)
			System.out.print(arg + " ");

		System.out.println("");
		
		
		// set default parameter values
		
		SKIP_SETCHANNEL = GlobalDefines.SKIP_SETCHANNEL;
		SKIP_MONITORING = GlobalDefines.SKIP_MONITORING;
		
		UPDATE_THREAD_PERIOD = GlobalDefines.UPDATE_THREAD_PERIOD;
		
//		NEIGH_INF_THREAD_SLEEP = GlobalDefines.NEIGH_INF_THREAD_SLEEP;
		
		ROAMING_THREAD_SLEEP = GlobalDefines.ROAMING_THREAD_SLEEP;
		
		SYNC_BROADCAST_PERIOD = GlobalDefines.SYNC_BROADCAST_PERIOD;
		SYNC_THRESHOLD = GlobalDefines.SYNC_THRESHOLD;
		
		NC_UNICAST_PERIOD = GlobalDefines.NC_UNICAST_PERIOD;
		
		MASTER_PHASE_BROADCAST_PERIOD = GlobalDefines.MASTER_PHASE_BROADCAST_PERIOD;
		MASTER_PHASE_BROADCAST_TRIES = GlobalDefines.MASTER_PHASE_BROADCAST_TRIES;
		
		CH_BROADCAST_PERIOD = GlobalDefines.CH_BROADCAST_PERIOD;
		CH_NOTIFICATION_TIMEOUT = GlobalDefines.CH_NOTIFICATION_TIMEOUT;
		
//		PHASE0_DELAY = GlobalDefines.PHASE0_DELAY;		// TODO: currently unused
		PHASE1_DELAY = GlobalDefines.PHASE1_DELAY;
		PHASE2_DELAY = GlobalDefines.PHASE2_DELAY;
		PHASE3_DELAY = GlobalDefines.PHASE3_DELAY;
		PHASE4_DELAY = GlobalDefines.PHASE4_DELAY;
		PHASE5_DELAY = GlobalDefines.PHASE5_DELAY;
		PHASE6_DELAY = GlobalDefines.PHASE6_DELAY;
//		PHASE7_DELAY = GlobalDefines.PHASE7_DELAY;		// TODO: currently unused
		
		MONITORING_DELAY = GlobalDefines.MONITORING_DELAY;
		ROAMING_DELAY = GlobalDefines.ROAMING_DELAY;
		
		
		// read optional args (coming after <IF1Name,IF1meshID,EthernetIFName,IF2Name,IF2meshID>)
		List<String> optionalArgs = new ArrayList<String>(Arrays.asList(args));
		for (int i = 0; i < 5; i++)
			optionalArgs.remove(0);
		
		// handle optional argument values
		for (String arg : optionalArgs)
		{
			String[] cmd = arg.split("=");

			try
			{
				switch (cmd[0])
				{
					case "SKIP_SETCHANNEL": {
						SKIP_SETCHANNEL = Boolean.parseBoolean(cmd[1]);
					}
					break;
					
					case "SKIP_MONITORING": {
						SKIP_MONITORING = Boolean.parseBoolean(cmd[1]);
					}
					break;
					
					case "UPDATE_THREAD_PERIOD": {
						UPDATE_THREAD_PERIOD = Integer.parseInt(cmd[1]);
					}
					break;
				
					case "INITIAL_THREAD_SLEEP": {
//						INITIAL_THREAD_SLEEP = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "WAIT_MESHNODE_IS_FREE": {
//						WAIT_MESHNODE_IS_FREE = Integer.parseInt(cmd[1]);
					}
					break;
					
//					case "NEIGH_INF_THREAD_SLEEP": {
//						NEIGH_INF_THREAD_SLEEP = Integer.parseInt(cmd[1]);
//					}
					
					case "SYNC_BROADCAST_PERIOD": {
						SYNC_BROADCAST_PERIOD = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "ROAMING_THREAD_SLEEP": {
						ROAMING_THREAD_SLEEP = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "SYNC_THRESHOLD": {
						SYNC_THRESHOLD = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "NC_UNICAST_PERIOD": {
						NC_UNICAST_PERIOD = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "CH_BROADCAST_PERIOD": {
						CH_BROADCAST_PERIOD = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "CH_NOTIFICATION_TIMEOUT": {
						CH_NOTIFICATION_TIMEOUT = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "MASTER_PHASE_BROADCAST_PERIOD": {
						MASTER_PHASE_BROADCAST_PERIOD = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "MASTER_PHASE_BROADCAST_TRIES": {
						MASTER_PHASE_BROADCAST_TRIES = Integer.parseInt(cmd[1]);
					}
					break;
					
//					case "PHASE0_DELAY": {
//						PHASE0_DELAY = Integer.parseInt(cmd[1]);	// TODO: currently unused
//					}
//					break;
					
					case "PHASE1_DELAY": {
						PHASE1_DELAY = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "PHASE2_DELAY": {
						PHASE2_DELAY = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "PHASE3_DELAY": {
						PHASE3_DELAY = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "PHASE4_DELAY": {
						PHASE4_DELAY = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "PHASE5_DELAY": {
						PHASE5_DELAY = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "PHASE6_DELAY": {
						PHASE6_DELAY = Integer.parseInt(cmd[1]);	// TODO: currently unused
					}
					break;
					
//					case "PHASE7_DELAY": {
//						PHASE7_DELAY = Integer.parseInt(cmd[1]);	// TODO: currently unused
//					}
//					break;
					
					case "MONITORING_DELAY": {
						MONITORING_DELAY = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "ROAMING_DELAY": {
						ROAMING_DELAY = Integer.parseInt(cmd[1]);
					}
					break;
						
					default: {
						System.out.println("Wrong argument: " + arg);
					}
					break;
				}
				
			} catch (NumberFormatException e) {
				System.out.println("Wrong argument: " + arg);
			}
		}
		
		
		// initialize context
		//
		virgin = true;
		//
		startTime = 0;
		doneTime = 0;
		//
		myLocalPhase = 0;
		//
		isSyncRunning = false;
		isLeavingPossible = true;
		isJoiningPossible = true;
		blockSetToCF = false;
		
		blockNextHopList = false;
		
		iAmIsolated = false;
		
		mySyncCounter = 0;
		//
		myMembership = GlobalDefines.CLUSTERFREE;
		//
		myCentrality = 0;
		//
		myNodeQuantity = 0;
		//
//		myClusterLoad = 0;
		//
		lastLeave = 0;
		//
		myMasterMAC = null;
		myMasterCentrality = 0;
		//
		myClusterheadMAC = null;
		myNextHopToClusterHead = null;
		ackCH = "";
		//
		// TODO: how should cluster IF channel be initialized?
		// -> could be that CM join a CH before it has completed channel selection in phases 5/6
		// -> the CH should still report a valid channel to the joining CM and override it later in/after phase 6
		// -> initialize with COMMON_CHANNEL for now
		myClusterChannel = GlobalDefines.COMMON_CHANNEL;	// TODO: get rid of redundant variable "myClusterChannel"
		//
		masterCHList = new ArrayList<String>();
		masterCHChannelList = new ArrayList<String>();
		myCHList = new ArrayList<String>();
		myCMList = new ArrayList<String>();
		myClusteringDoneList = new ArrayList<String>();
		
		globalClusterInfo = new ArrayList<ArrayList<String>>();
		myNeighboursNextHopToCH = new ArrayList<List<String>>();
		clusterheadBlackList = new ArrayList<ArrayList<String>>();
		AllClusterChannel = new ArrayList<String>();
		globalClusterTimeStamps = new ArrayList<Long>();

		expectedPongList = new ArrayList<ArrayList<String>>();
		
		
		myTimer = null;	// will be initialized later
//		roamingTimer = null; //will only be used if roaming at the end of phase 7 is used
		
//		bewareOfBackRoamingWhileBalancingTimeStamp = System.currentTimeMillis();
		
		handleSyncMon = new Object();
//		waitForCHMon = new Object();
		
		
		// construct IF1 MeshInterface object
		String myIF1Name = new String(args[0]);
		String myIF1MeshID = new String(args[1]);
		String myEthIfName = new String(args[2]);
//		myIF1Channel = GlobalDefines.COMMON_CHANNEL;
		myIF1 = new MeshInterfaceImpl(myIF1Name,myEthIfName, myIF1MeshID, this, UPDATE_THREAD_PERIOD); // TODO: also pass channel to MeshInterface constructor
		
		
		// init IF1 peer link / neighbour info
		List<List<String>> meshPeers = myIF1.getMeshPeers();
		int neighbourCount = myIF1.getNeighbourCount();
		myNeighbours = new Vector<NeighbourListEntry>(neighbourCount);
		for (int i = 0; i < neighbourCount; i++) {
			myNeighbours.add(new NeighbourListEntryImpl(meshPeers.get(i)));
		}
		
		
		// TODO: construct IF2 MeshInterface object
//		myIF2Name = new String(args[3]);
//		myIF2MeshID = new String(args[4]);
//		myIF2Channel = GlobalDefines.COMMON_CHANNEL;
//		myIF2 = null;
//		if (!myIF1Name.equals(myIF2Name)) {
//			myIF2 = new MeshInterfaceImpl(myIF2Name, myIF2MeshID, this); // TODO: also pass channel to MeshInterface constructor
//		}
		
		
		
		// TIMINFO: 000 ThreadPoolExecutor
		// ThreadPoolExecutor for threads that are started later during clustering phases
		// -> syncBroadcast, neighbourCountUnicast, masterPhaseBroadcast, clusterHeadBroadcast, webclientThread / webserverThread, roamingBalancing
		myExecutor = new ScheduledThreadPoolExecutor(1); // TODO: set adequate pool size
		myExecutor.setRemoveOnCancelPolicy(true);
		
		
		
		// start background thread that updates neighbour node info (IF1 peer links)
		// -> initial entries obtained from system via GetMeshInfo thread (iw wrapper)
		// -> copied by this thread to Vector<NeighbourListEntry> myNeighbours that will hold extended info
		// -> extended info of neighbour entries will be updated on reception of messages
		neighborListUpdate = new Runnable()
		{
			// TIMDRAFT: Update neighbour info
			@Override
			public void run()
			{
				try
				{
					synchronized (myNeighbours)
					{
						System.out.println(Util.getCurrentTime() + ": Check for new neighbours");
						
						List<List<String>> meshPeers = myIF1.getMeshPeers();
						List<List<String>> MeshPathCopy = new ArrayList<List<String>>();
						
						if( !myIF1.getMeshPaths().isEmpty() )
						{
							MeshPathCopy.addAll(myIF1.getMeshPaths());
						}
						
						int neighbourCount = myIF1.getNeighbourCount();
						
						
						
		    			for (int i = 0; i < neighbourCount; i++)
		    			{
		    				boolean isInList = false;
		    
		    				for (NeighbourListEntry nle : myNeighbours)
		    				{
		    					if (nle.getMAC().equals(meshPeers.get(i).get(2))) {
		    						isInList = true;
		    						System.out.println(Util.getCurrentTime() + ": " + Util.MACToName(meshPeers.get(i).get(2)) + " already in neighbour list");
		    						break;
		    					}
		    				}
		    				if (!isInList) {
		    					System.out.println(Util.getCurrentTime() + ": add " + Util.MACToName(meshPeers.get(i).get(2)) + " to neighbour list");
		    					myNeighbours.add(new NeighbourListEntryImpl(meshPeers.get(i)));
		    				}
		    			}
		    			
		    			System.out.println(Util.getCurrentTime() + ": Check for obsolete neighbours");
		    			
		    			Vector<NeighbourListEntry> toBeRemoved = new Vector<NeighbourListEntry>();
		    			for (NeighbourListEntry nle : myNeighbours)
		    			{
		    				boolean isInList = false;
	    					
	    					for (int i = 0; i < neighbourCount; i++)
	    					{
	    						if (nle.getMAC().equals(meshPeers.get(i).get(2))) {
		    						isInList = true;
		    						break;
		    					}
	    	    			}
	    					
	    					if (!isInList) {
	    						toBeRemoved.add(nle);
	    					}
	    				}
		    			for (NeighbourListEntry nle : toBeRemoved)
		    			{
		    				System.out.println(Util.getCurrentTime() + ": remove obsolete " + Util.MACToName(nle.getMAC()) + " from neighbour list");
		    				myNeighbours.remove(nle);
		    			}
		    			
		    			
		    			System.out.println(Util.getCurrentTime() + ": I have " + Integer.toString(myNeighbours.size()) + " neighbours");
		    			System.out.println(Util.getCurrentTime() + ": My IF has " + Integer.toString(neighbourCount) + " connections");
		    			
		    			for( int i = 0; i < myNeighbours.size(); i++ )
		    			{
		    				System.out.println(Util.getCurrentTime() + " Neighbor " + Integer.toString(i) + " is " + Util.MACToName(myNeighbours.get(i).getMAC()) );
		    			}
		    			
		    			String NextHopMessage = "";
						
						if( myClusterheadMAC != null )
						{
							for( int i = 0; i < MeshPathCopy.size(); i++ )
							{
								if( MeshPathCopy.get(i).get(2).equals(myClusterheadMAC) )
								{
									myNextHopToClusterHead = MeshPathCopy.get(i).get(3);
								}
							}
							
							
							
							if( myNextHopToClusterHead != null )
							{
								NextHopMessage = GlobalDefines.MY_NEXT_HOP_TO_CH + GlobalDefines.MESSAGE_SEPARATOR + myNextHopToClusterHead;
								
								if( GlobalDefines.SEND_CHINFO_TO_TEST_PC )
								{
//										myIF1.unicastEthernet(Global.GlobalDefines.TEST_PC_ETH_MAC, NextHopMessage + GlobalDefines.MESSAGE_SEPARATOR, null);
								}
								
								for (NeighbourListEntry nle : myNeighbours)
			    				{
									myIF1.unicast(nle.getMAC(), NextHopMessage, null);
			    				}
								
							
							
								if( myIF1.getMAC().equals(GlobalDefines.EXP_NODE) )
								{
									System.out.println(Util.getCurrentTime() + ": Sending my NH2CH to the test PC");
									NextHopMessage = GlobalDefines.GAL40_NEXT_HOP_TO_CH + GlobalDefines.MESSAGE_SEPARATOR + myNextHopToClusterHead;
									myIF1.unicastEthernet(GlobalDefines.TEST_PC_ETH_MAC, NextHopMessage, null);
									
//										sendMyNH2CHToTestPC();
								}
							}
						}
					}
			    
				} catch(ConcurrentModificationException e) {
					e.printStackTrace();
				}
			}
		};
		
		neighborListUpdateFuture = myExecutor.scheduleAtFixedRate(neighborListUpdate, 0, NEIGH_INF_THREAD_SLEEP, TimeUnit.MILLISECONDS);
		
		
		
		// TIMDRAFT: ROAMING AND BALANCING
		roamingBalancing = new Runnable()
		{
			// new functionality: 1.recognize Isolation 2.recognize rural area (decision regarding CH-construction) 3.recognize overstaffed area (decision regarding CH-destruction)
			// for 2./3. maybe a separate thread (CHevolution)
			
			@Override
			public void run()
			{
				if( myMembership.equals(GlobalDefines.CLUSTERFREE) )
				{
					amIFree = true;
					myIF1.enableRootMode(false);
					
					roam();
				}
				
//				System.out.println("This is a output of the new RoamingBalancingThread");
//				System.out.println(" This are the CH's: ");
				System.out.println(Util.getCurrentTime() + ": Executing Roaming - Thread");
				
//						for( int i = 0; i < myNeighbours.size(); i++)
//						{
//							System.out.println("my Neighbour " + Integer.toString(i+1) + " is: " + myNeighbours.get(i).getMAC());
//						}
				
//						System.out.println(Util.getCurrentTime() + ": checking TimeStamps");
//						checkTimeStampsAndRemoveObsolete();
				
//						System.out.println(Util.getCurrentTime() + ": checking Neighbors");
//						checkNeighbors();
				
				System.out.println(Util.getCurrentTime() + ": checking NH2CH-List and delete obsolete");
				checkNextHopListAndDeleteObsolete();
				
//						System.out.println(Util.getCurrentTime() + ": checking Expected-Pong-List");
//						checkPongList();
//						
//						if( myMembership.equals(GlobalDefines.CLUSTERMEMBER) )
//						{
//							System.out.println(Util.getCurrentTime() + ": checking the existens of my CH");
//							isMyCHStillThere();
//						}

//						if( myMembership.equals(GlobalDefines.CLUSTERHEAD) )
//						{
//							areMyCMStillThere();
//						}
				
				for( int i = 0; i < AllClusterChannel.size(); i++ )
				{
					System.out.println("Tim: Channel of Cluster " + Integer.toString(i+1) + " is " + AllClusterChannel.get(i));
				}
				
				if( roamingPossible )
				{
					System.out.println("Try to Balance!!!");
//					plotClusterInfo();
					performBalancing();
				}
				
				if( roamingPossible )
				{
					if( myMembership.equals(GlobalDefines.CLUSTERMEMBER) )
					{
						roamIfIsolated();
					}
				}
				
//						try
//						{
//							Thread.sleep(ROAMING_THREAD_SLEEP);
//							
//						} catch(InterruptedException e) {
//								e.printStackTrace();						
//						}
				}
		};
		
		
		
		// TIMINFO: 010 Runnable syncBroadcast
		syncBroadcast = new Runnable()
		{
			@Override
			public void run()
			{
				if (!isSyncRunning) {
					syncBroadcastFuture.cancel(true);
					return;
				}
				
				// TODO: getting myNodeQuantity only once during SYNC phase hile root mode is active
				if (myNodeQuantity == 0)
					myNodeQuantity = getNodeQuantity();
				
				// TODO: calculate myCentrality only once during SYNC phase while root mode is active
				if (myCentrality == 0)
					myCentrality = calculateCentrality();
				
				myIF1.broadcast(GlobalDefines.SYNC_MESSAGE + GlobalDefines.MESSAGE_SEPARATOR +
								Double.toString((myCentrality)) + GlobalDefines.MESSAGE_SEPARATOR);
				
				mySyncCounter++;
				
				if (mySyncCounter > SYNC_THRESHOLD)
				{
					// last node in the race -> cancel SYNC broadcasts and set to master CH
					
					System.out.println(Util.getCurrentTime() + ": I'm the master CH now!");
					
					syncBroadcastFuture.cancel(true);
					mySyncCounter = 0;
					isSyncRunning = false;
					
					setToMCH();
					
					// start UDP broadcast of phase messages
					masterPhaseBroadcastFuture = myExecutor.schedule(masterPhaseBroadcast, PHASE1_DELAY, TimeUnit.MILLISECONDS);
				}
			}
		};
		
		// TIMINFO: 020 Runnable neighbourCountUnicast
		neighbourCountUnicast = new Runnable()
		{
			@Override
			public void run()
			{
				synchronized (myNeighbours)
				{
					for (NeighbourListEntry nle : myNeighbours)
					{
						int myNC = myIF1.getNeighbourCount();
						List<String> data = new ArrayList<String>(1);
						data.add(Integer.toString(myNC));
						myIF1.unicast(nle.getMAC(), GlobalDefines.NC_MESSAGE, data);
					}
				}
			}
		};
		
		// TIMINFO: 030 Runnable masterPhaseBroadcast
		masterPhaseBroadcast = new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					System.out.println(Util.getCurrentTime() + ": Master thread. Phase " + String.valueOf(myLocalPhase));

					switch (myLocalPhase)
					{
						case 0: {
							// send MASTER_PHASE_BROADCAST_TRIES times UDP broadcast "PHASE 1"
							for (int i = 0; i < MASTER_PHASE_BROADCAST_TRIES; i++) {
								myIF1.broadcast(GlobalDefines.PHASE1_MESSAGE + GlobalDefines.MESSAGE_SEPARATOR);
								Thread.sleep(MASTER_PHASE_BROADCAST_PERIOD);
							}
						}
						break;
	
						case 1: {
							// send MASTER_PHASE_BROADCAST_TRIES times UDP broadcast "PHASE 2"
							for (int i = 0; i < MASTER_PHASE_BROADCAST_TRIES; i++) {
								myIF1.broadcast(GlobalDefines.PHASE2_MESSAGE + GlobalDefines.MESSAGE_SEPARATOR);
								Thread.sleep(MASTER_PHASE_BROADCAST_PERIOD);
							}
						}
						break;
	
						case 2: {
							// send MASTER_PHASE_BROADCAST_TRIES times UDP broadcast "PHASE 3"
							for (int i = 0; i < MASTER_PHASE_BROADCAST_TRIES; i++) {
								myIF1.broadcast(GlobalDefines.PHASE3_MESSAGE + GlobalDefines.MESSAGE_SEPARATOR);
								Thread.sleep(MASTER_PHASE_BROADCAST_PERIOD);
							}
						}
						break;
	
						case 3: {
							// send MASTER_PHASE_BROADCAST_TRIES times UDP broadcast "PHASE 4"
							for (int i = 0; i < MASTER_PHASE_BROADCAST_TRIES; i++) {
								myIF1.broadcast(GlobalDefines.PHASE4_MESSAGE + GlobalDefines.MESSAGE_SEPARATOR);
								Thread.sleep(MASTER_PHASE_BROADCAST_PERIOD);
							}
						}
						break;
	
						case 4: {
							// send MASTER_PHASE_BROADCAST_TRIES times UDP broadcast "PHASE 5"
							for (int i = 0; i < MASTER_PHASE_BROADCAST_TRIES; i++) {
								myIF1.broadcast(GlobalDefines.PHASE5_MESSAGE + GlobalDefines.MESSAGE_SEPARATOR);
								Thread.sleep(MASTER_PHASE_BROADCAST_PERIOD);
							}
						}
						break;
	
						case 5: {
							// send MASTER_PHASE_BROADCAST_TRIES times UDP broadcast "PHASE 6"
							for (int i = 0; i < MASTER_PHASE_BROADCAST_TRIES; i++) {
								myIF1.broadcast(GlobalDefines.PHASE6_MESSAGE + GlobalDefines.MESSAGE_SEPARATOR);
								Thread.sleep(MASTER_PHASE_BROADCAST_PERIOD);
							}
						}
						break;
						
						// TODO: transition from phase 6 to 7 is done directly without a phase message sent by master
						
						default: {
							throw new Exception("Master thread is still running!");
						}
					}
					
					myLocalPhase++;
					masterPhaseBroadcastFuture = null;
					amIFree = true;
					
				} catch (InterruptedException e) {
					//e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		
		// TIMINFO: 040 Runnable clusterHeadBroadcast
		clusterHeadBroadcast = new Runnable()
		{
			@Override
			public void run()
			{
				// TIMDRAFT: extend CH_MSG
				System.out.println("New CH-Broadcast-Loop");
				long currentTimeStamp = System.currentTimeMillis();
				int ClusterInfoOverwriteRowIndex = -1;
//				myClusterLoad = myCMList.size() + 1;
				ArrayList<String> ClusterInfoTmp = new ArrayList<String>();
				
				for( int i = 0; i < myCMList.size(); i++ )
				{
					System.out.println(Util.getCurrentTime() + ": My CM " + Integer.toString(i) + " is " + Util.MACToName(myCMList.get(i)));
				}
				
				ClusterInfoTmp.add(myIF1.getMAC());
				ClusterInfoTmp.addAll(myCMList);
				
				System.out.println("Schleife starten");
				
				for( int i = 0; i < globalClusterInfo.size();i++)
				{
//					System.out.println(Integer.toString(i));
					
					ArrayList<String> globalClusterInfoRow = new ArrayList<String>(globalClusterInfo.get(i));
					
					if( globalClusterInfoRow.contains(myIF1.getMAC() ) ) 	//ClusterInfoTmp.get(0) is always CH
					{
						ClusterInfoOverwriteRowIndex = i;
					}	
				}
				
				if( ClusterInfoOverwriteRowIndex > -1 )
				{
					System.out.println("globalClusterInfo wird überschrieben");
					globalClusterInfo.set(ClusterInfoOverwriteRowIndex, ClusterInfoTmp);
					AllClusterChannel.set(ClusterInfoOverwriteRowIndex, Integer.toString(myClusterChannel));
					globalClusterTimeStamps.set(ClusterInfoOverwriteRowIndex, currentTimeStamp );

				}
				else
				{
					System.out.println("globalClusterInfo wird erweitert");
					globalClusterInfo.add(ClusterInfoTmp);
					AllClusterChannel.add(Integer.toString(myClusterChannel));
					globalClusterTimeStamps.add( currentTimeStamp );
				}

				int myCMCounter = myCMList.size();
				
				String CHInfoPart1 = "";		//CH|timestamp|channel|
				String CHInfoPart2 = "";		//MAC(i)alpha(i)xxMAC(i+1)alpha(i+1)...xxMAC(n)alpha(n)
				String CHInfoPart3 = "";		//yyChecksum
				String CHInfoMessage = "";		//CHInfoPart1 + CHInfoPart2 + CHInfoPart3
				
				CHInfoPart1 += GlobalDefines.CH_MESSAGE;
//				CHInfoPart1 += GlobalDefines.MESSAGE_SEPARATOR;
//				CHInfoPart1 += Integer.toString(Util.getTimeStampInSeconds());
				CHInfoPart1 += GlobalDefines.MESSAGE_SEPARATOR;
				CHInfoPart1 += Integer.toString(myClusterChannel);
				CHInfoPart1 += GlobalDefines.MESSAGE_SEPARATOR;
				
				System.out.println("CH-Nachricht wird zusammen gebaut");
				if(myCMCounter != 0)
				{
					for( int i = 0; i < myCMCounter; i++)
					{					
						CHInfoPart2 += GlobalDefines.MAC_SEPARATOR;
						CHInfoPart2 += myCMList.get(i);
						
						if(i == (myCMCounter-1))
						{
							CHInfoPart2 += GlobalDefines.MAC_SEPARATOR;
						}
					}
				}
				
				CHInfoPart3 += GlobalDefines.CHK_SUM_SEPARATOR;
				CHInfoPart3 += Integer.toString(fletcher16Checksum(CHInfoPart2));				
				
				CHInfoMessage = CHInfoPart1 + CHInfoPart2 + CHInfoPart3;
				System.out.println("Und jetzt sollte die Nachricht eigentlich versendet werden (Broadcast)");
				myIF1.broadcast(CHInfoMessage);
				
				if( GlobalDefines.SEND_CHINFO_TO_TEST_PC )
				{
					myIF1.unicastEthernet(GlobalDefines.TEST_PC_ETH_MAC, CHInfoMessage, null);
				}		
			}
		};
		
		
		webserverThread = new Runnable() {
			@Override
			public void run()
			{
				try
				{
					// kill any running tcpdump instance
					System.out.println(Util.getCurrentTime() + ": Killing any running tcpdump instance");
					ProcessCall.runCommandThrowInterruptedException("pkill", GlobalDefines.toolPath, "tcpdump");
					
					// start detached webserver
					System.out.println(Util.getCurrentTime() + ": Starting webserver");
					//ProcessCall.runCommandThrowInterruptedException("./webserver.sh", GlobalDefines.toolPath, myIF1.getIP());
					ProcessCall.runCommandDetachedThrowInterruptedException("./webserver.sh", GlobalDefines.toolPath, myIF1.getIP());
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		
		
		webclientThread = new Runnable() {
			@Override
			public void run()
			{
				try
				{
					// kill any running tcpdump instance
					System.out.println(Util.getCurrentTime() + ": Killing any running tcpdump instance");
					ProcessCall.runCommandThrowInterruptedException("pkill", GlobalDefines.toolPath, "tcpdump");
					
					List<String> cmIPs;
					List<String> chIPs;
					
					synchronized (myCMList) // get current CM IPs (to be monitored)
					{
						cmIPs = new ArrayList<String>();
						
						for (String cmMac : myCMList)
						{
							cmIPs.add(Util.MACToIP(cmMac));
						}
					}
					
					synchronized (myCHList) // get current CH IPs (to sync cluster info with)
					{
						chIPs = new ArrayList<String>();
						
						for (String chMac : myCHList)
						{
							// skip myself
							if (!chMac.equals(myIF1.getMAC()))
							{
								// TODO: currently, sync will be performed on IF2 with cluster IP domain 192.168.124.X
								// -> replace "192.168.123." substring of IF1 common channel IP domain with "192.168.124."
								chIPs.add(Util.MACToIP(chMac).replaceAll(GlobalDefines.IF1_IPBODY, GlobalDefines.IF2_IPBODY));
							}
						}
					}
					
					// construct ProcessCall parameter array
					
					List<String> paramList = new ArrayList<String>();
					
					// own IF2 IP as first argument (IF1 IP with subnet "*.*.123.*" replaced by "*.*.124.*")
					paramList.add(myIF1.getIP().replaceAll(GlobalDefines.IF1_IPBODY, GlobalDefines.IF2_IPBODY));
					
					paramList.add("CM"); // CM IPs follow after "CM"
					for (String cmIP : cmIPs)
						paramList.add(cmIP);
					
					paramList.add("CH"); // CH IPs (192.168.124.X) follow after "CH"
					for (String chIP : chIPs)
						paramList.add(chIP);
					
					String[] paramArray = new String[paramList.size()];
					for (int i = 0; i < paramList.size(); i++)
						paramArray[i] = paramList.get(i);
					
					// start detached webclient with parameters
					System.out.println(Util.getCurrentTime() + ": Starting webclient");
					//ProcessCall.runCommandThrowInterruptedException("./webclient.sh", GlobalDefines.toolPath, paramArray);
					ProcessCall.runCommandDetachedThrowInterruptedException("./webclient.sh", GlobalDefines.toolPath, paramArray);
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		
	}
	
	// TIMINFO: 050 SM1
	// state machine part 1 -> changeLocalPhase()
	// -> called periodically via main
	// -> performs actions depending on "myLocalPhase"
	// -> progression of "myLocalPhase" mainly depends on external phase messages sent by master CH
	// -> incoming external messages are handled in state machine part 2 -> handleReceivedCommand()
	// TODO: improve state machine -> let handleReceivedCommand() directly call functions that handle phase actions in background
	public void changeLocalPhase()
	{
		amIFree = false;
		
		int masterDelay = -1; // if set to something >= 0, master thread (phase message broadcasts) will be activated after "masterDelay"

		System.out.println(Util.getCurrentTime() + ": Phase " + String.valueOf(myLocalPhase));

		switch (myLocalPhase)
		{
			// TIMINFO: 060 SM1 Phase 0
			// PHASE 0: bootstrap phase
			// -> context reset (once per clustering run or after errors)
			// -> exchange of NC info between adjacent nodes
			// -> master CH selection via SYNC messages (if initial run or otherwise necessary)
			// -> SYNC messages contain metric "centrality", node with highest metric becomes master
			case 0:
			{
				if (virgin) // do the following only when reaching "phase 0" for the first time in a clustering run
				{
					// init start/done time variables for this clustering run
					startTime = System.currentTimeMillis();
					doneTime = System.currentTimeMillis();
					
					// undo any previous context for new clustering run
					//
					
					if (clusterHeadBroadcastFuture != null) {
						clusterHeadBroadcastFuture.cancel(true);
						clusterHeadBroadcastFuture = null;
					}
					
					//
					myMembership = GlobalDefines.CLUSTERFREE;
					//
					myClusterChannel = GlobalDefines.COMMON_CHANNEL;
					//
					myCentrality = 0;
					//
					myNodeQuantity = 0;
					//
					myMasterMAC = null;
					myMasterCentrality = 0;
					//
					myClusterheadMAC = null;
					//
					isSyncRunning = false;
					mySyncCounter = 0;
					//
					masterCHList = new ArrayList<String>();
					masterCHChannelList = new ArrayList<String>();
					//
					myCHList = new ArrayList<String>();
					myCMList = new ArrayList<String>();
					//
					myTimer = null;
					
					// enable HWMP root mode until we reach phase 3 (end of race for (Master) CH)
					myIF1.enableRootMode(true);
					
					// (re-)start neighbour count unicasts to neighbours
					// TODO: initial delay of NC unicast thread
					if (neighbourCountUnicastFuture != null) {
						neighbourCountUnicastFuture.cancel(true);
						neighbourCountUnicastFuture = null;
					}
					neighbourCountUnicastFuture = myExecutor.scheduleAtFixedRate(neighbourCountUnicast, 2*NC_UNICAST_PERIOD, NC_UNICAST_PERIOD, TimeUnit.MILLISECONDS);
					
					virgin = false;
				}
				
				// CLUSTERFREE
				if (myMembership.equals(GlobalDefines.CLUSTERFREE))
				{
					// check if clustered network already exists
					// -> wait X * CH_NOTIFICATION_TIMEOUT
					// -> CH announcements are handled in background by handleReceivedCommand()
					// -> if CH exist, there will be a timeout after no incoming "new" CH announcements
					// -> in this case, try to join "closest" CH 
					
//					try
//					{
//						// TODO: CF phase 0 cluster (CH) join grace time
//						// -> handleReceivedCommand() will reset CH_NOTIFICATION_TIMEOUT for any incoming "new" CH_MESSAGE
//						// -> therefore we should wait longer than CH_NOTIFICATION_TIMEOUT here
//				    	synchronized (waitForCHMon)
//				    	{
//				    		waitForCHMon.wait(2*CH_NOTIFICATION_TIMEOUT);
//						}
//					}
//					catch (InterruptedException e)
//					{
//						e.printStackTrace();
//					}
					
					//TIMDRAFT: CF roam and jump to phase 7 if CH Message comes in
					
					if( !globalClusterInfo.isEmpty() )
					{
						blockSetToCF = true;
						
						if (neighbourCountUnicastFuture != null) {
							neighbourCountUnicastFuture.cancel(true);
							neighbourCountUnicastFuture = null;
						}
						
						// stop any SYNC broadcasts
						if (syncBroadcastFuture != null) {
							syncBroadcastFuture.cancel(true);
							syncBroadcastFuture = null;
						}
						
//						interruptRoamingThread = true;
						amIFree = true;
						myIF1.enableRootMode(false);
						roam();
						blockSetToCF = false;
					}
					
					// check if we joined a cluster within the grace time
					if (myMembership.equals(GlobalDefines.CLUSTERMEMBER))
					{
						// we are CM -> successfully joined a cluster
						
						// stop any NC unicasts to neighbours
						if (neighbourCountUnicastFuture != null) {
							neighbourCountUnicastFuture.cancel(true);
							neighbourCountUnicastFuture = null;
						}
						
						// stop any SYNC broadcasts
						if (syncBroadcastFuture != null) {
							syncBroadcastFuture.cancel(true);
							syncBroadcastFuture = null;
						}
						
						// deactivate root mode
						myIF1.enableRootMode(false);
						
						// CM in phase 0 -> switch to phase 7 directly
						myLocalPhase = 7;
						amIFree = true;
					}
					else
					{
						// all CF that do not find an initial cluster/CH to join -> start broadcasting SYNC messages
						if (syncBroadcastFuture != null) {
							syncBroadcastFuture.cancel(true);
							syncBroadcastFuture = null;
						}
						isSyncRunning = true;
						syncBroadcastFuture = myExecutor.scheduleAtFixedRate(syncBroadcast, 0, SYNC_BROADCAST_PERIOD, TimeUnit.MILLISECONDS);
					}
				}
			}
			break;
			
			// TIMINFO: 070 SM1 Phase 1
			// PHASE 1: PCH selection based on NC
			case 1:
			{
				// stop NC unicasts to neighbours
				if (neighbourCountUnicastFuture != null) {
					neighbourCountUnicastFuture.cancel(true);
					neighbourCountUnicastFuture = null;
				}
				
				if (isMaster()) {
					// broadcast PHASE 2 after delay
					masterDelay = PHASE2_DELAY;
				}
				else
				{
					// we might be one of the remaining CH candidates -> take part in NC comparison
					// (master is already CH and therefore skips the race for PCH)
					if (becomePCHBasedOnNC())
					{
						// we became PCH according to NC metric comparison
						// -> advertise as PCH to neighbours via PCH_MESSAGE unicast
						synchronized (myNeighbours)
						{
							for (NeighbourListEntry nle : myNeighbours) {
								myIF1.unicast(nle.getMAC(), GlobalDefines.PCH_MESSAGE, null);
							}
						}
					}
				}
			}
			break;
			
			// TIMINFO: 080 SM1 Phase 2
			// PHASE 2: CH selection out of PCH based on "weighted NPR"
			case 2:
			{
				if (isMaster()) {
					// broadcast PHASE 3 after delay
					masterDelay = PHASE3_DELAY;
				}
				
				// PROPOSED_CLUSTERHEAD
				if (myMembership.equals(GlobalDefines.PROPOSED_CLUSTERHEAD))
				{
					int myPCHNC = 0; // PCH neighbour count
					
					synchronized (myNeighbours)
					{
						// TODO: Phase 2 "PCH has no neighbours (anymore)" case
						// -> can this really happen? would we have become PCH then?
						// -> should we become CH here or become CF instead and wait?
						if (myNeighbours.isEmpty()) {
							System.out.println(Util.getCurrentTime() + ": I have no neighbours anymore, set to CH");
							setToCH();
							break;
						}
						// TODO: count Master CH as "PCH" neighbour as well
						for (NeighbourListEntry nle : myNeighbours) {
							if (nle.getMembership().equals(GlobalDefines.PROPOSED_CLUSTERHEAD) ||
								nle.getMAC().equals(myMasterMAC))
							{
								myPCHNC += 1;
							}
						}
					}
					
					if (myPCHNC == 0) {
						System.out.println(Util.getCurrentTime() + ": No PCH around me, set to CH");
						setToCH();
						break;
					}
					
					// calculate 2nd stage weighted NPR metric and send it to PCH neighbours
					// -> will thin out PCH and determine final CHs
					double myWeightedNPR = calculateWNPR();
					
					List<String> data = new ArrayList<String>(1);
					data.add(Double.toString(myWeightedNPR));
					
					System.out.println(Util.getCurrentTime() + ": Send weighted NPR to all PCH neighbours");
					
					synchronized (myNeighbours)
					{
						for (NeighbourListEntry nle : myNeighbours) {
							if (nle.getMembership().equals(GlobalDefines.PROPOSED_CLUSTERHEAD)) {
								myIF1.unicast(nle.getMAC(), GlobalDefines.WNPR_MESSAGE, data);
							}
						}
					}
				}
			}
			break;
			
			// TIMINFO: 090 SM1 Phase 3
			// PHASE 3: CH announcement phase (unicast to master and broadcast to rest of NW)
			case 3:
			{
				if (isMaster()) {					
					// broadcast PHASE 4 after delay
					// TODO: if there are other CHs announcing to the master, the delay will be at least CH_NOTIFICATION_TIMEOUT
					masterDelay = PHASE4_DELAY;
				}
				
				// CLUSTERFREE
				if (myMembership.equals(GlobalDefines.CLUSTERFREE))
				{
					// deactivate root mode
					// -> race for (Master) CH is over and global ALM info not needed for centrality anymore
					myIF1.enableRootMode(false);
				}
				
				// CLUSTERHEAD
				if (myMembership.equals(GlobalDefines.CLUSTERHEAD))
				{
					// activate HWMP root mode (should already/still be enabled)
					// -> new CF / existing CM need info to decide for best CH at runtime
					myIF1.enableRootMode(true);
					
					if (!isMaster())
					{
						// "normal" CH -> announce via TCP unicast to master CH first
						myIF1.unicast(myMasterMAC, GlobalDefines.CH_MESSAGE, null);
					}
					
					// all CH (including master) -> start UDP broadcast of CH messages
					if (clusterHeadBroadcastFuture != null) {
						clusterHeadBroadcastFuture.cancel(true);
						clusterHeadBroadcastFuture = null;
					}
					clusterHeadBroadcastFuture = myExecutor.scheduleAtFixedRate(clusterHeadBroadcast, 0, CH_BROADCAST_PERIOD, TimeUnit.MILLISECONDS);
				}
			}
			break;
			
			// TIMINFO: 100 SM1 Phase 4
			// PHASE 4: CF join "closest" cluster/CH based on ALM
			case 4:
			{
				if (isMaster()) {
					// broadcast PHASE 5 after delay
					masterDelay = PHASE5_DELAY;
				}
				
				// CLUSTERFREE
				if (myMembership.equals(GlobalDefines.CLUSTERFREE))
				{
					// connect to CH with smallest ALM (prefer neighbouring CH)
					joinClusterBasedOnALM();
				}
			}
			break;
			
			// TIMINFO: 110 SM1 Phase 5
			// PHASE 5: CH select channel for cluster IF
			case 5:
			{				
				// -> in phase 5 only master and "normal" CHs do something
				// -> all "normal" CHs already know each other since the end of phase 3 (got CH_LIST_MESSAGE from master)
				// -> as master we initialize the channel selection sequence and send a CHANNEL_SELECT_MESSAGE to the closest "normal" CH
				// -> as "normal" CH we should do nothing until we receive a CHANNEL_SELECT_MESSAGE from the master or a previous "normal" CH
				// -> handleReceivedCommand() should handle "normal" CH behavior completely, only implement master in changeLocalPhase()
				
				// Master in phase 5
				if (isMaster())
				{
					// initialize "empty" CH_LIST_MESSAGE contents
					// -> empty message will be sent to first CH if we are not CH ourselves
					List<String> values = new ArrayList<String>(); 
					
					if (myMembership.equals(GlobalDefines.CLUSTERHEAD))
					{
						// master CH
						// -> we are the first to select a channel
						// -> add own selection to contents of CH_LIST_MESSAGE
						
						System.out.println(Util.getCurrentTime() + ": I'm master and also the first CH in the channel select sequence");
						
						// select the first channel in CHANNEL_SEQUENCE (consider last channel as prevChannel)
						// TODO: selectNewClusterChannel() error handling
						selectNewClusterChannel(GlobalDefines.CHANNEL_SEQUENCE[GlobalDefines.CHANNEL_SEQUENCE.length - 1]);
						
						System.out.println(Util.getCurrentTime() + ": I have selected channel " + Integer.toString(myClusterChannel));
						
						// set own selection as the message contents
						values = new ArrayList<String>();
						values.add(myIF1.getMAC());
						values.add(Integer.toString(myClusterChannel));
					}
					
					// determine if there are (other) CH and find the "closest" to send CH_LIST_MESSAGE to
					synchronized (myCHList)
					{
						// TODO: CH list should never be empty here ...
						System.out.println(Util.getCurrentTime() + ": CH list empty?: " + myCHList.isEmpty());
						
						// there is only one CH and it's me -> proceed to next phase
						if (myCHList.size() == 1 && myCHList.contains(myIF1.getMAC()) && myMembership.equals(GlobalDefines.CLUSTERHEAD))
						{
							System.out.println(Util.getCurrentTime() + ": I'm master and the only CH, proceed to next phase");
							
							// broadcast PHASE 6 after delay
							masterDelay = PHASE6_DELAY;
						}
						// there is at least one (other) CH -> send CHANNEL_SELECT_MESSAGE to the "closest" CH
						else if (myCHList.size() >= 1)
						{
							System.out.println(Util.getCurrentTime() + ": I'm master and there is at least one (other) CH");
							
							String closestCH = findClosestNode(myCHList);
							
							if (closestCH != null)
							{
								System.out.println(Util.getCurrentTime() + ": Send CHANNEL_SELECT_MESSAGE to \"closest\" CH: " + Util.MACToName(closestCH));
								myIF1.unicast(closestCH, GlobalDefines.CHANNEL_SELECT_MESSAGE, values);
							}
						}
					}
				}
			}
			break;
			
			// TIMINFO: 120 SM1 Phase 6
			// PHASE 6: apply selected channel and inform associated CM to do the same
			case 6:
			{
				if (myMembership.equals(GlobalDefines.CLUSTERHEAD))
				{
					// set new cluster channel
					setClusterChannel(myClusterChannel);
					
					synchronized (myCMList)
					{
						// send new channel to all associated CM
						// -> this is also done by CH for any individual CM that joins later
						for (String memberMAC : myCMList)
						{
							List<String> ch = new ArrayList<String>(1);
							ch.add(Integer.toString(myClusterChannel));
							myIF1.unicast(memberMAC, GlobalDefines.CHANNEL_SET_MESSAGE, ch);
						}
					}
	
					// directly proceed to phase 7
					myLocalPhase = 7;
					amIFree = true;
				}
			}
			break;
			
			// TIMINFO: 130 SM1 Phase 7
			// PHASE 7: application running within clusters -> monitoring of CM performed by CH
			case 7:
			{
				
				
				// determine clustering duration in seconds
				doneTime = System.currentTimeMillis();
				long clusteringDuration = (doneTime-startTime) / 1000;
//				String Time = Util.getCurrentTimeInMillisec();
				
//				// TIMDRAFT: print global Cluster Information
//				if( !globalClusterInfo.isEmpty() )
//				{
//					System.out.println("Tim: AllClusterTest New Print Run_____________________________: ");
//					
//					for( int i = 0;  i < globalClusterInfo.size(); i++)
//					{
//						System.out.println("Tim: AllClusterTest Cluster " + Integer.toString(i+1) + " :");
//						
//						for( int j = 0; j < globalClusterInfo.get(i).size(); j++ )
//						{
//							if( j == 0 )
//							{
//								System.out.println("Tim: " + Time + " AllClusterTest CH-MAC: " + globalClusterInfo.get(i).get(j));
//							}
//							else
//							{
//								System.out.println("Tim: " + Time + " AllClusterTest CM-MAC: " + globalClusterInfo.get(i).get(j));
//							}
//						}
//					}
//					
//					
//					System.out.println("Tim: myMac --> " + myIF1.getMAC() );
//					System.out.println("Tim: myClusterLoad (incl. ClusterHead) --> " + Integer.toString(myClusterLoad));
//				}
				
				// print status info
				System.out.println(Util.getCurrentTime() + ": Clustering finished in " + Long.toString(clusteringDuration) + " seconds");
				System.out.println("\tName: " + Util.MACToName(myIF1.getMAC()));
				System.out.println("\tMAC: " + myIF1.getMAC());
				System.out.println("\tIP: " + myIF1.getIP());
				//
				System.out.println("\tMembership: " + myMembership);
				//
				if( myMasterMAC != null )
				{
					System.out.println("\tMaster Name: " + Util.MACToName(myMasterMAC));
					System.out.println("\tMaster MAC: " + myMasterMAC);
					System.out.println("\tMaster IP: " + Util.MACToIP(myMasterMAC));
				}else {
					System.out.println("\tMaster Name: Unknown, startet my software after a finished clustering run");
				}
				
				if( myClusterheadMAC != null )
				{
					System.out.println("\tClusterhead Name: " + Util.MACToName(myClusterheadMAC));
					System.out.println("\tClusterhead MAC: " + myClusterheadMAC);
					System.out.println("\tClusterhead IP: " + Util.MACToIP(myClusterheadMAC));
					System.out.println("\tCluster Channel: " + Integer.toString(myClusterChannel));
				}else {
					System.out.println("\tClusterhead Name: Unknown, startet my software after a finished clustering run");
					System.out.println("\tCluster Channel: Unknown, startet my software after a finished clustering run");
				}
				
		
				//
				
				
				// signal that phase 7 has been reached by creating a "done" file -> needed for test automation
				try {
					
					File doneFile = new File(GlobalDefines.DONE_FILE_PATH + "clustering.done");
					doneFile.createNewFile();
					
					// write status info to "done" file
					FileWriter fw = new FileWriter(doneFile);
					
					fw.write("clusteringDuration: "	+ Long.toString(clusteringDuration) + System.lineSeparator());
					
					fw.write(System.lineSeparator());
					
					fw.write("myName: "				+ Util.MACToName(myIF1.getMAC()) + System.lineSeparator());
					fw.write("myMAC: "				+ myIF1.getMAC() + System.lineSeparator());
					fw.write("myIP: "				+ myIF1.getIP() + System.lineSeparator());
					
					fw.write(System.lineSeparator());
					
					fw.write("myMembership: "		+ myMembership + System.lineSeparator());
					
					fw.write(System.lineSeparator());
					
					if( myMasterMAC != null )
					{
						fw.write("myMasterName: "		+ Util.MACToName(myMasterMAC) + System.lineSeparator());
						fw.write("myMasterMAC: "		+ myMasterMAC + System.lineSeparator());
						fw.write("myMasterIP: "			+ Util.MACToIP(myMasterMAC) + System.lineSeparator());
					}
					
					fw.write(System.lineSeparator());
					
					if( myClusterheadMAC != null )
					{
						fw.write("myClusterheadName: "	+ Util.MACToName(myClusterheadMAC) + System.lineSeparator());
						fw.write("myClusterheadMAC: "	+ myClusterheadMAC + System.lineSeparator());
						fw.write("myClusterheadIP: "	+ Util.MACToIP(myClusterheadMAC) + System.lineSeparator());
						
						fw.write(System.lineSeparator());
						
						fw.write("myClusterChannel: "	+ Integer.toString(myClusterChannel) + System.lineSeparator());
					}
					
					
					
					fw.write(System.lineSeparator());
					
					if (!myCMList.isEmpty())
					{
						fw.write("myCMList:");
						for (int i=0; i<myCMList.size(); i++) {
							fw.write(" " + Util.MACToName(myCMList.get(i)));
						}
						fw.write(System.lineSeparator());
					}
					
					if (!myCHList.isEmpty())
					{
						fw.write("myCHList:");
						for (int i=0; i<myCHList.size(); i++) {
							fw.write(" " + Util.MACToName(myCHList.get(i)));
						}
						fw.write(System.lineSeparator());
					}
					
					fw.write(System.lineSeparator());
					
					fw.write("isMaster: "			+ Boolean.toString(isMaster()) + System.lineSeparator());
					
					if (!masterCHList.isEmpty())
					{
						fw.write("masterCHList:");
						for (int i=0; i<masterCHList.size(); i++) {
							fw.write(" " + Util.MACToName(masterCHList.get(i)));
						}
						fw.write(System.lineSeparator());
					}
					
					if (!masterCHChannelList.isEmpty())
					{
						fw.write("masterCHChannelList:");
						for (int i=0; i<masterCHChannelList.size(); i++) {
							fw.write(" " + masterCHChannelList.get(i));
						}
						fw.write(System.lineSeparator());
					}
					
					fw.close();
					
				} catch (IOException e) {
					e.printStackTrace();
				}
								
				if (myMembership.equals(GlobalDefines.CLUSTERMEMBER) || myMembership.equals(GlobalDefines.CLUSTERFREE))
				{
					// stop any NC unicasts to neighbours
					if (neighbourCountUnicastFuture != null) {
						neighbourCountUnicastFuture.cancel(true);
						neighbourCountUnicastFuture = null;
					}
					
					// stop any SYNC broadcasts
					if (syncBroadcastFuture != null) {
						syncBroadcastFuture.cancel(true);
						syncBroadcastFuture = null;
					}
					// disable HWMP root mode (should already be off since phase 3)
					myIF1.enableRootMode(false);
					
					if (!SKIP_MONITORING)
					{
						// start webserver on CM
						webserverThreadFuture = myExecutor.schedule(webserverThread, 0, TimeUnit.MILLISECONDS);
						
						// TODO: wait until webserver has been started
						while (true) {
							try {
								Thread.sleep(1000);	// TODO: as cmdline / config parameter
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							if(webserverThreadFuture.isDone()) {
								break;
							}
						}
					}
				}
				
				if (myMembership.equals(GlobalDefines.CLUSTERHEAD))
				{
					// TODO: should root mode and CH announcements be active only on common channel (normally IF1) or on cluster channel (normally IF2) as well?
					// myIF1.enableRootMode(true);
					// myIF2.enableRootMode(true);
					
					if (!SKIP_MONITORING)
					{
						// start webclient on CH
						webclientThreadFuture = myExecutor.schedule(webclientThread, MONITORING_DELAY, TimeUnit.MILLISECONDS);
						
						// TODO: wait until webclient has been started
						while (true) {
							try {
								Thread.sleep(1000);	// TODO: as cmdline / config parameter
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							if(webclientThreadFuture.isDone()) {
								break;
							}
						}
					}
					else
					{
						if (isMaster())
						{
							// directly write file "monitoring.done" for test automation
							File doneFile = new File(GlobalDefines.DONE_FILE_PATH + "monitoring.done");
							try {
								doneFile.createNewFile();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				}
				
				
				
				
				// TODO: as future work we need to implement various mechanisms that handle network dynamics
				//
				// * 	roaming of CM between CH via JOIN/LEAVE messages to respective CH
				//		-> CM need to check for "closer" CH (receive CH_MESSAGE broadcasts and compare path ALMs)
				//		-> CH adapt their CMs to be monitored if a CM joins/leaves (JOIN/LEAVE messages)
				//
				// * 	detection of failed CH via missing broadcast CH announcements (on common and/or cluster channel)
				//		-> try to select new CH from CM group in "abandoned" cluster, if possible
				//		-> alternatively, "abandoned" CM join other CH, or master CH re-triggers a new clustering run (starting in phase 0)
				//
				// * 	detect the need for new clustering (starting in phase 0 again) if a certain cluster capacity has been exceeded, etc.
				// 		-> store previous (i.e., current) membership and other status info for comparison to a new clustering result?
				//		-> reset current (i.e., upcoming) membership info (etc.) before entering phase 0
				//		-> perform data migration of existing CH info to new CH
				//
				// ...
				
				// in case of cm wait for ch's, so we can start semi-synchron 
				// TODO: maybe use a info message, that guarantees a synchronous start of the balancing thread
//				for( int i = 0; i < 3; i++ )
//				{
//					myIF1.broadcast(GlobalDefines.CLUSTERING_DONE + GlobalDefines.MESSAGE_SEPARATOR);
//					
//					try {
//						Thread.sleep(2000);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//				}
//				
//				System.out.println("myClusteringDoneListSize = " + Integer.toString(myClusteringDoneList.size()));
//				
//				while( myClusteringDoneList.size() < (getNodeQuantity()-1) )
//				{
//					System.out.println(Util.getCurrentTimeInMillisec() + " Wait for Clustering-Done-Messages...");
//					System.out.println("Remaining Clustering-Done-Messages: " + Integer.toString((getNodeQuantity()-1-myClusteringDoneList.size())) + " ...");
//					
//					try {
//						Thread.sleep(5000);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//				}
				
				System.out.println(Util.getCurrentTimeInMillisec() + " Received all needed Clustering-Done-Messages...");
				
				if( myMembership.equals(GlobalDefines.CLUSTERHEAD) )
				{
					System.out.println("Sending ClusteringDoneMessage to: " + GlobalDefines.TEST_PC_ETH_MAC);
					
					myIF1.unicastEthernet(GlobalDefines.TEST_PC_ETH_MAC , GlobalDefines.CLUSTERING_DONE + GlobalDefines.MESSAGE_SEPARATOR, null);
				}
				
				if( ROAMING_DELAY > -1 )
				{
					blockBalancingForOtherNodes = false;
					roamingPossible = true;
					roamingStartTime = System.currentTimeMillis();
					
					roamingBalancingFuture = myExecutor.scheduleAtFixedRate(roamingBalancing, 0, ROAMING_THREAD_SLEEP, TimeUnit.MILLISECONDS);
					System.out.println("RoamingBalancing-Thread is getting started");
					
					try
					{		
						Thread.sleep(ROAMING_DELAY);
						
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					roamingBalancingFuture.cancel(true);
					
					// write roaming.done
					updateRoamingDoneFile();
					
				}
				
				// TODO: exit "after" phase 7
				System.out.println(Util.getCurrentTime() + ": System exit");
				System.exit(0);
			}
			break;
			
			default: { }
			break;
		}
		
		// Master -> announce next phase (after masterDelay milliseconds)
		if (isMaster() && (masterDelay >= 0)) {
			int nextPhase = myLocalPhase + 1;
			System.out.println(Util.getCurrentTime() + ": Master: announce phase " + nextPhase + " after " + masterDelay + " ms");
			masterPhaseBroadcastFuture = myExecutor.schedule(masterPhaseBroadcast, masterDelay, TimeUnit.MILLISECONDS);
		}
	}
	
	
	// TIMINFO: 140 SM2
	// state machine part 2 -> handleReceivedCommand()
	// -> progression of "myLocalPhase" mainly depends on external phase messages sent by master node
	// -> incoming external messages are handled here
	public void handleReceivedCommand(String senderMACAdress, String data)
	{
		System.out.println(Util.getCurrentTime() + ": Received data from " + Util.MACToName(senderMACAdress) + ": " + data.trim());

		if (!data.trim().contains(GlobalDefines.MESSAGE_SEPARATOR))
			return;

		ArrayList<String> values = new ArrayList<String>(
				Arrays.asList(data.trim().split("\\" + GlobalDefines.MESSAGE_SEPARATOR)));
		String command = new String(values.get(0));
		values.remove(0);
		
		System.out.println(Util.getCurrentTime() + ": Command: " + command + " | Values: " + values);
		
		
		// for nodes in phase 0 ...
		if (myLocalPhase == 0)
		{
			// TIMINFO: 150 SM2 Arriving in Phase 0
			// PHASE 1 ... PHASE 6 message
			// -> there already is a clustering run in progress with an active master
			// -> we just came here (received a first phase message) and did not know the master yet
			// -> the race for master is over, so stop any SYNC broadcasts
			// -> remember current master's MAC address
			if ((myMasterMAC == null) &&
				(command.equals(GlobalDefines.PHASE1_MESSAGE) ||
				 command.equals(GlobalDefines.PHASE2_MESSAGE) ||
				 command.equals(GlobalDefines.PHASE3_MESSAGE) ||
				 command.equals(GlobalDefines.PHASE4_MESSAGE) ||
				 command.equals(GlobalDefines.PHASE5_MESSAGE) ||
				 command.equals(GlobalDefines.PHASE6_MESSAGE) ))
			{				
				if (syncBroadcastFuture != null) 
				{
					syncBroadcastFuture.cancel(true);
					syncBroadcastFuture = null;
					isSyncRunning = false;
				}
				
				myMasterMAC = new String(senderMACAdress);
			}
			
			
			// PHASE 1, PHASE 4, SYNC, NC, or CH message
			//
			// -> in case of PHASE 1 or 4 it is safe to continue and "hook in" to the clustering run
			// 	* in case of PHASE 1 we practically missed nothing but the race for master
			// 	* in case of PHASE 4 we get the chance to join an existing cluster / CH
			// 	* otherwise, stay in phase 0 until phase 1 or 4 is announced (again) or a CH is found to join
			//
			// -> in case of SYNC, there still is a race for master and we should take part
			// -> in case of NC, we should get it and update our neighbour's info
			// -> in case of CH, we should update our CH info and join the best cluster
			//
			if (command.equals(GlobalDefines.PHASE1_MESSAGE)	||
				command.equals(GlobalDefines.PHASE4_MESSAGE)	||
				command.equals(GlobalDefines.SYNC_MESSAGE)		||
				command.equals(GlobalDefines.NC_MESSAGE)		||
				command.equals(GlobalDefines.CH_MESSAGE))
			{
				; // continue to switch statement below
			}
			else {
				return;
			}
		}
		

		switch (command)
		{
			// PHASE 0 message
			// -> TODO: this should be triggered by master CH if found necessary -> logic still needs to be implemented
			// -> if we had a previous context as (M)CH or CM, we should remember everything and hook in as a "virgin" CF
			case GlobalDefines.PHASE0_MESSAGE:
			{
				// TODO: remember previous context (membership, master CH, own cluster CH, etc.)
				// TODO: CH -> remember previous cluster info for possible migration
				// ...
				
				myLocalPhase = 0;
				virgin = true;
				amIFree = true;
			}
			break;
			
			case GlobalDefines.PHASE1_MESSAGE:
			{
				if (myLocalPhase == 0)
				{
					myLocalPhase = 1;
					
					if (!isMaster())
						amIFree = true;
				}
			}
			break;
			
			case GlobalDefines.PHASE2_MESSAGE:
			{
				if (myLocalPhase == 1)
				{					
					myLocalPhase = 2;
					
					if (!isMaster())
						amIFree = true;
				}
			}
			break;
			
			case GlobalDefines.PHASE3_MESSAGE:
			{
				if (myLocalPhase == 2)
				{
					myLocalPhase = 3;
					
					if (myMembership.equals(GlobalDefines.PROPOSED_CLUSTERHEAD))
						setToCH();
					
					if (!isMaster())
						amIFree = true;
				}
			}
			break;
	
			case GlobalDefines.PHASE4_MESSAGE:
			{
				if (myLocalPhase == 3 || myLocalPhase == 0)
				{
					myLocalPhase = 4;
					
					// update membership info of myNeighbours in phase 4
					// -> assume phase 3 PCH neighbour info as outdated and reset memberships to CF
					// -> PCH neighbours that actually became CH are updated on incoming CH messages
					synchronized (myNeighbours)
					{
						for (NeighbourListEntry nle : myNeighbours) {
							if (nle.getMembership().equals(GlobalDefines.PROPOSED_CLUSTERHEAD)) {
								nle.setMembership(GlobalDefines.CLUSTERFREE);
							}
						}
					}
					
					if (!isMaster())
						amIFree = true;
				}
			}
			break;
			
			case GlobalDefines.PHASE5_MESSAGE:
			{
				if (myLocalPhase == 4)
				{
					myLocalPhase = 5;
					
					if (!isMaster())
						amIFree = true;
				}
			}
			break;
	
			case GlobalDefines.PHASE6_MESSAGE:
			{
				if (myLocalPhase == 5)
				{
					myLocalPhase = 6;
					
					if (!isMaster())
						amIFree = true;
				}
			}
			break;
			
			// TIMINFO: 160 SM2 Incoming Sync_MSG
			case GlobalDefines.SYNC_MESSAGE: // TODO: carries metric "centrality"
			{
				if (myLocalPhase == 0)
				{
					double senderCentrality = Double.valueOf(values.get(0));
					long senderMACSum = Util.calculateMACSum(senderMACAdress);
					
					System.out.println(Util.getCurrentTime() + ": Received SYNC from " + Util.MACToName(senderMACAdress) +
							", senderCentrality: " + Double.toString(senderCentrality) +
							", senderMACSum: " + Long.toString(senderMACSum));
					
					synchronized (handleSyncMon)
					{
						// as long as we are "syncing" ourselves (are still in the race), compare our centrality with others
						if (isSyncRunning)
						{
							// TODO: calculate myCentrality only once during SYNC phase while root mode is active
							if (myCentrality == 0)
								myCentrality = calculateCentrality();
							
							long myMACSum = myIF1.getMACSum();
							
							System.out.println(Util.getCurrentTime() + ": myCentrality: " + Double.toString(myCentrality) +
									", myMACSum: " + Long.toString(myMACSum));
							
							// compare sender's centrality with ours (highest wins, MAC sum will decide if metrics are equal)
							if	(	 (myCentrality  > senderCentrality) ||
									((myCentrality == senderCentrality) && (myMACSum > senderMACSum))
								)
							{
								// we won this comparison and are still in the race for master
								System.out.println(Util.getCurrentTime() + ": Setting myself as my current master!");
								myMasterMAC = myIF1.getMAC();
								myMasterCentrality = myCentrality;
								mySyncCounter = 0;
							}
							else
							{
								// we are out of the race -> cancel sending of SYNC messages
								if (syncBroadcastFuture != null)
								{
									System.out.println(Util.getCurrentTime() + ": I have lost the comparison and will not be master!");
									syncBroadcastFuture.cancel(true);
									syncBroadcastFuture = null;
									isSyncRunning = false;
								}
							}
						}
						// check if we need to update our current master info
						// -> compare metric of currently accepted master with the metric of this SYNC message's candidate
						// -> highest wins, MAC sum will decide if metrics are equal
						if	(	( myMasterMAC == null ) ||
								( !myMasterMAC.equals(senderMACAdress) &&
									(	( senderCentrality  > myMasterCentrality) ||
										((senderCentrality == myMasterCentrality) && (Util.calculateMACSum(senderMACAdress) > Util.calculateMACSum(myMasterMAC)))
									)
								)
							)
						{
							System.out.println(Util.getCurrentTime() + ": Setting " + Util.MACToName(senderMACAdress) + " as my current master!");
							myMasterMAC = senderMACAdress;
							myMasterCentrality = senderCentrality;
						}
					}
					
				}
				else if (myLocalPhase > 0) // TODO: is SYNC_ABORT_MESSAGE necessary?
				{
//					synchronized (myNeighbours)
//					{
//						for (NeighbourListEntry nle : myNeighbours) {
//							if (nle.getMAC().equals(senderMACAdress))
//								myIF1.unicast(senderMACAdress, GlobalDefines.SYNC_ABORT_MESSAGE, null);
//						}
//					}
				}
			}
			break;
				
			case GlobalDefines.SYNC_ABORT_MESSAGE: // TODO: is SYNC_ABORT_MESSAGE necessary?
			{
				if (syncBroadcastFuture != null) {
					syncBroadcastFuture.cancel(true);
					syncBroadcastFuture = null;
				}
				isSyncRunning = false;
				
				if( !blockSetToCF )
				{
					setToCF();
				}
			}
			break;
	
			// TIMINFO: 170 SM2 Incoming NC_MSG
			case GlobalDefines.NC_MESSAGE: // TODO: carries metric "NC"
			{
				synchronized (myNeighbours)
				{
					for (NeighbourListEntry nle : myNeighbours) {
						if (nle.getMAC().equals(senderMACAdress)) {
							int senderNC = Integer.parseInt(values.get(0));
							nle.setNeighbourCount(senderNC);
							System.out.println(Util.getCurrentTime() + ": Setting NC " + Integer.toString(senderNC) + " for " + Util.MACToName(senderMACAdress));
							break;
						}
					}
				}
			}
			break;
			
			// TIMINFO: 180 SM2 Incoming WNPR_MSG
			case GlobalDefines.WNPR_MESSAGE: // TODO: carries metric "weighted NPR"
			{
				if (myMembership.equals(GlobalDefines.PROPOSED_CLUSTERHEAD))
				{
					System.out.println(Util.getCurrentTime() + ": PCH: Received WNPR_MESSAGE.");
					
					double myWeightedNPR = calculateWNPR();
							
					synchronized (myNeighbours)
					{
						for (NeighbourListEntry nle : myNeighbours)
						{
							if (!nle.getMAC().equals(senderMACAdress)) {
								continue;
							}
							
							double senderWeightedNPR = Double.valueOf(values.get(0));
							
							System.out.println(Util.getCurrentTime() + ": " + Util.MACToName(nle.getMAC())
									+ " has weighted NPR of " + Double.toString(senderWeightedNPR)
									+ " and MAC sum of " + Long.toString(nle.getMACSum()));
							
							// compare neighbour's weighted NPR with ours (highest wins, MAC sum will decide if metrics are equal)
							if ((senderWeightedNPR > myWeightedNPR) ||
							   ((senderWeightedNPR == myWeightedNPR) && (nle.getMACSum() > myIF1.getMACSum())))
							{
								// we lost -> become CF
								System.out.println(Util.getCurrentTime() + ": I lost the comparison and become CF again!");
								setToCF();
								break;
							}
							
							break;
						}
					}
				}
			}
			break;
			
			// TIMINFO: 190 SM2 Incoming PCH_MSG
			case GlobalDefines.PCH_MESSAGE:
			{
				synchronized (myNeighbours)
				{
					System.out.println(Util.getCurrentTime() + ": Received PCH_MESSAGE. Size of myNeighbours: " + Integer.toString(myNeighbours.size()));
					for (NeighbourListEntry nle : myNeighbours) {
						if (nle.getMAC().equals(senderMACAdress)) {
							nle.setMembership(GlobalDefines.PROPOSED_CLUSTERHEAD);
							break;
						}
					}
				}
			}
			break;
			
			// TIMDRAFT: 191 SM2 Incoming ROAMAING_MESSAGE
			case GlobalDefines.ROAMING_FALSE_MESSAGE:
			{
				System.out.println(Util.getCurrentTime() + ": Received ROAMING_FALSE_MESSAGE from: "  + Util.MACToName(senderMACAdress) + " ... disable Roaming");
				roamingPossible = false;
			}
			break;
			
			// TIMDRAFT: 192 SM2 Incoming ROAMAING_MESSAGE
			case GlobalDefines.ROAMING_TRUE_MESSAGE:
			{
				System.out.println(Util.getCurrentTime() + ": Received ROAMING_TRUE_MESSAGE from: "  + Util.MACToName(senderMACAdress) + " ... enable Roaming");
				roamingPossible = true;
			}
			break;
			
			// TIMINFO: 200 SM2 Incoming CH_MSG
			case GlobalDefines.CH_MESSAGE:
			{
				System.out.println(Util.getCurrentTime() + ": Received CH_MESSAGE. My membership: " + myMembership +
						"; My phase: " + Integer.toString(myLocalPhase) + "; isMaster: " + isMaster());
				
				// TIMDRAFT: creating global network overview based on incoming CH_MESSAGES
				// -> takes the whole data-String and uses method extractCHMessageInformation() to fill globalClusterInformation-List
				// -> the dynamic ArrayList defined by [private ArrayList<ArrayList> globalClusterInfo;] contains the CHs and their CMs
				// -> therefore every sublists first entry is the senderMAC (CH) and the following entries in the List are the CM-MACS of this cluster
				
				// TIM: Checksum CH-MSG
				// to avoid the usage of incorrect messages, we use a Checksum in the last field of the ch-message
				// therefore we implemented a simple fletcher-16bit-checksum-generator which returns the sum of the CM-MAC-field of the message
				// in first instance the transmitting CH calculates the checksum of the CM-MAC-field and add it to the CH-MSG
				// secondly all CH-MSG-receiving nodes extract die last field (checksum) and than calculates the checksum of the MAC-field equally
				// in case of equal checksum of both transmitting and receiving node we will perform further steps
				
//				int generatedChecksum = calculateChecksumFromReceivedCMMACS( data );
//				int receivedChecksum = getChecksumFromReceivedCHMsg( data );
				int ClusterInfoOverwriteRowIndex = -1;
				
//				if( generatedChecksum != 0 )
//				{
//					if( generatedChecksum == receivedChecksum )
//					{
						System.out.println(" ");
						System.out.println("Tim: Checksum-comparison of incoming CH-Message was successful");
						
						ArrayList<String> ClusterInfoTmp = new ArrayList<String>(extractCHMessageInformation( senderMACAdress, data ));
						
//						if(!ClusterInfoTmp.isEmpty() && !globalClusterInfo.isEmpty())
						synchronized( globalClusterInfo )
						{
							if(!ClusterInfoTmp.isEmpty())
							{
	
								for( int i = 0; i < globalClusterInfo.size();i++)
								{
									ArrayList<String> globalClusterInfoRow = new ArrayList<String>(globalClusterInfo.get(i));
									
									if( globalClusterInfoRow.contains(ClusterInfoTmp.get(0) ) ) 	//ClusterInfoTmp.get(0) is always CH
									{
										ClusterInfoOverwriteRowIndex = i;
									}
									
//									if( globalClusterInfoRow.contains(myIF1.getMAC()) )
//									{
//										myClusterLoad = globalClusterInfoRow.size();
//									}
									
								}
								
								if( ClusterInfoOverwriteRowIndex > -1 )
								{
										globalClusterInfo.set(ClusterInfoOverwriteRowIndex, ClusterInfoTmp);
										AllClusterChannel.set(ClusterInfoOverwriteRowIndex, values.get(0));
										globalClusterTimeStamps.set(ClusterInfoOverwriteRowIndex, System.currentTimeMillis());
								}
								else
								{
									globalClusterInfo.add(ClusterInfoTmp);
									AllClusterChannel.add(values.get(0));
									globalClusterTimeStamps.add(System.currentTimeMillis());
								}
								
								System.out.println(" ");
								
							}
						}
//					}
//					else
//					{
//						System.out.println(" ");
//						System.out.println("Tim: Checksum-comparison of incoming CH-Message was not successful.... skipping this CH-Message");
//					}
//				}
//				else
//				{
//					System.out.println(" ");
//					System.out.println("Tim: there are no CM-MACs in this Message, so we can't generate a Checksum");
//				}
				
				// all nodes -> update CH membership info in myNeighbours (if CH is a neighbour)
				synchronized (myNeighbours)
				{
					for (NeighbourListEntry nle : myNeighbours) {
						if (nle.getMAC().equals(senderMACAdress)) {
							nle.setMembership(GlobalDefines.CLUSTERHEAD);
							nle.setClusterheadMAC(senderMACAdress);
							break;
						}
					}
				}
				
				// all nodes except master -> add CH to myCHList if not already known
				if (!isMaster())
				{
					synchronized (myCHList)
					{
						if (!myCHList.contains(senderMACAdress))
						{
							myCHList.add(senderMACAdress);
							System.out.println(Util.getCurrentTime() + ": Added " + Util.MACToName(senderMACAdress) + " to myCHList");
						}
						else
						{
							System.out.println(Util.getCurrentTime() + ": I already know CH " + Util.MACToName(senderMACAdress));
						}
					}
				}
				
				// CLUSTERFREE in phase 0
				// -> we just arrived as CF and received a CH notification
				// -> try to join best CH that we know after CH_NOTIFICATION_TIMEOUT ms without new CH messages
				if (myMembership.equals(GlobalDefines.CLUSTERFREE) && (myLocalPhase == 0))
				{
					System.out.println(Util.getCurrentTime() + ": CF: Missed previous clustering activity. Got a new CH message, (re-)start CH notification timeout");
					
//					// for any CH message from a previously unknown CH, restart timer to gather further notifications
//					// -> after CH_NOTIFICATION_TIMEOUT ms without new CH messages, finally join "closest" CH we know 
//					
//					// terminate any running timer task
//					if (myTimer != null)
//					{
//						myTimer.cancel();
//						myTimer.purge();
//					}
//					
//					// start new timer task
//					myTimer = new Timer();
//					myTimer.schedule(new TimerTask()
//					{
//						@Override
//						public void run()
//						{
//							// CH_NOTIFICATION_TIMEOUT elapsed -> join "closest" CH in list
//							
//							// System.out.println(Util.getCurrentTime() + ": CF: CH notification timer elapsed, join \"closest\" CH");
//							
////							if (joinClusterBasedOnALM())
////							{
////								// TODO: join was successful
////								// -> inform monitor that state machine threads waits for
////								synchronized (waitForCHMon) {
////									waitForCHMon.notify();
////								}
////							}
////							else
////							{
////								// TODO: join was not successful
////								// -> stay in phase 0 and do nothing, we will get here again ...
////							}
//							
//							// terminate this timer task
//							myTimer.cancel();
//							myTimer.purge();
//						}
//					}, CH_NOTIFICATION_TIMEOUT );
					
					//TIM:
					if (virgin)
					{
						myLocalPhase = 0;
						changeLocalPhase();
						
						myLocalPhase = 7;
						changeLocalPhase();
						
					}else {
						
						myLocalPhase = 7;
						changeLocalPhase();
					}
				}
				
				// TODO: Master in phase 2 or 3:
				// -> receive incoming CH messages and remember "new" (previously unknown) CH in masterCHList
				// -> after CH_NOTIFICATION_TIMEOUT in phase 3, take masterCHList as final CH list and send it to the other CH
				// -> ignore new CH announcements after final CH list has been sent on CH_NOTIFICATION_TIMEOUT
				else if (isMaster() && (myLocalPhase == 2 || myLocalPhase == 3))
				{
					boolean known = false;
					boolean sentFinalCHList = false;
					synchronized (masterCHList)
					{
						// true if CH already in list
						known = masterCHList.contains(senderMACAdress);
						
						synchronized (myCHList)
						{
							// after sending final masterCHList to other CH, myCHList is set to masterCHList
							sentFinalCHList = !myCHList.isEmpty() && myCHList.equals(masterCHList);
						}
					}
					
					// do the following only before timeout fired first in phase 3 and final CH list has been sent
					if ( !sentFinalCHList )
					{
						// add previously unknown CH to masterCHList
						if ( !known )
						{
							synchronized (masterCHList)
							{
								if (!masterCHList.contains(senderMACAdress)) // TODO: do another sanity check
								{
									masterCHList.add(senderMACAdress);
								}
							}
							
							System.out.println(Util.getCurrentTime() + ": Master: Added " + Util.MACToName(senderMACAdress) + " to masterCHList");
						}
						else
						{
							System.out.println(Util.getCurrentTime() + ": Master: I already know CH " + Util.MACToName(senderMACAdress));
						}
						
						// do the following only within phase 3 -> at least once and for every "new" CH message
						if ((myLocalPhase == 3) && ((myTimer == null) || !known))
						{
							// as long as we handle CH notifications in phase 3, prevent sending phase 4 messages
							if (masterPhaseBroadcastFuture != null)
							{
								masterPhaseBroadcastFuture.cancel(true);
								masterPhaseBroadcastFuture = null;
							}
							
							// reactivate a timer on every incoming "new" CH message
							// after CH_NOTIFICATION_TIMEOUT ms without new CH notification -> send final CH list to all CH
							
							System.out.println(Util.getCurrentTime() + ": Master: (Re-)start CH notification timeout");
							
							// terminate any running timer task
							if (myTimer != null)
							{
								myTimer.cancel();
								myTimer.purge();
							}
							
							// start new timer task
							myTimer = new Timer();
							myTimer.schedule(new TimerTask()
							{
								@Override
								public void run()
								{
									System.out.println(Util.getCurrentTime() + ": Master: CH notification timer elapsed, send final CH list to all CH");
									
									// order of CHs in masterCHList, sent by master to CHs in phase 3, does not matter
									// -> only want to inform all CH about each other, needed for phase 5 (at least)
									// -> order of CH in phase 5 channel select sequence will be given by contents of CHANNEL_SELECT_MESSAGE
									// -> CH only need to compare local CH list with received CHANNEL_SELECT_MESSAGE to derive remaining CH in channel select sequence
									// -> CH will select among the remaining CH the closest CH to be their successor in channel selection
									
									synchronized (masterCHList)
									{									
										// unicast master CH list to all CH
										for (String mle : masterCHList)
										{
											// skip myself as receiver
											if (mle.equals(myIF1.getMAC()))
												continue;
											
											myIF1.unicast(mle, GlobalDefines.CH_LIST_MESSAGE, masterCHList);
										}
										
										// set myCHList to masterCHList
										synchronized (myCHList)
										{
											myCHList = new ArrayList<String>(masterCHList);
										}
									}
									
									// start sending phase 4 messages after having handled CH_NOTIFICATION_TIMEOUT in phase 3
									if (masterPhaseBroadcastFuture != null) {
										masterPhaseBroadcastFuture.cancel(true);
										masterPhaseBroadcastFuture = null;
									}
									masterPhaseBroadcastFuture = myExecutor.schedule(masterPhaseBroadcast, 0, TimeUnit.MILLISECONDS);
									
									// terminate this timer task
									myTimer.cancel();
									myTimer.purge();
								}
							}, CH_NOTIFICATION_TIMEOUT);
						}
					}
				}
			}
			break;
			
			// TIMINFO: 201 incoming ClusteringDoneMessage
			case GlobalDefines.CLUSTERING_DONE:
			{
				System.out.println(Util.getCurrentTime() + " Incoming CLUSTERING_DONE-Message");
				
				if( myClusteringDoneList.isEmpty() )
				{
					myClusteringDoneList.add(senderMACAdress);
				}else {
					if( !myClusteringDoneList.contains(senderMACAdress) )
					{
						myClusteringDoneList.add(senderMACAdress);
					}
				}
			}break;
			
			case GlobalDefines.MY_NEXT_HOP_TO_CH:
			{
//				blockNextHopList = true;
				
				System.out.println("Received NextHopMessage from: " + Util.MACToName(senderMACAdress));
				
				boolean allreadyadded = false;
				
				List<String> receivedNextHopToCH = new ArrayList<String>();
				
//				ArrayList<String> Info = new ArrayList<String>(
//						Arrays.asList(data.trim().split(GlobalDefines.MAC_SEPARATOR)));
				
				receivedNextHopToCH.add(senderMACAdress);
				receivedNextHopToCH.add(values.get(0));
				
				if( !blockNextHopList )
				{
					synchronized (myNeighboursNextHopToCH)
					{
						if( myNeighboursNextHopToCH.isEmpty() )
						{
							myNeighboursNextHopToCH.add(receivedNextHopToCH);
							allreadyadded = true;
							
						}else {
							
							for(int i = 0; i < myNeighboursNextHopToCH.size(); i++ )
							{
								if( myNeighboursNextHopToCH.get(i).get(0).contains(senderMACAdress) )
								{
									myNeighboursNextHopToCH.set(i, receivedNextHopToCH);
									allreadyadded = true;
								}
							}
						}
				
						if( !allreadyadded )
						{
							myNeighboursNextHopToCH.add(receivedNextHopToCH);
						}
						
						for( int i = 0; i < myNeighboursNextHopToCH.size(); i++ )
						{
							System.out.println("My Neighbour " + Integer.toString(i) + " is " + Util.MACToName(myNeighboursNextHopToCH.get(i).get(0)) + " and its next Hop to his CH is: " + Util.MACToName(myNeighboursNextHopToCH.get(i).get(1)));
						}
					}
				}
				
				checkNextHopListAndDeleteObsolete();
				
//				blockNextHopList = false;
								
			}break;
			
			// TIMINFO: 210 SM2 Incoming CH_LIST_MSG
			case GlobalDefines.CH_LIST_MESSAGE:
			{
				if (myMembership.equals(GlobalDefines.CLUSTERHEAD))
				{
					synchronized (myCHList)
					{
						// accept new CH list that was sent to us by the master
						myCHList = new ArrayList<String>(values);
					}
				}
			}break;
			
			// TIMINFO: 220 SM2 Incoming JOIN_MSG
			case GlobalDefines.JOIN_MESSAGE:
			{
				if (myMembership.equals(GlobalDefines.CLUSTERHEAD))
				{					
					synchronized (myCMList)
					{
						if (!myCMList.contains(senderMACAdress))
						{
							myCMList.add(senderMACAdress);
							
							if (myLocalPhase >= 6)
							{
								// let newly joined CM set its cluster interface to the correct channel
								List<String> ch = new ArrayList<String>(1);
								ch.add(Integer.toString(myClusterChannel));

								myIF1.unicast(senderMACAdress, GlobalDefines.CHANNEL_SET_MESSAGE, ch);
								
								// TODO: "refresh" monitoring application to consider joined CM
								if (!SKIP_MONITORING)
								{
									webclientThreadFuture.cancel(true);
									webclientThreadFuture = myExecutor.schedule(webclientThread, MONITORING_DELAY, TimeUnit.MILLISECONDS);
								}
							}
						}
					}
				}
			}
			break;
			
			// TIMINFO: 230 SM2 Incoming LEAVE_MSG
			case GlobalDefines.LEAVE_MESSAGE:
			{
				if (myMembership.equals(GlobalDefines.CLUSTERHEAD))
				{
					synchronized (myCMList)
					{
						if (myCMList.contains(senderMACAdress))
						{
							myCMList.remove(senderMACAdress);
							
							if (myLocalPhase >= 6)
							{
								// TODO: "refresh" monitoring application to consider left CM
								if (!SKIP_MONITORING)
								{
									webclientThreadFuture.cancel(true);
									webclientThreadFuture = myExecutor.schedule(webclientThread, MONITORING_DELAY, TimeUnit.MILLISECONDS);
								}
							}
						}
					}
				}
			}
			break;
			
			case GlobalDefines.PING_MESSAGE:
			{
				boolean isInMyCluster = false;
 				
				for( int i = 0; i < globalClusterInfo.size(); i++ )
				{
					if( globalClusterInfo.get(i).contains(myIF1.getMAC()) && globalClusterInfo.get(i).contains(senderMACAdress) )
					{
						isInMyCluster = true;
					}
				}
				
				if( isInMyCluster )
				{
					sendPongTo(senderMACAdress);
				}
				
			}break;
			
			case GlobalDefines.PONG_MESSAGE:
			{
				int indexToRemove = -1;
				
				for( int i = 0; i < expectedPongList.size(); i++ )
				{
					if( expectedPongList.get(i).contains(senderMACAdress) )
					{
						indexToRemove = i;
					}
				}
				
				if( indexToRemove > -1 )
				{
					expectedPongList.remove(indexToRemove);
				}
			}break;
			
			case GlobalDefines.LEAVE_REQUEST_MESSAGE:
			{				
				if( myMembership.equals(GlobalDefines.CLUSTERHEAD) )
				{
					if( ( System.currentTimeMillis() - lastLeave ) > GlobalDefines.ACK_TIMEOUT )
					{
						lastLeave = System.currentTimeMillis();
						
						myIF1.unicast(senderMACAdress, GlobalDefines.LEAVE_ACK_MESSAGE, null);
					}
				}
			}break;
			
			case GlobalDefines.JOIN_REQUEST_MESSAGE:
			{
				boolean permitClusterJoining = false;
				boolean allreadyInBlackList = false;
				
				String clusterheadOfSendingNode = "";
				
				ArrayList<String> blackListNewEntry = new ArrayList<String>();
				
				for( int i = 0; i < globalClusterInfo.size(); i++ )
				{
					if( globalClusterInfo.get(i).contains(senderMACAdress) )
					{
						clusterheadOfSendingNode = globalClusterInfo.get(i).get(0);
					}
				}
				
				blackListNewEntry.add(Long.toString(System.currentTimeMillis()));
				blackListNewEntry.add(clusterheadOfSendingNode);
				
				if( !clusterheadBlackList.isEmpty() )
				{
					for( int i = 0; i < clusterheadBlackList.size(); i++ )
					{
						if( clusterheadBlackList.get(i).contains(clusterheadOfSendingNode) )
						{
							allreadyInBlackList = true;
							
							if( ( System.currentTimeMillis() - Long.parseLong(clusterheadBlackList.get(i).get(0)) ) > 2*(long)GlobalDefines.CH_BROADCAST_PERIOD )
							{
								permitClusterJoining = true;
								clusterheadBlackList.get(i).set(0, Long.toString(System.currentTimeMillis()));
							}
						}
					}
				}else {
					clusterheadBlackList.add(blackListNewEntry);
				}
				
				if( !allreadyInBlackList )
				{
					clusterheadBlackList.add(blackListNewEntry);
					myIF1.unicast(senderMACAdress, GlobalDefines.JOIN_ACK_MESSAGE, null);
				}
				
				if( permitClusterJoining )
				{
					myIF1.unicast(senderMACAdress, GlobalDefines.JOIN_ACK_MESSAGE, null);
				}
				
			}break;
			
			case GlobalDefines.LEAVE_ACK_MESSAGE:
			{
				if( myMembership.equals(GlobalDefines.CLUSTERMEMBER) )
				{
					isLeavingPossible = true;
				}
				
			}break;
			
			case GlobalDefines.JOIN_ACK_MESSAGE:
			{
//				if( myMembership.equals(GlobalDefines.CLUSTERMEMBER) )
//				{
					isJoiningPossible = true;
					ackCH = senderMACAdress;
//				}
			}break;
			
			// TIMINFO: 240 SM2 Incoming CHAN_SEL_MSG
			case GlobalDefines.CHANNEL_SELECT_MESSAGE:
			{
				// Master 	-> should receive a full CHANNEL_SELECT_MESSAGE at the end of the sequence
				//			-> would have been the first to select a channel if master is also CH
				if (isMaster() && !values.isEmpty())
				{
				    // check if all CH managed to select a channel
					boolean success = true;
					for (String chMac : masterCHList)
					{
						if (!values.contains(chMac)) {
							success = false;
							break;
						}
					}
				    if (success)
				    {
				    	// remember CH channel selection sequence (masterCHChannelList) to print it into "clustering.done" file later
				    	masterCHChannelList = new ArrayList<String>();
				    	for (int i=0; i<values.size(); i++)
				    	{
				    		if ( (i==0) || ((i % 2) == 0) )
				    		{
				    			// even index -> CH MAC String -> convert to name
				    			masterCHChannelList.add(Util.MACToName(values.get(i)));
				    		}
				    		else
				    		{
				    			// uneven index -> channel String -> add directly
				    			masterCHChannelList.add(values.get(i));
				    		}
				    	}
				    	
				    	// announce phase 6 after successful channel selection
				    	System.out.println(Util.getCurrentTime() + ": Master: Channel selection finished successfully, announce phase 6");
				        masterPhaseBroadcastFuture = myExecutor.schedule(masterPhaseBroadcast, PHASE6_DELAY, TimeUnit.MILLISECONDS);
				    }
				    else {
				    	// TODO: handle error case (re-trigger channel selection sequence?)
				    	System.out.println(Util.getCurrentTime() + ": Master: ERROR in channel selection!");
				    	// ...
				    }
					
					break;
				}
	
				// CLUSTERHEAD -> perform my part of the channel selection sequence
				if (myMembership.equals(GlobalDefines.CLUSTERHEAD))
				{
					// select new channel from list (pass channel selected by previous CH to function)
					// TODO: selectNewClusterChannel() error handling
					if (values.isEmpty())
					{
						// we are the first non-master CH to select a channel (received an empty CHANNEL_SELECT_MESSAGE from master)
						selectNewClusterChannel(GlobalDefines.CHANNEL_SEQUENCE[GlobalDefines.CHANNEL_SEQUENCE.length - 1]);
					}
					else {
						// non-empty CHANNEL_SELECT_MESSAGE, some other CH (maybe even the master) selected a channel before us -> use this as prevChannel
						selectNewClusterChannel(Integer.parseInt(values.get(values.size() - 1)));
					}
					
					System.out.println(Util.getCurrentTime() + ": I have selected channel " + Integer.toString(myClusterChannel));
					
					// add myself and my selected channel to message contents
					values.add(myIF1.getMAC());
					values.add(Integer.toString(myClusterChannel));
					
					// copy CHs which have not selected a channel yet
					List<String> remainingCH = new ArrayList<String>();
					synchronized (myCHList)
					{
						for (String ch : myCHList)
						{
							if (!values.contains(ch)) {
								remainingCH.add(ch);
							}
						}
					}
					
					// find "closest" among remaining CHs (smallest path ALM)
					String closestCH = findClosestNode(remainingCH);
					
					if (closestCH != null)
					{
						// successfully found a successor CH -> send extended message to next CH
						myIF1.unicast(closestCH, GlobalDefines.CHANNEL_SELECT_MESSAGE, values);
					}
					else
					{
						// we are the last CH in the channel select sequence
						System.out.println(Util.getCurrentTime() + ": I'm the last CH in the channel select sequence, send full CHANNEL_SELECT_MESSAGE to master");
						myIF1.unicast(myMasterMAC, GlobalDefines.CHANNEL_SELECT_MESSAGE, values);
					}
				}
			}
			break;
			
			// TIMINFO: 250 SM2 Incoming CHAN_SET_MSG
			case GlobalDefines.CHANNEL_SET_MESSAGE:
			{
				if (myMembership.equals(GlobalDefines.CLUSTERMEMBER) && senderMACAdress.equals(myClusterheadMAC))
				{
					// CM: set new cluster channel
					setClusterChannel(Integer.parseInt(values.get(0)));
					
					if (myLocalPhase == 5 || myLocalPhase == 6)
					{
						// phase 5: CM whose CH reached phase 6 quicker and already sent CHANNEL_SET_MESSAGE
						// phase 6: CM that received PHASE6_MESSAGE early enough and reached phase 6 before receiving CHANNEL_SET_MESSAGE
						System.out.println(Util.getCurrentTime() + ": " + myMembership + ": Received CHANNEL_SET_MESSAGE in phase " + myLocalPhase + ", proceed to phase 7");
						myLocalPhase = 7;
						amIFree = true;
					}
				}
			}
			break;
			
			default: { }
			break;
		}
	}
	
	
	// TODO: get rid of pseudo mutex "amIFree"
	public boolean isFree() {
		return amIFree;
	}
	
	
	
	/* Main: */
	
	public static void main(String[] args)
	{
		
		int INITIAL_THREAD_SLEEP = GlobalDefines.INITIAL_THREAD_SLEEP;
		int WAIT_MESHNODE_IS_FREE = GlobalDefines.WAIT_MESHNODE_IS_FREE;

		// read optional args (coming after <IF1Name,IF1meshID,IF2Name,IF2meshID>)
		List<String> optionalArgs = new ArrayList<String>(Arrays.asList(args));
		for (int i = 0; i < 5; i++)
			optionalArgs.remove(0);
					
		// handle optional argument values
		for (String arg : optionalArgs)
		{
			String[] cmd = arg.split("=");
			try
			{
				switch (cmd[0])
				{				
					case "INITIAL_THREAD_SLEEP": {
						INITIAL_THREAD_SLEEP = Integer.parseInt(cmd[1]);
					}
					break;
					
					case "WAIT_MESHNODE_IS_FREE": {
						WAIT_MESHNODE_IS_FREE = Integer.parseInt(cmd[1]);
					}
					break;
				}
			
			} catch (NumberFormatException e) {
				System.out.println("Wrong argument: " + arg);
			}
		}
		
		MeshNode LocalMeshNode = null;
		try {
			LocalMeshNode = new MeshNodeImpl(args);
		} catch (InvalidParameterException e) {
			System.out.println(e.getMessage());
			return;
		}
		
		// TODO: wait for initial mesh interface object set-up
		try {
			 
			//Thread.sleep(ThreadLocalRandom.current().nextLong(3000, 5001));	// wait 3-5 seconds
			Thread.sleep(INITIAL_THREAD_SLEEP);		// TIMDONE: as cmdline / config parameter
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		synchronized (LocalMeshNode)
		{
			do
			{
				System.out.println(Util.getCurrentTime() + ": In main do-while");
				
				// TODO: improve state machine implementation
				LocalMeshNode.changeLocalPhase();
				
				try {
					System.out.println(Util.getCurrentTime() + ": In main try-wait");
					
					while (!LocalMeshNode.isFree())
					{
						Thread.sleep(WAIT_MESHNODE_IS_FREE);	// TIMDONE: as cmdline / config parameter
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			while (true);
		}
	}
	
	
	/* Private: */
	
	// TIMDRAFT: leaveRequest()
	private boolean leaveRequest()
	{
		isLeavingPossible = false;
		boolean iCanLeave = false;
		long leaveTimeStamp = System.currentTimeMillis();	
		
		if( myClusterheadMAC != null )
		{
			myIF1.unicast(myClusterheadMAC, GlobalDefines.LEAVE_REQUEST_MESSAGE, null);
			
			while( ( System.currentTimeMillis() - leaveTimeStamp ) < GlobalDefines.ACK_TIMEOUT )
			{
				if( isLeavingPossible )
				{
					iCanLeave = true;
					break;
				}else {
					myIF1.unicast(myClusterheadMAC, GlobalDefines.LEAVE_REQUEST_MESSAGE, null);
				}
				
				try{
					Thread.sleep(200);
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}else {
			iCanLeave = true;
		}
		
// 		// this functionality is similar to the breakdown detection and can be used if this doesn't work
//		
//		// myClusterhead can not answer for two reasons:
//		// * he reject my Leaving-Request
//		// * he is no longer reachable
//		// if he isn't reachable anymore, i send a final ping. If he doesn't answer,
//		// it is time for me to join another cluster
//		if( !iCanLeave )
//		{
//			leaveTimeStamp = System.currentTimeMillis();
//			sendPingTo(myClusterheadMAC);
//			
//			while( ( System.currentTimeMillis() - leaveTimeStamp ) < GlobalDefines.PINGPONG_TIMEOUT )
//			{
//				iCanLeave = true;
//				
//				for( int i = 0; i < expectedPongList.size(); i++ )
//				{
//					if( expectedPongList.get(i).contains(myClusterheadMAC) )
//					{
//						iCanLeave = false;
//					}
//				}
//			}
//		}
		
		if( !iCanLeave )
		{
			iCanLeave = true;
			
			for( int i = 0; i < globalClusterInfo.size() ; i++ )
			{
				if( globalClusterInfo.get(i).contains(myClusterheadMAC) )
				{
					iCanLeave = false;
				}
			}
		}
		
		
		
		isLeavingPossible = true;
		return iCanLeave;
	}
	
	private boolean joinRequest( String ch )
	{
		isJoiningPossible = false;
		myIF1.unicast(ch, GlobalDefines.JOIN_REQUEST_MESSAGE, null);
		
		long joinTimeStamp = System.currentTimeMillis();
		boolean newCHagrees = false;
		
		while( ( System.currentTimeMillis() - joinTimeStamp ) < GlobalDefines.ACK_TIMEOUT )
		{
			if( isJoiningPossible && ackCH.equals(ch) ) // && !ackCH.equals(myClusterheadMAC) )
			{
				newCHagrees = true;
				break;
//				isJoiningPossible = false;
			}
			
			try{
				Thread.sleep(200);
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return newCHagrees;
		
	}

	private void setToCF() {
		System.out.println(Util.getCurrentTime() + ": Set to CF");
//		interruptRoamingThread = true;
		myMembership = GlobalDefines.CLUSTERFREE;
//		myLocalPhase = 0;
		myClusterheadMAC = null;
	}
	
	private void setToCM(String chMAC) {
		System.out.println(Util.getCurrentTime() + ": Set to CM");
		myMembership = GlobalDefines.CLUSTERMEMBER;
		myClusterheadMAC = chMAC;
	}
	
	private void setToPCH() {
		System.out.println(Util.getCurrentTime() + ": Set to PCH");
		myMembership = GlobalDefines.PROPOSED_CLUSTERHEAD;
	}
	
	private void setToCH() {
		System.out.println(Util.getCurrentTime() + ": Set to CH");
		myMembership = GlobalDefines.CLUSTERHEAD;
		myClusterheadMAC = myIF1.getMAC();
	}
	
	private void setToMCH() {
		System.out.println(Util.getCurrentTime() + ": Set to Master CH");
		myMembership = GlobalDefines.CLUSTERHEAD;
		myClusterheadMAC = myIF1.getMAC();
		myMasterMAC = myIF1.getMAC();
		synchronized (masterCHList)
		{
			if (!masterCHList.contains(myIF1.getMAC()))
			{
				masterCHList.add(myIF1.getMAC());
			}
		}
	}
	
	
	// 2008 - A cluster driven channel assignment mechanism for wireless mesh networks
	private boolean becomePCHBasedOnNC()
	{
		int neighbourCount = myIF1.getNeighbourCount();
		
		// check NC of all neighbour nodes and compare it to our own (ignore master CH)
		// (NC also called "Highest Connected Node (HCN)" metric in literature)
		synchronized (myNeighbours)
		{
			System.out.println(Util.getCurrentTime() + ": In becomePCHBasedOnNC():"
					+ "\n\tMy NC: " + Integer.toString(neighbourCount)
					+ "\n\tMy MAC Sum: " + Long.toString(myIF1.getMACSum())
			);
			
			for (NeighbourListEntry nle : myNeighbours)
			{
				if (!nle.getMAC().equals(myMasterMAC))
				{
					System.out.println(Util.getCurrentTime() + ": NC comparison to neighbour:"
						+ "\n\tName: " + Util.MACToName(nle.getMAC())
						+ "\n\tMAC: " + nle.getMAC()
						+ "\n\tMembership: " + nle.getMembership()
						+ "\n\tNC: " + Integer.toString(nle.getNeighbourCount())
						+ "\n\tMAC Sum: " + nle.getMACSum()
					);
					
					if (nle.getNeighbourCount() > (neighbourCount))
					{
						System.out.println(Util.getCurrentTime() + ": This one is better suited for PCH, I will stay CF");
						setToCF();
						return false;
					}
					else if (nle.getNeighbourCount() == (neighbourCount))
					{
						System.out.println(Util.getCurrentTime() + ": This one isn't better suited for PCH, we might both become PCH");
					}
					else
					{
						System.out.println(Util.getCurrentTime() + ": This one isn't better suited for PCH");
					}
				}
			}
		}
		System.out.println(Util.getCurrentTime() + ": Nobody around me is better suited for PCH, so I become PCH (as well)");
		setToPCH();
		return true;
	}
	
	
	// TODO: need path info to all nodes before calling this (via temporarily enabled root mode or address range ping)
	private double calculateCentrality()
	{
		double centrality = 0;
		
		List<List<String>> MeshPathCopy = new ArrayList<List<String>>(myIF1.getMeshPaths());
		
		if (MeshPathCopy.isEmpty()) {
			System.out.println(Util.getCurrentTime() + ": In calculateCentrality(): No path info yet!");
			return centrality;
		}
		
		// get path info to calculate average of all ALMs to remote nodes
		List<Double> allALM = new ArrayList<Double>();
		for (int i = 0; i < MeshPathCopy.size(); i++)
		{
			double nodeALM = Double.valueOf(MeshPathCopy.get(i).get(4)).doubleValue();
			allALM.add(nodeALM);
		}
		
		// calculate centrality metric -> C = minALM / meanALM
		
		double minALM = Util.min(allALM);
		double maxALM = Util.max(allALM);
		double meanALM = Util.mean(allALM);
		
		centrality = 1 / meanALM;
		
		System.out.println(Util.getCurrentTime() + ": In calculateCentrality():"
			+ "\n\tminALM: " + minALM
			+ "\n\tmaxALM: " + maxALM
			+ "\n\tmeanALM: " + meanALM
			+ "\n\tcentrality (1/meanALM): " + centrality
		);
		
		return centrality;
	}
	
	//TIMDRAFT: extractCHMessageInfo()
	private ArrayList<String> extractCHMessageInformation(String senderMACAdress, String data )
	{
		ArrayList<String> ClusterInfo = new ArrayList<String>(
				Arrays.asList(data.trim().split(GlobalDefines.MAC_SEPARATOR)));
		
		
		
		ClusterInfo.set(0, senderMACAdress);
		
		if( ClusterInfo.size() > 1 )
		{
			ClusterInfo.remove(ClusterInfo.size()-1);
		}
		
//		System.out.println("Tim: new List -> content: first entry CH, following CMs");
//		
//		for( int i = 0; i < ClusterInfo.size(); i++ )
//		{
//			System.out.println("Tim: Incoming CH_MSG Information -> " + ClusterInfo.get(i));
//		}
//		
		return ClusterInfo;
		
	}
	
	//TIMDONE: getChecksumFromReceivedCHMsg( data )
//	private int getChecksumFromReceivedCHMsg( String data )
//	{
//		int Checksum = 0;
//		
//		ArrayList<String> dataList = new ArrayList<String>(
//				Arrays.asList(data.trim().split(GlobalDefines.CHK_SUM_SEPARATOR)));
//		
////		System.out.println("Tim: Received Checksum -> " + dataList.get(1));
//		
//		Checksum = Integer.valueOf(dataList.get(1));
//		
//		return Checksum;
//	}
	
	//TIMDONE: calculateChecksumFromReceivedCMMACS( data )
//	private int calculateChecksumFromReceivedCMMACS( String data )
//	{
//		int Checksum = 0;
//
//		ArrayList<String> dataList = new ArrayList<String>(
//				Arrays.asList(data.trim().split( "\\" + GlobalDefines.MESSAGE_SEPARATOR + "|" + GlobalDefines.CHK_SUM_SEPARATOR )));
//		
//		if( getChecksumFromReceivedCHMsg(data) > 0 )
//		{
//			Checksum = fletcher16Checksum(dataList.get(2));
//		}
//		
////		System.out.println("Tim: 3. Line from CH-Message -> " + dataList.get(3));
////		System.out.println("Tim: calculated Checksum from MAC-Line -> " + Integer.toString(Checksum));
//		
//		return Checksum;
//		}
	
	
	// TIMDONE: fletcher16Checksum()
	private int fletcher16Checksum(String CHInfoPart2)
	{
//		System.out.println("Tim: Incoming CHInfoPart2 for fletcher16Checksum -> " + CHInfoPart2);
		
		int sum1 = 0;
		int sum2 = 0;
		int length = CHInfoPart2.length();
		
		for( int i = 0; i < length; i++ )
		{
			sum1 = ( ( sum1 + (int)CHInfoPart2.charAt(i) ) % 255 );
			sum2 = ( ( sum2 + sum1 ) % 255 );
		}
		
//		System.out.println("Tim: Calculated fletcher16Checksum -> " + Integer.toString(Integer.parseInt(String.valueOf(sum1+""+sum2))));
		
		return Integer.parseInt(String.valueOf(sum1+""+sum2)); 
		
	}
	
	//TIMDRAFT: checkTimeStamps
//	private void checkTimeStampsAndRemoveObsolete()
//	{
//		long currentTimeStampInMillis = System.currentTimeMillis();
//		ArrayList<ArrayList<String>> globalClusterInfoTmp = new ArrayList<ArrayList<String>>();
//		List<String> AllClusterChannelTmp = new ArrayList<String>();
//		List<Long> globalClusterTimeStampsTmp = new ArrayList<Long>();
//		
//		synchronized ( globalClusterInfo ) 
//		{
//		
//			for( int i = 0; i < globalClusterTimeStamps.size(); i++ )
//			{
//				if( !globalClusterInfo.get(i).get(0).equals(myIF1.getMAC()) )
//				{
//					if( ( System.currentTimeMillis() - globalClusterTimeStamps.get(i) ) < GlobalDefines.TIMESTAMP_RUNNING_OUT)
//					{
//						globalClusterInfoTmp.add(globalClusterInfo.get(i));
//						AllClusterChannelTmp.add(AllClusterChannel.get(i));
//						globalClusterTimeStampsTmp.add(globalClusterTimeStamps.get(i));
//					}
//				}
//			}
//			
//			globalClusterInfo.clear();
//			AllClusterChannel.clear();
//			globalClusterTimeStamps.clear();
//			
//			globalClusterInfo.addAll(globalClusterInfoTmp);
//			AllClusterChannel.addAll(AllClusterChannelTmp);
//			globalClusterTimeStamps.addAll(globalClusterTimeStampsTmp);
//		
//		}
//	}
	
	private void checkNextHopListAndDeleteObsolete()
	{
		
//			blockNextHopList = true;
			
			System.out.println("Clearing obsolete NextHopToCHInfos from older Neighbors");
			
			List<List<String>> nh2chTmp = new ArrayList<List<String>>();
			
			synchronized (myNeighboursNextHopToCH)
			{
				synchronized (myNeighbours)
				{
					for( int i = 0; i < myNeighboursNextHopToCH.size(); i++ )
					{
						
							for(NeighbourListEntry nle : myNeighbours)
							{
								if( nle.getMAC().equals(myNeighboursNextHopToCH.get(i).get(0)) )
								{
									nh2chTmp.add(myNeighboursNextHopToCH.get(i));
								}
							}
						
					}
					
					if( GlobalDefines.DEBUGGING ){for( int i = 0; i < myNeighboursNextHopToCH.size();i++ ) {System.out.println(Util.getCurrentTime() + " --DB-> !CHECKNEXTHOPLISTANDDELETEOBSOLETE! --> OLDNEXTHOPLISTENTRY: " + Integer.toString(i) + " IS " + Util.MACToName(myNeighboursNextHopToCH.get(i).get(0)));}}
					
					if( !blockNextHopList )
					{
						myNeighboursNextHopToCH.clear();
						myNeighboursNextHopToCH.addAll(nh2chTmp);
						
						if( GlobalDefines.DEBUGGING ){for( int i = 0; i < myNeighboursNextHopToCH.size();i++ ) {System.out.println(Util.getCurrentTime() + " --DB-> !CHECKNEXTHOPLISTANDDELETEOBSOLETE! --> NEWNEXTHOPLISTENTRY: " + Integer.toString(i) + " IS " + Util.MACToName(myNeighboursNextHopToCH.get(i).get(0)));}}
		
					}else {
						if( GlobalDefines.DEBUGGING ) {System.out.println(Util.getCurrentTime() + " --DB-> !CHECKNEXTHOPLISTANDDELETEOBSOLETE! --> NEXT HOP LIST WAS BLOCKED");}
					}
				}
			}
		
//			blockNextHopList = false;
			
	}
	
	// TIMDRAFT: performBalancing()
	private void performBalancing()
	{
		if( GlobalDefines.DEBUGGING ){System.out.println(Util.getCurrentTime() + " --DB-> !PERFORMBALANCING()! --> STARTING BALANCING-METHOD ");}
			
		if( myMembership.equals(GlobalDefines.CLUSTERMEMBER) )
		{
			boolean chIsMyDirectNeighbour = false;
			boolean chToJoinIsMyDirectNeighbour = false;
//			boolean newCHagrees = true;
			
			int myClusterLoad = 0;
			
			double nodeQuantity = 0;
			int balancedClusterTarget = 0;
			

			System.out.println(Util.getCurrentTime() + " globalClusterInfo.size(): " + Integer.toString(globalClusterInfo.size()));
			
			for( int i = 0; i < globalClusterInfo.size(); i++ )
			{
				if( globalClusterInfo.get(i).contains(myIF1.getMAC()) )
				{
					myClusterLoad = globalClusterInfo.get(i).size();
				}
				
				nodeQuantity += globalClusterInfo.get(i).size();
			}
			
			balancedClusterTarget = (int)Math.ceil(nodeQuantity / globalClusterInfo.size());

			if( GlobalDefines.DEBUGGING ){ System.out.println(Util.getCurrentTime() + " --DB-> !PERFORMBALANCING()! --> BALANCED CLUSTER TARGET = " + Integer.toString(balancedClusterTarget));}
			
			boolean foundNeigh = false;
			
			String clusterheadTOJoin = "";
//			String clusterChannelToJoin = "";
			
			List<String> proposedCHToJoin = new ArrayList<String>();
			
			ArrayList<ArrayList<String>> proposedRoamingCluster = new ArrayList<ArrayList<String>>();
			ArrayList<String> proposedRoamingClusterChannel = new ArrayList<String>();
			
			for( int i = 0; i < globalClusterInfo.size(); i++ )
			{				
				//TIMDRAFT: now we can hand over CMs to all neighbor-cluster until the own cluster is beneath the balancedClusterTarget
				if( ( globalClusterInfo.get(i).size() < balancedClusterTarget  && ( myClusterLoad > balancedClusterTarget ) ) || 
				  ( ( globalClusterInfo.get(i).size() < ( myClusterLoad -1 ) ) && ( myClusterLoad > ( balancedClusterTarget - 1 ) ) ) 
						)
				{
					if( !globalClusterInfo.get(i).get(0).equals(myClusterheadMAC))
					{
						proposedRoamingCluster.add( globalClusterInfo.get(i) );
						proposedRoamingClusterChannel.add( AllClusterChannel.get(i) );
					}
				}
				
			}
			
//			if( myClusterLoad >= balancedClusterTarget && myMembership.equals(GlobalDefines.CLUSTERMEMBER) )
			if( myMembership.equals(GlobalDefines.CLUSTERMEMBER) )
			{
				for( int i = 0; i < proposedRoamingCluster.size(); i++ )
				{
					for( int j = 0; j < myNeighbours.size(); j++ )
					{
						if( proposedRoamingCluster.get(i).contains(myNeighbours.get(j).getMAC()) )
						{
							foundNeigh = true;
						}	
					}
					
					if( foundNeigh )
					{
						proposedCHToJoin.add(proposedRoamingCluster.get(i).get(0));
//						System.out.println(Util.getCurrentTime() + " add Proposed ClusterHead to roam: " + proposedRoamingCluster.get(i).get(0));
					}
					
					foundNeigh = false;
				}
				

				if( GlobalDefines.DEBUGGING ){	for( int i = 0; i < proposedCHToJoin.size(); i++ ){System.out.println(Util.getCurrentTime() + " --DB-> !PERFORMBALANCING()! --> PROPOSEDCHTOJOIN " + Integer.toBinaryString(i) + " = " + Util.MACToName(proposedCHToJoin.get(i)));}}
				
				
				if( !proposedCHToJoin.isEmpty() )
				{
					clusterheadTOJoin = findClosestNode(proposedCHToJoin);
					
					if( clusterheadTOJoin != null )
					{
						if( GlobalDefines.DEBUGGING ){ System.out.println(Util.getCurrentTime() + " --DB-> !PERFORMBALANCING()! --> CLOSESTCH = " + Util.MACToName(clusterheadTOJoin));}
						
						System.out.println("The CH that I would join is: " + Util.MACToName(clusterheadTOJoin));
						
//						for( int i = 0; i < proposedRoamingCluster.size(); i++ )
//						{
//							if( proposedRoamingCluster.get(i).contains(clusterheadTOJoin) )
//							{
//								clusterChannelToJoin = proposedRoamingClusterChannel.get(i);
//								
//							}	
//						}	
						
//						for (NeighbourListEntry nle : myNeighbours) {
//							if (nle.getMAC().equals(myClusterheadMAC))
//							{
//								chIsMyDirectNeighbour = true;
//							}
//							if (nle.getMAC().equals(clusterheadTOJoin))
//							{
//								chToJoinIsMyDirectNeighbour = true;
//							}
//						}
						
						if( !chIsMyDirectNeighbour || chToJoinIsMyDirectNeighbour )
						{
							// after roaming wait a random time from 0 to 5000 milliseconds to prevent following case:
							// if for example two nodes roam at nearly the same time, it is possible, that they roam back 
							// to their old clusters also in the same time. A vibration behavior could occur and a balanced
							// network could never be achieved
							
							if( GlobalDefines.DEBUGGING ){ System.out.println(Util.getCurrentTime() + " --DB-> !PERFORMBALANCING()! --> TRYING TO ROAM TO " + Util.MACToName(clusterheadTOJoin));}
							
							roam( clusterheadTOJoin );
	
						}
					}
				}
			}
		}
		
		System.out.println(Util.getCurrentTime() + " Balancing-Method complete");
	} //end of performBalancing()
	
	
	// perform roaming in last instance
	private void roam( String chMac)
	{
		if( !chMac.equals(myClusterheadMAC) || myClusterheadMAC==null ) 
		{
			if( GlobalDefines.DEBUGGING ){ System.out.println(Util.getCurrentTime() + " --DB-> !ROAMING(CHMAC)! --> STARTING ROAMING-METHOD (WITH CHMAC)");}
			
			System.out.println("Call Method: Roam with chMac: " + chMac + " as Argument");
			boolean iAmBridgeToCH = false;
//			boolean CHIsMyDirectNeighbour = false;
			boolean myNextHopToCHIsInMyCluster = false;
			
			List<List<String>> MeshPathCopy = new ArrayList<List<String>>(myIF1.getMeshPaths());
			
			List<String> ClusterCopy = new ArrayList<String>();
		
			// copy my Cluster to ClusterCopy
			for( int i = 0; i < globalClusterInfo.size(); i++ )
			{
				if( globalClusterInfo.get(i).contains(myClusterheadMAC) )
				{
					ClusterCopy.addAll(globalClusterInfo.get(i));
				}
			}
			
			synchronized (myNeighboursNextHopToCH)
			{
				for( int i = 0; i < myNeighboursNextHopToCH.size(); i++ )
				{
					// if i am the next hop to ch for any of my neighbors
					
					if( myNeighboursNextHopToCH.get(i).get(1).equals(myIF1.getMAC()) )
					{	
						if( ClusterCopy.contains(myNeighboursNextHopToCH.get(i).get(0)) )
						{
							iAmBridgeToCH = true;
						}
					}
				}
			}
			
			if( GlobalDefines.DEBUGGING ){ System.out.println(Util.getCurrentTime() + " --DB-> !ROAMING(CHMAC)! --> IAMBRIDGE = " + Boolean.toString(iAmBridgeToCH));}
			
			for( int i = 0; i < MeshPathCopy.size(); i++ )
			{
				// search my path (next hop) to my ch
				if( MeshPathCopy.get(i).get(2).equals(myClusterheadMAC) )
				{
					// if next hop is in my cluster
					if( ClusterCopy.contains(MeshPathCopy.get(i).get(3)) )
					{
						myNextHopToCHIsInMyCluster = true;
					}
				}
			}
			
			if( GlobalDefines.DEBUGGING ){ System.out.println(Util.getCurrentTime() + " --DB-> !ROAMING(CHMAC)! --> MYNEXTHOPTOCHISINMYCLUSTER = " + Boolean.toString(myNextHopToCHIsInMyCluster));}
			
	//		for (NeighbourListEntry nle : myNeighbours) {
	//			if (nle.getMAC().equals(myClusterheadMAC))
	//			{
	//				CHIsMyDirectNeighbour = true;
	//			}
	//		}
			
			System.out.println("My ClusterHead is: " + myClusterheadMAC);
			
			if( (( !iAmBridgeToCH || !myNextHopToCHIsInMyCluster ) || myClusterheadMAC==null || iAmIsolated))
			{
				if( roamingPossible || myClusterheadMAC==null )
				{
					if( blockBalancingForOtherNodes )
					{
						myIF1.broadcast(GlobalDefines.ROAMING_FALSE_MESSAGE + GlobalDefines.MESSAGE_SEPARATOR);
					}
					
					System.out.println( Util.getCurrentTime() + "I found: " + Util.MACToName(chMac) + " as my new ClusterHead and join to him");
					
					if( leaveRequest() )
					{
						if( GlobalDefines.DEBUGGING ){ System.out.println(Util.getCurrentTime() + " --DB-> !ROAMING(CHMAC)! --> LEAVEREQUEST WAS SUCCESSFUL ");}
						
						if( joinRequest( chMac ) )
						{
							if( GlobalDefines.DEBUGGING ){	System.out.println(Util.getCurrentTime() + " --DB-> !ROAMING(CHMAC)! --> JOINREQUEST TO " + Util.MACToName(chMac) + " WAS SUCCESSFUL");}
							
							if( myClusterheadMAC != null )
							{
								myIF1.unicast(myClusterheadMAC, GlobalDefines.LEAVE_MESSAGE, null);
							}
							
							myIF1.unicast(chMac, GlobalDefines.JOIN_MESSAGE, null);
							
							if( GlobalDefines.DEBUGGING ){ System.out.println(Util.getCurrentTime() + " --DB-> !ROAMING(CHMAC)! --> SETTOCM, MY NEW CH: " + Util.MACToName(chMac));}
							
							setToCM(chMac);
							
							for( int i = 0; i < globalClusterInfo.size(); i++ )
							{
								if( globalClusterInfo.get(i).get(0) == chMac )
								{
									myClusterChannel = Integer.parseInt(AllClusterChannel.get(i));
								}
							}
										
							System.out.println("Roaming to " + Util.MACToName(chMac) + " was successful");
										
							if( blockBalancingForOtherNodes )
							{
								myIF1.broadcast(GlobalDefines.ROAMING_TRUE_MESSAGE + GlobalDefines.MESSAGE_SEPARATOR);
							}
							
						}
					}
					
	//				myIF1.unicast(chMac, GlobalDefines.JOIN_MESSAGE, null);
	//				
	//				if( myClusterheadMAC != null )
	//				{
	//					myIF1.unicast(myClusterheadMAC, GlobalDefines.LEAVE_MESSAGE, null);
	//				}
					
				}
			}
		}
		
//		sendPingTo(myClusterheadMAC);
		
	}
	
	private void roam()
	{
		List<List<String>> MeshPathCopy = new ArrayList<List<String>>(myIF1.getMeshPaths());

		String currentNeighbour = "";
		String CurrentCH = "";
		String BestCH = "";
		int CurrentCHALM = 0;
		int CHALMTmp = 0;
		myLocalPhase = 7;
		
		System.out.println("The roam-Method was called without a chMac, so I try to find a suitable cluster!!!");
		
		if( !globalClusterInfo.isEmpty() )
		{
			// TIMDRAFT: from all direct neighbors, join to CH with the smallest ALM
			for( int i = 0; i < globalClusterInfo.size(); i++ )
			{
				for( int j = 0; j < myNeighbours.size(); j++)
				{
					currentNeighbour = myNeighbours.get(j).getMAC();
					
//					System.out.println("Current Neighbour to analyse: " + currentNeighbour + " ...and its Name is: " + Util.MACToName(currentNeighbour));
					
					if( globalClusterInfo.get(i).contains(currentNeighbour) ) // && ( !globalClusterInfo.get(i).get(0).equals(myClusterheadMAC) || myMembership.equals(GlobalDefines.CLUSTERFREE) ) )
					{
						CurrentCH = globalClusterInfo.get(i).get(0);
						CHALMTmp = CurrentCHALM;
						
						for( int k = 0; k < MeshPathCopy.size(); k++ )
						{
							if( MeshPathCopy.get(k).get(2).equals(CurrentCH) )
							{
								CurrentCHALM = Integer.valueOf(MeshPathCopy.get(k).get(4)).intValue();
								
								System.out.println("Current CHALM to analyse: " + Integer.toString(CurrentCHALM));
							}
						}
						
						if( CurrentCHALM < CHALMTmp || CHALMTmp == 0 )
						{
							if( myClusterheadMAC == null )
							{
								BestCH = CurrentCH;
								
							}else if( !CurrentCH.equals(myClusterheadMAC) ){
								
								BestCH = CurrentCH;
							}
							
							
							
							System.out.println("My new best CH is CH: " + BestCH + " ...and its name is: " + Util.MACToName(BestCH));
						}
					}
				}
			}
			
			if( !BestCH.isEmpty() )
			{
				roam( BestCH );
			}
		}
	}
	
//	private void checkPongList()
//	{
//		long currentTimeStamp = System.currentTimeMillis();
//		ArrayList<ArrayList<String>> expectedPongListTmp = new ArrayList<ArrayList<String>>();
//		
//		for( int i = 0; i < expectedPongList.size(); i++ )
//		{
//			if( (currentTimeStamp - Long.parseLong(expectedPongList.get(i).get(0))) > GlobalDefines.PINGPONG_TIMEOUT )
//			{
//				// if I am CH and my CM doesn't answer
//				if( myMembership.equals(GlobalDefines.CLUSTERHEAD) )
//				{
//					myCMList.remove(expectedPongList.get(i).get(1));
//				}
//				
//				// if I am CM and the PongListEntry is my CH
//				if( myMembership.equals( GlobalDefines.CLUSTERMEMBER ) && expectedPongList.get(i).get(1).equals(myClusterheadMAC))
//				{
//					roam();
//				}
//			}else {
//				// only "not-out-of-time"-entries will left in expectedPongList
//				expectedPongListTmp.add(expectedPongList.get(i));
//			}
//		}
//		
//		expectedPongList.clear();
//		expectedPongList.addAll(expectedPongListTmp);
//	}
	
//	private void sendPingTo( String macToPing )
//	{
//		long currentTimeStamp = System.currentTimeMillis();
//		ArrayList<String> newPongEntry = new ArrayList<String>();
//		boolean isInList = false;
//		
//		if( GlobalDefines.PERIODICAL_MSG_VIA_TCP)
//		{
//			myIF1.unicast(macToPing, GlobalDefines.PING_MESSAGE, null);
//		}else {
//			
//		}
//		
//		newPongEntry.add(Long.toString(currentTimeStamp));
//		newPongEntry.add(macToPing);
//		
//		for( int i = 0; i < expectedPongList.size(); i++ )
//		{
//			if( expectedPongList.get(i).contains(macToPing) )
//			{
//				isInList = true;
//			}
//		}
//		
//		if( !isInList )
//		{
//			expectedPongList.add(newPongEntry);
//		}
//	}
	
	private void sendPongTo( String macToPong )
	{
		if( GlobalDefines.PERIODICAL_MSG_VIA_TCP)
		{
			myIF1.unicast(macToPong, GlobalDefines.PONG_MESSAGE, null);
		}else {
			
		}
	}
	
//	private boolean isMyCHStillThere()
//	{
//		System.out.println(Util.getCurrentTime() + " Check if my ClusterHead is still there");
//		boolean CHisStillThere = false;
//		boolean myCHKnowsMe = false;
//		
//		for( int i = 0; i < globalClusterInfo.size(); i++ )
//		{
//			if( globalClusterInfo.get(i).contains(myClusterheadMAC) )
//			{
//				if( globalClusterInfo.get(i).contains(myIF1.getMAC()) )
//				{
//					myCHKnowsMe = true;
//				}
//				
//				CHisStillThere = true;
//			}
//		}
//		
//		if( CHisStillThere && myCHKnowsMe)
//		{
//			System.out.println(Util.getCurrentTime() + " Everything is fine with my CH");
//		}
//		
//		if( !CHisStillThere )
//		{
//			System.out.println(Util.getCurrentTime() + " Something is wrong with my CH, sending Ping to him");
//			sendPingTo(myClusterheadMAC);
//		}
//		
//		return CHisStillThere;
//	}
	
//	private boolean areMyCMStillThere()
//	{
//		List<List<String>> MeshPathCopy = new ArrayList<List<String>>(myIF1.getMeshPaths());
//		
//		boolean myCMAreStillThere = true;
//		boolean notAllCMAreReachable = false;
//		boolean cmIsInAnotherCluster = false;
//		boolean cmIsStillThere = true;
//		
//		for( int i = 0; i < globalClusterInfo.size(); i++ )
//		{
//			if( globalClusterInfo.get(i).contains(myIF1.getMAC()) )
//			{
//				// if my cm is also in another cluster
//				for( int j = 1; j < globalClusterInfo.get(i).size(); j++ )
//				{
//					for( int k = 0; k < globalClusterInfo.size(); k++ )
//					{
//						// if not my own cluster
//						if( !globalClusterInfo.get(k).contains(myIF1.getMAC()) )
//						{
//							// if another cluster
//							if( globalClusterInfo.get(k).contains(globalClusterInfo.get(i).get(j)) )
//							{
//								cmIsInAnotherCluster = true;
//								sendPingTo( globalClusterInfo.get(i).get(j) );
//							}
//						}
//					}
//				}
//				
//				// if the next hop to cm is not a cm
//				// starts at 1 because I am the 0 (CH)
//				for( int j = 1; j < globalClusterInfo.get(i).size(); j++ )
//				{
//					for( int k = 0; k < MeshPathCopy.size(); k++ )
//					{
//						if( MeshPathCopy.get(k).get(2).equals(globalClusterInfo.get(i).get(j)) )
//						{
//							if( !globalClusterInfo.get(i).contains(MeshPathCopy.get(k).get(3)) )
//							{
//								notAllCMAreReachable = true;
//								sendPingTo( globalClusterInfo.get(i).get(j) );
//							}
//						}
//					}
//				}
//			}
//		}
//		
//		if( notAllCMAreReachable || cmIsInAnotherCluster )
//		{
//			myCMAreStillThere = false;
//		}
//		
//		return myCMAreStillThere;
//	}
	
//	private void checkNeighbors()
//	{
//		if( myNeighbours.size() == 0 && !myIF1.getMAC().equals(GlobalDefines.EXP_NODE) )
//		{
//			setToCF();
//		}
//	}
	
	// find out if the node is lying in an area with no CH resulting from a "loosing-chain" during PCH race
//	private boolean recognizeWeakOccupiedRegion()
//	{
//		int myClusterLoad = 0;
//		int averageClusterLoadOfAllOtherCluster = 0;
//		
//		// TIMDRAFT: recognition of thin settled regions, used to decide if a new ch is necessary
//		boolean isVillage = false;
//		
//		for( int i = 0; i < globalClusterInfo.size(); i++)
//		{
//			if( globalClusterInfo.get(i).get(0).equals(myClusterheadMAC) )
//			{
//				myClusterLoad = globalClusterInfo.get(i).size();
//				
//				for( int j = 0; j < globalClusterInfo.size(); j++ )
//				{
//					if( !globalClusterInfo.get(0).equals(myClusterheadMAC) )
//					{
//						averageClusterLoadOfAllOtherCluster += globalClusterInfo.get(j).size();
//					}
//					averageClusterLoadOfAllOtherCluster = averageClusterLoadOfAllOtherCluster / (globalClusterInfo.size()-1);
//					
//					if( myClusterLoad > ( 2 * averageClusterLoadOfAllOtherCluster ) )
//					{
//						isVillage = true;
//					}
//				}
//			}
//		}
//		
//		return isVillage;
//	}
	
	
	// find out if the node is isolated (all surrounded nodes are member of other clusters) and returns the best cluster to join
	private boolean roamIfIsolated()
	{
		boolean isIsolated = false;
		boolean myClusterNeighboursNextHopIsInMyCluster = true;
		boolean thereIsNoNeighbourWhichIsAlsoInMyCluster = true;
		
		// MeshPathCopy.get(i).get(0) = MeshPointLocalInterfaceNmae  .get(1) = LocalMAC  .get(2) = DestinationMAC  .get(3) = NextHopMAC  .get(4) = ALM 
		List<List<String>> MeshPathCopy = new ArrayList<List<String>>(myIF1.getMeshPaths());
		
		List<String> ClusterCopy = new ArrayList<String>();
		
		// copy my Cluster to ClusterCopy
		for( int i = 0; i < globalClusterInfo.size(); i++ )
		{
			if( globalClusterInfo.get(i).contains(myClusterheadMAC) )
			{
				ClusterCopy.addAll(globalClusterInfo.get(i));
			}
		}
		
		synchronized (myNeighboursNextHopToCH)
		{
			if( !myNeighboursNextHopToCH.isEmpty() )
			{
				for( int i = 0; i < myNeighboursNextHopToCH.size(); i++ )
				{
					if( ClusterCopy.contains(myNeighboursNextHopToCH.get(i).get(0)) )
					{	
						thereIsNoNeighbourWhichIsAlsoInMyCluster = false;
						
						if( ClusterCopy.contains(myNeighboursNextHopToCH.get(i).get(1)) )
						{
							myClusterNeighboursNextHopIsInMyCluster = false;
						}
					}
				}
			}
		}
		
		
	
		// i am isolated, if the node i use to reach my CH is not in my Cluster
		for( int i = 0; i < MeshPathCopy.size(); i++ )
		{
			if( MeshPathCopy.get(i).get(2).equals(myClusterheadMAC) )
			{
				if( !ClusterCopy.contains(MeshPathCopy.get(i).get(3)) )
				{
					System.out.println("My next Hop Node is: " + MeshPathCopy.get(i).get(3) + " ...and its Name is: " + Util.MACToName(MeshPathCopy.get(i).get(3)) + " ...I AM ISOLATED");
					isIsolated = true;
				}
			}
		}
		
//		myClusterNeighboursNextHopIsInMyCluster = true;
		
		if( ( isIsolated && !myClusterNeighboursNextHopIsInMyCluster ) || 
			( isIsolated && thereIsNoNeighbourWhichIsAlsoInMyCluster ) )
		{
			iAmIsolated = true;
			roam();
			iAmIsolated = false;
		}
		
		return isIsolated;
	}
	
	
	// TIMDONE: need path info to all nodes before calling this (via temporarily enabled root mode or address range ping)	
	private int getNodeQuantity()
	{
		int nodeQuantity = 0;
		
		List<List<String>> MeshPathCopy = new ArrayList<List<String>>(myIF1.getMeshPaths());
		
		if (MeshPathCopy.isEmpty()) {
			System.out.println(Util.getCurrentTime() + ": In getNodeQuantity: No path info yet!");
			return nodeQuantity;
		}
		
		nodeQuantity = MeshPathCopy.size() + 1; // own node + all remote nodes
		
		return nodeQuantity;
	}
	
//	private void sendMyNH2CHToTestPC()
//	{
//		List<List<String>> MeshPathCopy = new ArrayList<List<String>>(myIF1.getMeshPaths());
//		String MyNextHopToCH = "";
//		
//		for( int i = 0; i < MeshPathCopy.size(); i++ )
//		{
//			if( MeshPathCopy.get(i).get(2).equals(myClusterheadMAC) )
//			{
//				MyNextHopToCH = MeshPathCopy.get(i).get(3);
//			}
//		}
//		
////		MyNextHopToCH = myNeighbours.get(0).getMAC();
//		if( !MyNextHopToCH.isEmpty() )
//		{
//			System.out.println("Sending myNextHop To CH To The Test-Pc: NH2CH = " + MyNextHopToCH );
//		
//			myIF1.unicastEthernet(GlobalDefines.TEST_PC_ETH_MAC, GlobalDefines.GAL40_NEXT_HOP_TO_CH + GlobalDefines.MESSAGE_SEPARATOR + MyNextHopToCH, null);
//		}else {
//			if( GlobalDefines.DEBUGGING ){System.out.println(Util.getCurrentTime() + " --DB-> !SENDNH2CH2PC()! --> GOT NO NEXT HOP TO MY CH ");}
//
//		}
//	}
	
	
	/* TIMDONE: Calculate WNPR
	 * 
	 */
	private double calculateWNPR()
	{
		int myNC = myIF1.getNeighbourCount();	// number of all neighbours (neighbour count)
		
		int myPCHNC = 0;						// number of PCH neighbours (PCH neighbour count)
		synchronized (myNeighbours)
		{
			// TODO: count Master CH as "PCH" neighbour as well
			for (NeighbourListEntry nle : myNeighbours) {
				if (nle.getMembership().equals(GlobalDefines.PROPOSED_CLUSTERHEAD) ||
					nle.getMAC().equals(myMasterMAC))
				{
					myPCHNC += 1;
				}
			}
		}
		
		/*  
		 *  TIMDONE: WNPR at 22.05.2018
		 *  - NPR is now normalized to the count of nodes in the whole network 
		 *  - NPR-range: 0->no neighbours ... ((PCHNC+1)^-1)->all nodes in the network are direct reachable neighbours
		 *  - (+1) ensures the robustness in cases of missing PCH
		 *  - Centrality Ratio = (Centrality/MasterCentrality)^(CentralityPower) .. 
		 */
		
		double myNPR = (double) ( myNC ) / ( ( myPCHNC + 1 ) * myNodeQuantity );
		double myCentralityRatio = Math.pow(( myCentrality / myMasterCentrality), (double) GlobalDefines.CENTRALITY_POWER );
		/*
		 *  the higher the CENTRALITY_POWER, the fewer is the centrality-influence
		 */ 
		
		//double myWeightedNPR = myNPR + ( myCentrality / GlobalDefines.CENTRALITY_WEIGHT );
		double myWeightedNPR = myNPR * myCentralityRatio;
		
		// TIMDONE: extended at 22.05.2018 by the NodeQuantity
		System.out.println(Util.getCurrentTime() + ": PCH: in calculateWNPR()"
				+ "\n\tMy NC: " + Integer.toString(myNC)
				+ "\n\tMy PCH NC: " + Integer.toString(myPCHNC)
				+ "\n\tMy NodeQuantity: " + Integer.toString(myNodeQuantity)
				+ "\n\tMy NPR: " + Double.toString(myNPR)
				+ "\n\tMy Centrality: " + Double.toString(myCentrality)
				+ "\n\tMy Master Centrality: " + Double.toString(myMasterCentrality)
				+ "\n\tMy Centrality Power.: " + Integer.toString(GlobalDefines.CENTRALITY_POWER)
				+ "\n\tMy Centrality Ratio: " + Double.toString(myCentralityRatio)
				+ "\n\tMy Weighted NPR: " + Double.toString(myWeightedNPR)
				+ "\n\tMy MAC Sum: " + Long.toString(myIF1.getMACSum())
		);
		
		return myWeightedNPR;
	}
	
	
	// 2008 - A cluster driven channel assignment mechanism for wireless mesh networks
	private boolean joinClusterBasedOnALM()
	{
		System.out.println(Util.getCurrentTime() + ": In joinClusterBasedOnALM()");		
		String closestCH = null;
		
		List<String> chList;
		synchronized (myCHList) {
			chList = new ArrayList<String>(myCHList);
		}
		
		if (!chList.isEmpty())
		{
			closestCH = findClosestCHpreferNeighbourhood(chList);
			
			// TIM: join message
			// TODO: we should also check our neighbours' info to decrease chance for split clusters (IF2 isolation of CM groups)
			// -> perform virtual choice of "closest" CH already during phase 3 while receiving CH announcements
			// -> all CF with a CH as neighbour(!) continuously announce current join decision to their remaining neighbours
			// -> these "single-hop CF" only choose between neighbouring CH based on ALM, not considering other neighbours' choices
			// -> if there is a "closer" CH neighbour, they update their choice and announce it
			// -> CH reachability will propagate from "single-hop CF" (with CH neighbour) to CF in multi-hop distance to all CH
			// -> "multi-hop CF" (without CH neighbour) update their own choice depending on their neighbours' choices (and propagate it)
			// -> "multi-hop CF" should prefer those "close" CH that will also be reachable via neighbours (i.e., at least one neighbour)
			
			// TODO: allow for subsequent cluster balancing after an initial clustering run
			// -> CH include the current number of CM ("cluster size") in their CH announcement messages
			// -> CM (or "late" CF) not only check proximity before joining but also consider cluster size
			// -> within a certain proximity tolerance, joining "smaller" clusters should be preferred
			
			if (closestCH != null)
			{
				setToCM(closestCH);
				myIF1.unicast(myClusterheadMAC, GlobalDefines.JOIN_MESSAGE, null);
				System.out.println(Util.getCurrentTime() + ": Successfully joined CH " + Util.MACToName(myClusterheadMAC));
				return true;
			}
		}
		
		System.out.println(Util.getCurrentTime() + ": Could not join any cluster");
		return false;
	}
	
	
	// returns MAC of "closest" CH (smallest ALM, highest MAC sum decides if metrics are equal)
	// -> CH within neighbourhood are preferred over those outside neighbourhood (still considering ALM)
	private String findClosestCHpreferNeighbourhood(List<String> chList)
	{
		if (!chList.isEmpty())
		{
			List<String> singleHopCH = new ArrayList<String>();	// CHs within neighbourhood
			List<String> multiHopCH = new ArrayList<String>();	// CHs outside neighbourhood
			
			Vector<NeighbourListEntry> NeighboursCopy;
			synchronized (myNeighbours) {
				NeighboursCopy = new Vector<NeighbourListEntry>(myNeighbours);
			}
			
			// fill singleHopCH and multiHopCH
			for (String chMAC : chList)
			{
				boolean found = false;
				for (NeighbourListEntry nle : NeighboursCopy)
				{
					if (nle.getMAC().equals(chMAC))
					{
						singleHopCH.add(chMAC);
						found = true;
						break;
					}
				}
				if (!found) {
					multiHopCH.add(chMAC);
				}
			}
			
			// prefer singleHopCH
			List<String> consideredCH;
			if (!singleHopCH.isEmpty())
			{
				consideredCH = singleHopCH;
				
				// check if Master CH is among my neighbours
				// -> prefer joining Master CH to reduce future cluster info sync effort of "aux" CH
				// -> this prevents CH in vicinity of the Master to "steal" intermediate CF from the central cluster
				for (String mac : consideredCH) {
					if (mac.equals(myMasterMAC)) {
						System.out.println(Util.getCurrentTime() + ": Master CH is my neighbour!");
						return myMasterMAC;
					}
				}
			}
			else
			{
				consideredCH = multiHopCH;
			}
			
			String singleHopCHStr = "";
			String multiHopCHStr = "";
			String consideredCHStr = "";
			for (String mac : singleHopCH)
				singleHopCHStr += Util.MACToName(mac) + " ";
			for (String mac : multiHopCH)
				multiHopCHStr += Util.MACToName(mac) + " ";
			for (String mac : consideredCH)
				consideredCHStr += Util.MACToName(mac) + " ";
			System.out.println(Util.getCurrentTime() + ": In findClosestCHpreferNeighbourhood():"
					+ "\n\tNeighbour CH: " + singleHopCHStr
					+ "\n\tNon-Neighbour CH: " + multiHopCHStr
					+ "\n\tConsidered CH: " + consideredCHStr
			);
			
			// find closest CH among consideredCH
			return findClosestNode(consideredCH);
		}
		return null;
	}
	
	
	// returns MAC of "closest" Node (smallest ALM, highest MAC sum decides if metrics are equal)
	private String findClosestNode(List<String> nodeList)
	{
		if (!nodeList.isEmpty())
		{
			if( nodeList.size() == 1 )
			{
				return nodeList.get(0);
			}
			// find closest CH ...
			String closestNodeMAC = null;
			
			int alm = Integer.MAX_VALUE;
			long macSum = Long.MAX_VALUE;
			
			List<List<String>> MeshPathCopy = new ArrayList<List<String>>(myIF1.getMeshPaths());
			
			for (String chMAC : nodeList)
			{
				for (int i = 0; i < MeshPathCopy.size(); i++)
				{
					// prefer smallest ALM to CH, then highest MAC sum of CH to decide
					String nodeMAC = MeshPathCopy.get(i).get(2);
					long nodeMACSum = Util.calculateMACSum(nodeMAC);
					int nodeALM = Integer.valueOf(MeshPathCopy.get(i).get(4)).intValue();
					
					if ((nodeMAC.equals(chMAC)) &&
						((nodeALM < alm) || ((nodeALM == alm) && (nodeMACSum > macSum))))
					{
						alm = nodeALM;
						macSum = nodeMACSum;
						closestNodeMAC = nodeMAC;
					}
				}
			}
			if (closestNodeMAC != null)
			{
				System.out.println(Util.getCurrentTime() + ": In findClosestNode():"
						+ "\n\tClosest CH: " + Util.MACToName(closestNodeMAC)
						+ "\n\tALM: " + alm
				);
				return closestNodeMAC;
			}
		}
		return null;
	}
		
	
	// Select new cluster channel
	// -> performed by CHs in phase 5, setting is applied in phase 6
	// -> takes as input the channel that was chosen by the previous CH in the channel select sequence
	// -> the first CH in the sequence calls this with GlobalDefines.CHANNEL_SEQUENCE[GlobalDefines.CHANNEL_SEQUENCE.length-1]
	private boolean selectNewClusterChannel(int prevChannel)
	{
		int newChannel = 0;
		
		// select next channel from sequence
		for (int i = 0; i < GlobalDefines.CHANNEL_SEQUENCE.length; i++)
		{
			if (GlobalDefines.CHANNEL_SEQUENCE[i] == prevChannel)
			{
				// -> this always selects the next index channel after prevChannel
				// -> starts again at index 0 when prevChannel index was GlobalDefines.CHANNEL_SEQUENCE.length-1
				newChannel = GlobalDefines.CHANNEL_SEQUENCE[(i + 1) % GlobalDefines.CHANNEL_SEQUENCE.length];
			}
		}
		if (newChannel != 0) // we found a valid successor to prevChannel in the predefined CHANNEL_SEQUENCE
		{
			myClusterChannel = newChannel; // TODO: get rid of redundant variable "myClusterChannel"
			System.out.println(Util.getCurrentTime() + ": Selected new channel " + Integer.toString(newChannel));
			return true;
		}
		else // did not find a channel based on prevChannel
		{
			System.out.println(Util.getCurrentTime() + ": ERROR! Previous channel is not in sequence!");
			return false;
		}
	}
	
	
	// Apply cluster channel switch
	// TODO: we currently use IF1 as cluster interface; IF2 only available on some core grid nodes to enable CH-2-CH sync
	//		 -> IF2 initialized in webserver/client scripts, configured to GlobalDefines.COMMON_CHANNEL and IP 192.168.124.X
	// TODO: equip all nodes with 2 PHY; use MeshInterface object for IF2 and its setChannel() function
	private void setClusterChannel(int channel)
	{
		if (!SKIP_SETCHANNEL)
		{
			myIF1.setChannel(channel);	// TODO: myIF2
		}
		
		myClusterChannel = channel;		// TODO: get rid of redundant variable "myClusterChannel"
		
		return;
	}
	
//	private void plotClusterInfo()
//	{
//		synchronized (globalClusterInfo) 
//		{
//			if( !globalClusterInfo.isEmpty() )
//			{
//				System.out.println("Tim: AllClusterTest New Print Run_____________________________: ");
//				
//				for( int i = 0;  i < globalClusterInfo.size(); i++)
//				{
//					System.out.println("Tim: AllClusterTest Cluster " + Integer.toString(i+1) + " :");
//					
//					for( int j = 0; j < globalClusterInfo.get(i).size(); j++ )
//					{
//						if( j == 0 )
//						{
//							System.out.println(Util.getCurrentTime() + " AllClusterTest CH-MAC: " + globalClusterInfo.get(i).get(j));
//						}
//						else
//						{
//							System.out.println(Util.getCurrentTime() + " AllClusterTest CM-MAC: " + globalClusterInfo.get(i).get(j));
//						}
//					}
//				}
//				
//				
//				System.out.println("Tim: myMac --> " + myIF1.getMAC() );
//				System.out.println("Tim: myClusterLoad (incl. ClusterHead) --> " + Integer.toString(myClusterLoad));
//			}
//		}
//	}
	
	// TIMDRAFT: Roaming Done File
	private void updateRoamingDoneFile()
	{	
		roamingDoneTime = System.currentTimeMillis();
		long roamingDuration = (roamingDoneTime-roamingStartTime) / 1000;
		
		try {
			
			File doneFile = new File(GlobalDefines.DONE_FILE_PATH + "roaming.done");
			doneFile.createNewFile();
			
			// write status info to "done" file
			FileWriter roamingFW = new FileWriter(doneFile);
			
			roamingFW.write("roamingDuration: "	+ Long.toString(roamingDuration) + System.lineSeparator());
			
			roamingFW.write(System.lineSeparator());
			
			roamingFW.write("myName: "				+ Util.MACToName(myIF1.getMAC()) + System.lineSeparator());
			roamingFW.write("myMAC: "				+ myIF1.getMAC() + System.lineSeparator());
			roamingFW.write("myIP: "				+ myIF1.getIP() + System.lineSeparator());
			
			roamingFW.write(System.lineSeparator());
			
			roamingFW.write("myMembership: "		+ myMembership + System.lineSeparator());
			
			roamingFW.write(System.lineSeparator());
			
			roamingFW.write("myMasterName: "		+ Util.MACToName(myMasterMAC) + System.lineSeparator());
			roamingFW.write("myMasterMAC: "		+ myMasterMAC + System.lineSeparator());
			roamingFW.write("myMasterIP: "			+ Util.MACToIP(myMasterMAC) + System.lineSeparator());
			
			roamingFW.write(System.lineSeparator());
			
			roamingFW.write("myClusterheadName: "	+ Util.MACToName(myClusterheadMAC) + System.lineSeparator());
			roamingFW.write("myClusterheadMAC: "	+ myClusterheadMAC + System.lineSeparator());
			roamingFW.write("myClusterheadIP: "	+ Util.MACToIP(myClusterheadMAC) + System.lineSeparator());
			
			roamingFW.write(System.lineSeparator());
			
			roamingFW.write("myClusterChannel: "	+ Integer.toString(myClusterChannel) + System.lineSeparator());
			
			roamingFW.write(System.lineSeparator());
			
			if (!myCMList.isEmpty())
			{
				roamingFW.write("myCMList:");
				for (int i=0; i<myCMList.size(); i++) {
					roamingFW.write(" " + Util.MACToName(myCMList.get(i)));
				}
				roamingFW.write(System.lineSeparator());
			}
			
			if (!myCHList.isEmpty())
			{
				roamingFW.write("myCHList:");
				for (int i=0; i<myCHList.size(); i++) {
					roamingFW.write(" " + Util.MACToName(myCHList.get(i)));
				}
				roamingFW.write(System.lineSeparator());
			}
			
			roamingFW.write(System.lineSeparator());
			
			roamingFW.write("isMaster: "			+ Boolean.toString(isMaster()) + System.lineSeparator());
			
			if (!masterCHList.isEmpty())
			{
				roamingFW.write("masterCHList:");
				for (int i=0; i<masterCHList.size(); i++) {
					roamingFW.write(" " + Util.MACToName(masterCHList.get(i)));
				}
				roamingFW.write(System.lineSeparator());
			}
			
			if (!masterCHChannelList.isEmpty())
			{
				roamingFW.write("masterCHChannelList:");
				for (int i=0; i<masterCHChannelList.size(); i++) {
					roamingFW.write(" " + masterCHChannelList.get(i));
				}
				roamingFW.write(System.lineSeparator());
			}
			
			roamingFW.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	private boolean isMaster()
	{
		if (myMembership.equals(GlobalDefines.CLUSTERHEAD) &&
			myMasterMAC != null &&
			myMasterMAC.equals(myIF1.getMAC()))
		{
			return true;
		}
		return false;
	}
	
	
	private boolean virgin;
	
	
	// start/done times (milliseconds since epoch) to determine clustering duration in phase 7
	private long startTime;
	private long doneTime;
	
	private long roamingDoneTime;
	private long roamingStartTime;
	
//	private long bewareOfBackRoamingWhileBalancingTimeStamp;
	private long lastLeave;
	
	
	private int myLocalPhase;
	
	private MeshInterface myIF1;
	
	private Vector<NeighbourListEntry> myNeighbours;	// extended mesh peer info for clustering algorithm
	
	// TODO: use "MeshInterface" object for IF2
//	private MeshInterface myIF2;
//	private String myIF2Name;							// TODO: currently unused
//	private String myIF2MeshID;							// TODO: currently unused
//	private String CHInfoMessage;						// contains CH|timestamp|channel|CM_MAC(i)alpha(i)xxCM_MAC(i+1)alpha(i+1)...yyChecksum with xx and yy as delimiter

	private int myClusterChannel;						// TODO: should be stored only in cluster IF
	
	private List<String> AllClusterChannel;				// TIM: Channel-Collection based on incoming CH-Messages
	private ArrayList<ArrayList<String>> clusterheadBlackList;
	
	private String myMembership;
	
	private double myCentrality;						// TODO: calculate myCentrality only once during SYNC phase while root mode is active
	
	private int myNodeQuantity;							// TODO: myNodeQuantity gets the the number of all nodes in the Network via function getNodeQuantity()
	
	private String myMasterMAC;							// TODO: IF1 info of current master CH
	private double myMasterCentrality;
	
	private String myClusterheadMAC;					// TODO: IF1 info of current CH (associated cluster)
	private String myNextHopToClusterHead;
	
	private List<String> masterCHList;					// TODO: IF1 info of CHs (used on master only)
	private List<String> masterCHChannelList;			// channel selection sequence of CHs, received by master at the end of phase 5
	
	private List<String> myCHList;						// TODO: IF1 info of CHs (used on all node types)
	private List<String> myCMList;						// TODO: IF1 info of CMs (used on CH only)
	
	private List<String> myClusteringDoneList;
	
//	private int myClusterLoad;							// TIM: number of ClusterMembers + ClusterHead in my Cluster
	
	private ArrayList<ArrayList<String>> globalClusterInfo;		// TIM: filled in case of incoming CH_MSG with all information about CHs and CMs (MAC-Adresses)
	private List<List<String>> myNeighboursNextHopToCH;
	private ArrayList<Long> globalClusterTimeStamps;	// TIM: will be updated for every incoming CH-Message
	
	private ArrayList<ArrayList<String>> expectedPongList;
//	private ArrayList<ArrayList<String>> expectedJoinACKList;
	
	private boolean isLeavingPossible;
	private boolean isJoiningPossible;
	
	private boolean iAmIsolated;
	
	private boolean blockNextHopList;
	
	private String ackCH;
	
	private boolean isSyncRunning;
	
	private boolean blockSetToCF;
	private int mySyncCounter;
	private Object handleSyncMon;

	private Timer myTimer;
//	private Timer roamingTimer;
	
//	private Object waitForCHMon;						// TODO: monitor object, used in phase 0 to wait for incoming CH announcements
	
	private boolean amIFree;							// TODO: get rid of pseudo mutex "amIFree"
	
//	private boolean interruptRoamingThread;				// TIMINFO: to cancel the Roaming-Balancing-Thread
	
	private boolean roamingPossible;					// TIMINFO: to avoid Roaming while other is roaming
	private boolean blockBalancingForOtherNodes;		// TIMINFO: set before starting Roaming-Balancing-Thread
	
	private boolean SKIP_SETCHANNEL;
	private boolean SKIP_MONITORING;
	
	private int UPDATE_THREAD_PERIOD;
	
	private int NEIGH_INF_THREAD_SLEEP;
	
	private int ROAMING_THREAD_SLEEP;
	
	private int SYNC_BROADCAST_PERIOD;
	private int SYNC_THRESHOLD;
	
	private int NC_UNICAST_PERIOD;
	
	private int CH_BROADCAST_PERIOD;
	private int CH_NOTIFICATION_TIMEOUT;
	
	private int MASTER_PHASE_BROADCAST_PERIOD;
	private int MASTER_PHASE_BROADCAST_TRIES;
	
//	private int PHASE0_DELAY;							// TODO: currently unused
	private int PHASE1_DELAY;							// TODO: currently unused
	private int PHASE2_DELAY;
	private int PHASE3_DELAY;
	private int PHASE4_DELAY;
	private int PHASE5_DELAY;
	private int PHASE6_DELAY;							// TODO: currently unused
//	private int PHASE7_DELAY;							// TODO: currently unused
	
	private int MONITORING_DELAY;
	
	private int ROAMING_DELAY;
	
	
	private ScheduledThreadPoolExecutor myExecutor;
	
	private Runnable neighborListUpdate;
	private ScheduledFuture<?> neighborListUpdateFuture;
	
	private Runnable roamingBalancing;
	private ScheduledFuture<?> roamingBalancingFuture;
	
	private Runnable syncBroadcast;
	private ScheduledFuture<?> syncBroadcastFuture;
	
	private Runnable neighbourCountUnicast;
	private ScheduledFuture<?> neighbourCountUnicastFuture;
	
	private Runnable masterPhaseBroadcast;
	private ScheduledFuture<?> masterPhaseBroadcastFuture;
	
	private Runnable clusterHeadBroadcast;
	
	private ScheduledFuture<?> clusterHeadBroadcastFuture;
	
	private Runnable webserverThread;
	private ScheduledFuture<?> webserverThreadFuture;
	
	private Runnable webclientThread;
	private ScheduledFuture<?> webclientThreadFuture;
	
}
