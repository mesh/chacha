package Implementations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.google.gson.*;

import Interfaces.MeshPathListEntry;
import Interfaces.StationListEntry;
import Implementations.ProcessCall;

import Global.GlobalDefines;

public class GetMeshInfo
{
	private Gson gson;

	private List<List<String>> meshPeers;
	private List<List<String>> meshPaths;

	private ScheduledThreadPoolExecutor myExecutor;
	private Runnable updateThread;
	
	private String myIFName;
	
	private int myUPDATE_THREAD_PERIOD;
	
	public GetMeshInfo(String ifname, Integer UPDATE_THREAD_PERIOD)
	{
		
		myIFName = new String(ifname);
		
		myUPDATE_THREAD_PERIOD = UPDATE_THREAD_PERIOD;
		
		gson = new Gson();

		meshPeers = new ArrayList<List<String>>();
		meshPaths = new ArrayList<List<String>>();

		myExecutor = new ScheduledThreadPoolExecutor(1);
		myExecutor.setRemoveOnCancelPolicy(true);

		updateThread = new Runnable() {
			@Override
			public void run() {
				try {
					updateFunction();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};

		myExecutor.scheduleAtFixedRate(updateThread, 0, myUPDATE_THREAD_PERIOD, TimeUnit.MILLISECONDS);	// TIMDONE: as cmdline / config parameter
	}
	
	private void updateFunction()
	{
		// use iw to get mesh info

		List<List<String>> meshPeersTmp = new ArrayList<List<String>>();
		List<List<String>> meshPathsTmp = new ArrayList<List<String>>();

		List<StationListEntry> peerList = getMeshPeerList();
		List<MeshPathListEntry> pathList = getMeshPathList();

		for (StationListEntry sle : peerList)
		{
			// Add only established links
			if( sle.getMeshPlinkState().equals("ESTAB") && sle.getInactiveTime() < GlobalDefines.PLINK_MAX_INACTIVE_TIME)
			{
				List<String> peerStringList = new ArrayList<String>();
	
				peerStringList.add(sle.getMeshPointLocalInterfaceName());
				peerStringList.add(sle.getMeshPointLocalMacAddress());
				peerStringList.add(sle.getMeshPointRemoteMacAddress());
				peerStringList.add(sle.getMeshPlinkState());
				peerStringList.add(String.valueOf(sle.getInactiveTime()));
				peerStringList.add(String.valueOf(sle.getSignalStrengthAvg()));
				peerStringList.add(String.valueOf(sle.getTxBitrate()));
				
				meshPeersTmp.add(peerStringList);
			}
		}

		for (MeshPathListEntry mple : pathList)
		{
			List<String> pathStringList = new ArrayList<String>();

			pathStringList.add(mple.getMeshPointLocalInterfaceName());
			pathStringList.add(mple.getMeshPointLocalMacAddress());
			pathStringList.add(mple.getDestinationMacAddress());
			pathStringList.add(mple.getNextHopMacAddress());
			pathStringList.add(String.valueOf(mple.getMetric()));
			//pathStringList.add(String.valueOf(mple.getExpirationTime()));

			meshPathsTmp.add(pathStringList);
		}
		
		System.out.println(Util.getCurrentTime() + ": I called the updateFunction() from the GetMeshInfo-Class to update my MeshPeers and MeshPaths.");
		if( !meshPeersTmp.isEmpty() )
		{
			System.out.println(Util.getCurrentTime() + ": these are my current MeshPeers: ");
			for( int i = 0; i < meshPeersTmp.size(); i++ )
			{
				System.out.println("MeshPeer " + Integer.toString(i) + " : " + Util.MACToName(meshPeersTmp.get(i).get(2)));
			}
			System.out.println("");
		}
		
		setMeshPeers(meshPeersTmp);
		setMeshPaths(meshPathsTmp);
	}
	
	
	private List<StationListEntry> getMeshPeerList()
	{
		List<StationListEntry> meshPeerList = new ArrayList<StationListEntry>();

		Scanner scan = null;

		try {
			scan = ProcessCall.runCommandWithOutput("./iw_mod", GlobalDefines.toolPath, "--java", "dev", myIFName, "station", "dump");
			String rawDump = scan.nextLine();

			meshPeerList = new ArrayList<StationListEntry>(
					Arrays.asList(gson.fromJson(rawDump, StationListEntryImpl[].class)));

			// removes all null-elements
			// TODO: check if working correctly
			meshPeerList.removeAll(Collections.singleton(null));

		} catch (Exception e) {
			if (scan != null)
				scan.close();
			System.out.println("\nException in getMeshPeerList()\n");
//			e.printStackTrace(); // TODO: remove debug print
			return new ArrayList<StationListEntry>();
		}

		if (scan != null)
			scan.close();
		return meshPeerList;
	}
	
	
	private List<MeshPathListEntry> getMeshPathList()
	{
		List<MeshPathListEntry> meshPathList = new ArrayList<MeshPathListEntry>();

		Scanner scan = null;

		try {
			scan = ProcessCall.runCommandWithOutput("./iw_mod", GlobalDefines.toolPath, "--java",
					"dev", myIFName, "mpath", "dump");

			String rawDump = scan.nextLine();

			meshPathList = new ArrayList<MeshPathListEntry>(
					Arrays.asList(gson.fromJson(rawDump, MeshPathListEntryImpl[].class)));

			meshPathList.removeAll(Collections.singleton(null));

		} catch (Exception e) {
			if (scan != null)
				scan.close();
			System.out.println("\nException in getMeshPathList()\n");
//			e.printStackTrace(); // TODO: remove debug print
			return new ArrayList<MeshPathListEntry>();
		}

		if (scan != null)
			scan.close();
		return meshPathList;
	}

	
	public synchronized List<List<String>> getMeshPeers() {
		return meshPeers;
	}
	
	public synchronized List<List<String>> getMeshPaths() {
		return meshPaths;
	}
	
	public synchronized void setMeshPeers(List<List<String>> meshPeers) {
		this.meshPeers = meshPeers;
	}
	
	public synchronized void setMeshPaths(List<List<String>> meshPaths) {
		this.meshPaths = meshPaths;
	}
	
}
