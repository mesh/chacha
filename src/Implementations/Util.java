package Implementations;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import Global.GlobalDefines;


public class Util
{

	public static String getCurrentTime() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		return sdf.format(cal.getTime());
	}
	
	public static String getCurrentTimeInMillisec() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSSS");
		return sdf.format(cal.getTime());
	}
	
	public static Integer getTimeStampInSeconds() {
		return (int) (System.currentTimeMillis() / 1000L);
	}
	

	public static long calculateMACSum(String mac)
	{
		long macSum = 0;
		String[] splitString = mac.split(":");
		for (int i = 0; i < splitString.length; i++) {
			macSum += (Long.parseLong("+" + splitString[i].replaceAll("\\s+", ""), 16)) << ((splitString.length - i - 1) * 8);
		}
		return macSum;
	}
	
	
	public static double max(List<Double> list) {
	    double max = Double.MIN_VALUE;
	    for (Double elem : list) {
	    	if (elem > max) {
	    		max = elem;
	    	}
	    }
	    return max;
	}
	
	
	public static double min(List<Double> list) {
	    double min = Double.MAX_VALUE;
	    for (Double elem : list) {
	    	if (elem < min) {
	    		min = elem;
	    	}
	    }
	    return min;
	}
	
	
	public static double mean(List<Double> list) {
	    double sum = 0;
	    for (Double elem : list) {
	        sum += elem;
	    }
	    return sum / list.size();
	}
	
	
	public static double median(List<Double> list) {
		Collections.sort(list);
	    int middle = list.size()/2;
	    if (list.size()%2 == 1) {
	        return list.get(middle);
	    } else {
	        return (list.get(middle-1) + list.get(middle)) / 2.0;
	    }
	}
	
	
	// TODO: improve function to cover all 2.4/5 GHz channels
	public static int channel2Freq(int channel)
	{
		int freq = 0;
		
		if (channel == GlobalDefines.COMMON_CHANNEL) {
			// common channel freq resolution
			return GlobalDefines.COMMON_CHANNEL_FREQ;
		}
		else {
			// channel pool freq resolution
			for (int i = 0; i < GlobalDefines.CHANNEL_SEQUENCE.length; i++) {
				if (GlobalDefines.CHANNEL_SEQUENCE[i] == channel)
					return GlobalDefines.CHANNEL_FREQ_SEQUENCE[i];
			}
		}
		
		return freq;
	}
	
	
	public static String MACToName(String MAC)
	{
        String name = null;

		switch (MAC)
		{
			case "04:f0:21:10:c3:23": // Galileo 1
				name = "G1";
				break;
				
			case "04:f0:21:10:c3:20": // Galileo 2
				name = "G2";
				break;
				
			case "04:f0:21:10:c8:0f": // Galileo 3
				name = "G3";
				break;
				
			case "04:f0:21:10:c3:4f": // Galileo 4
				name = "G4";
				break;
				
			case "04:f0:21:10:c2:90": // Galileo 5
				name = "G5";
				break;
				
			case "04:f0:21:10:c2:8a": // Galileo 6
				name = "G6";
				break;
				
			case "04:f0:21:10:c8:18": // Galileo 7
				name = "G7";
				break;
				
			case "04:f0:21:10:c3:2b": // Galileo 8
				name = "G8";
				break;
				
			case "04:f0:21:10:c8:15": // Galileo 9
				name = "G9";
				break;
				
			case "04:f0:21:10:c3:57": // Galileo 10
				name = "G10";
				break;
				
			case "04:f0:21:10:c3:41": // Galileo 11
				name = "G11";
				break;
				
			case "04:f0:21:10:c3:71": // Galileo 12
				name = "G12";
				break;
				
			case "04:f0:21:10:c3:2a": // Galileo 13
				name = "G13";
				break;
				
			case "04:f0:21:10:c3:62": // Galileo 14
				name = "G14";
				break;
				
			case "04:f0:21:10:c3:55": // Galileo 15
				name = "G15";
				break;
				
			case "04:f0:21:10:c2:8e": // Galileo 16
				name = "G16";
				break;
				
			case "04:f0:21:0f:77:54": // Galileo 17
				name = "G17";
				break;
				
			case "04:f0:21:10:c3:39": // Galileo 18
				name = "G18";
				break;
				
			case "04:f0:21:10:c3:33": // Galileo 19
				name = "G19";
				break;
				
			case "04:f0:21:10:c5:af": // Galileo 20
				name = "G20";
				break;
				
			case "04:f0:21:10:c3:3f": // Galileo 21
				name = "G21";
				break;
				
			case "04:f0:21:10:c3:51": // Galileo 22
				name = "G22";
				break;
				
			case "04:f0:21:10:c3:4c": // Galileo 23
				name = "G23";
				break;
				
			case "04:f0:21:10:c2:88": // Galileo 24
				name = "G24";
				break;
				
			case "04:f0:21:10:c8:0d": // Galileo 25
				name = "G25";
				break;
				
			case "04:f0:21:10:c3:53": // Galileo 26
				name = "G26";
				break;
				
			case "04:f0:21:10:c5:52": // Galileo 27
				name = "G27";
				break;
				
			case "04:f0:21:10:c8:17": // Galileo 28
				name = "G28";
				break;
				
			case "04:f0:21:10:c5:51": // Galileo 29
				name = "G29";
				break;
				
			case "04:f0:21:10:c3:59": // Galileo 30
				name = "G30";
				break;
				
			case "04:f0:21:10:c3:52": // Galileo 31
				name = "G31";
				break;
				
			case "04:f0:21:10:c3:54": // Galileo 32
				name = "G32";
				break;
				
			case "04:f0:21:10:c3:38": // Galileo 33
				name = "G33";
				break;
				
			case "04:f0:21:10:c3:65": // Galileo 34
				name = "G34";
				break;
				
			case "04:f0:21:10:c3:21": // Galileo 35
				name = "G35";
				break;
				
			case "04:f0:21:10:c5:53": // Galileo 36
				name = "G36";
				break;
				
			case "04:f0:21:10:c8:03": // Galileo 39
				name = "G39";
				break;
				
			case "04:f0:21:14:c6:5e": // Galileo 40
				name = "G40";
				break;
				
			case "f0:1f:af:0e:97:18": //Meshlab1
				name = "Meshlab1";
				break;
				
			case "":
				name = "Unknown";
			
			default: // unknown
				break;
		}
		
		return name;
	}
	
	
	public static String MACToIP(String MAC)
	{
        String ipAddr = null;

		switch (MAC)
		{
			case "04:f0:21:10:c3:23": // Galileo 1
				ipAddr = "192.168.123.10";
				break;
				
			case "04:f0:21:10:c3:20": // Galileo 2
				ipAddr = "192.168.123.11";
				break;
				
			case "04:f0:21:10:c8:0f": // Galileo 3
				ipAddr = "192.168.123.12";
				break;
				
			case "04:f0:21:10:c3:4f": // Galileo 4
				ipAddr = "192.168.123.13";
				break;
				
			case "04:f0:21:10:c2:90": // Galileo 5
				ipAddr = "192.168.123.14";
				break;
				
			case "04:f0:21:10:c2:8a": // Galileo 6
				ipAddr = "192.168.123.15";
				break;
				
			case "04:f0:21:10:c8:18": // Galileo 7
				ipAddr = "192.168.123.16";
				break;
				
			case "04:f0:21:10:c3:2b": // Galileo 8
				ipAddr = "192.168.123.17";
				break;
				
			case "04:f0:21:10:c8:15": // Galileo 9
				ipAddr = "192.168.123.18";
				break;
				
			case "04:f0:21:10:c3:57": // Galileo 10
				ipAddr = "192.168.123.19";
				break;
				
			case "04:f0:21:10:c3:41": // Galileo 11
				ipAddr = "192.168.123.20";
				break;
				
			case "04:f0:21:10:c3:71": // Galileo 12
				ipAddr = "192.168.123.21";
				break;
				
			case "04:f0:21:10:c3:2a": // Galileo 13
				ipAddr = "192.168.123.22";
				break;
				
			case "04:f0:21:10:c3:62": // Galileo 14
				ipAddr = "192.168.123.23";
				break;
				
			case "04:f0:21:10:c3:55": // Galileo 15
				ipAddr = "192.168.123.24";
				break;
				
			case "04:f0:21:10:c2:8e": // Galileo 16
				ipAddr = "192.168.123.25";
				break;
				
			case "04:f0:21:0f:77:54": // Galileo 17
				ipAddr = "192.168.123.26";
				break;
				
			case "04:f0:21:10:c3:39": // Galileo 18
				ipAddr = "192.168.123.27";
				break;
				
			case "04:f0:21:10:c3:33": // Galileo 19
				ipAddr = "192.168.123.28";
				break;
				
			case "04:f0:21:10:c5:af": // Galileo 20
				ipAddr = "192.168.123.29";
				break;
				
			case "04:f0:21:10:c3:3f": // Galileo 21
				ipAddr = "192.168.123.30";
				break;
				
			case "04:f0:21:10:c3:51": // Galileo 22
				ipAddr = "192.168.123.31";
				break;
				
			case "04:f0:21:10:c3:4c": // Galileo 23
				ipAddr = "192.168.123.32";
				break;
				
			case "04:f0:21:10:c2:88": // Galileo 24
				ipAddr = "192.168.123.33";
				break;
				
			case "04:f0:21:10:c8:0d": // Galileo 25
				ipAddr = "192.168.123.34";
				break;
				
			case "04:f0:21:10:c3:53": // Galileo 26
				ipAddr = "192.168.123.35";
				break;
				
			case "04:f0:21:10:c5:52": // Galileo 27
				ipAddr = "192.168.123.36";
				break;
				
			case "04:f0:21:10:c8:17": // Galileo 28
				ipAddr = "192.168.123.37";
				break;
				
			case "04:f0:21:10:c5:51": // Galileo 29
				ipAddr = "192.168.123.38";
				break;
				
			case "04:f0:21:10:c3:59": // Galileo 30
				ipAddr = "192.168.123.39";
				break;
				
			case "04:f0:21:10:c3:52": // Galileo 31
				ipAddr = "192.168.123.40";
				break;
				
			case "04:f0:21:10:c3:54": // Galileo 32
				ipAddr = "192.168.123.41";
				break;
				
			case "04:f0:21:10:c3:38": // Galileo 33
				ipAddr = "192.168.123.42";
				break;
				
			case "04:f0:21:10:c3:65": // Galileo 34
				ipAddr = "192.168.123.43";
				break;
				
			case "04:f0:21:10:c3:21": // Galileo 35
				ipAddr = "192.168.123.44";
				break;
				
			case "04:f0:21:10:c5:53": // Galileo 36
				ipAddr = "192.168.123.45";
				break;
				
			case "04:f0:21:10:c8:03": // Galileo 39
				ipAddr = "192.168.123.48";
				break;
				
			case "04:f0:21:14:c6:5e": // Galileo 40
				ipAddr = "192.168.123.49";
				break;
				
			case "f0:1f:af:0e:97:18": //Meshlab1
				ipAddr = "192.168.0.2";
				break;
			
			default: // unknown
				break;
		}
		
		return ipAddr;
	}
	
	
	public static String IPToMAC(String IP)
	{
        String MAC = new String(IP);

		switch (IP)
		{
			case "192.168.123.10": // Galileo 1
				MAC = "04:f0:21:10:c3:23";
				break;
				
			case "192.168.123.11": // Galileo 2
				MAC = "04:f0:21:10:c3:20";
				break;
				
			case "192.168.123.12": // Galileo 3
				MAC = "04:f0:21:10:c8:0f";
				break;
				
			case "192.168.123.13": // Galileo 4
				MAC = "04:f0:21:10:c3:4f";
				break;
				
			case "192.168.123.14": // Galileo 5
				MAC = "04:f0:21:10:c2:90";
				break;
				
			case "192.168.123.15": // Galileo 6
				MAC = "04:f0:21:10:c2:8a";
				break;
				
			case "192.168.123.16": // Galileo 7
				MAC = "04:f0:21:10:c8:18";
				break;
				
			case "192.168.123.17": // Galileo 8
				MAC = "04:f0:21:10:c3:2b";
				break;
				
			case "192.168.123.18": // Galileo 9
				MAC = "04:f0:21:10:c8:15";
				break;
				
			case "192.168.123.19": // Galileo 10
				MAC = "04:f0:21:10:c3:57";
				break;
				
			case "192.168.123.20": // Galileo 11
				MAC = "04:f0:21:10:c3:41";
				break;
				
			case "192.168.123.21": // Galileo 12
				MAC = "04:f0:21:10:c3:71";
				break;
				
			case "192.168.123.22": // Galileo 13
				MAC = "04:f0:21:10:c3:2a";
				break;
				
			case "192.168.123.23": // Galileo 14
				MAC = "04:f0:21:10:c3:62";
				break;
				
			case "192.168.123.24": // Galileo 15
				MAC = "04:f0:21:10:c3:55";
				break;
				
			case "192.168.123.25": // Galileo 16
				MAC = "04:f0:21:10:c2:8e";
				break;
				
			case "192.168.123.26": // Galileo 17
				MAC = "04:f0:21:0f:77:54";
				break;
				
			case "192.168.123.27": // Galileo 18
				MAC = "04:f0:21:10:c3:39";
				break;
				
			case "192.168.123.28": // Galileo 19
				MAC = "04:f0:21:10:c3:33";
				break;
				
			case "192.168.123.29": // Galileo 20
				MAC = "04:f0:21:10:c5:af";
				break;
				
			case "192.168.123.30": // Galileo 21
				MAC = "04:f0:21:10:c3:3f";
				break;
				
			case "192.168.123.31": // Galileo 22
				MAC = "04:f0:21:10:c3:51";
				break;
				
			case "192.168.123.32": // Galileo 23
				MAC = "04:f0:21:10:c3:4c";
				break;
				
			case "192.168.123.33": // Galileo 24
				MAC = "04:f0:21:10:c2:88";
				break;
				
			case "192.168.123.34": // Galileo 25
				MAC = "04:f0:21:10:c8:0d";
				break;
				
			case "192.168.123.35": // Galileo 26
				MAC = "04:f0:21:10:c3:53";
				break;
				
			case "192.168.123.36": // Galileo 27
				MAC = "04:f0:21:10:c5:52";
				break;
				
			case "192.168.123.37": // Galileo 28
				MAC = "04:f0:21:10:c8:17";
				break;
				
			case "192.168.123.38": // Galileo 29
				MAC = "04:f0:21:10:c5:51";
				break;
				
			case "192.168.123.39": // Galileo 30
				MAC = "04:f0:21:10:c3:59";
				break;
				
			case "192.168.123.40": // Galileo 31
				MAC = "04:f0:21:10:c3:52";
				break;
				
			case "192.168.123.41": // Galileo 32
				MAC = "04:f0:21:10:c3:54";
				break;
				
			case "192.168.123.42": // Galileo 33
				MAC = "04:f0:21:10:c3:38";
				break;
				
			case "192.168.123.43": // Galileo 34
				MAC = "04:f0:21:10:c3:65";
				break;
				
			case "192.168.123.44": // Galileo 35
				MAC = "04:f0:21:10:c3:21";
				break;
				
			case "192.168.123.45": // Galileo 36
				MAC = "04:f0:21:10:c5:53";
				break;
				
			case "192.168.123.48": // Galileo 39
				MAC = "04:f0:21:10:c8:03";
				break;
				
			case "192.168.123.49": // Galileo 40
				MAC = "04:f0:21:14:c6:5e";
				break;
				
			case "192.168.0.2": //Meshlab1
				MAC = "f0:1f:af:0e:97:18";
				break;
			
			default: // unknown
				break;
		}
		
		return MAC;
	}
	
	
	/*	TODO: ViPMesh address resolution
	public static String MACToIP(String MAC) {
		// Parse MAC to IP address for emulator/simulator
		
		String[] splitAddress = MAC.split(":");

		return new String(GlobalDefines.IF1_IPBODY + 
				Integer.toString(Integer.parseInt(splitAddress[3],16)) + "." + 
				Integer.toString(Integer.parseInt(splitAddress[4],16)));
		
	}
	public static String IPToMAC(String IP) {
		// Parse IP address to MAC for emulator/simulator
		
		String[] splitAdress = IP.split("\\.");
		
		return new String(GlobalDefines.IF1_MACBODY + 
				String.format("%02x", Integer.parseInt(splitAdress[2])) + ":" +
				String.format("%02x", Integer.parseInt(splitAdress[3])) + ":08");
	}
	*/
	
}
