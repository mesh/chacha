package Global;

import java.io.File;

public interface GlobalDefines
{
	
	// Define debug parameters (skip channel switch and/or monitoring phase, used in eval)
	public final static boolean SKIP_SETCHANNEL = false;
	public final static boolean SKIP_MONITORING = false;
	
	public final static boolean DEBUGGING = true;
	
	public final static boolean SEND_CHINFO_TO_TEST_PC = true;
	public final static boolean PERIODICAL_MSG_VIA_TCP = true;
	
	// Define path for "*.done"-files (tmpfs)
	public final static String DONE_FILE_PATH = "/run/";
	
	public final static String TEST_PC_ETH_MAC = "f0:1f:af:0e:97:18";
	public final static String EXP_NODE = "04:f0:21:14:c6:5e";
	
	// Define addresses
	public final static String IF1_IPBODY = "192.168.123.";				// use this for Mini-Mesh
	public final static String IF2_IPBODY = "192.168.124.";				// use this for Mini-Mesh
	
	public final static String IF1_ETH_IPBODY = "192.168.0.";			// use this for Mini-Mesh
	public final static String IF2_ETH_IPBODY = "192.168.124.";			// use this for Mini-Mesh
	//
//	public final static String IF1_MACBODY = "42:00:00:";				// TODO: reserved for ViPMesh addr translation
//	public final static String IF2_MACBODY = "42:XX:XX:";				// TODO: reserved for ViPMesh addr translation
//	public final static String IF1_IPBODY = "10.1.";					// TODO: reserved for ViPMesh addr translation
//	public final static String IF2_IPBODY = "10.3.";					// TODO: reserved for ViPMesh addr translation
	
	
	// Define memberships
	public final static String CLUSTERFREE = "CF";						// cluster free (CF)
	public final static String CLUSTERMEMBER = "CM";					// cluster member (CM)
	public final static String PROPOSED_CLUSTERHEAD = "PCH";			// proposed cluster head (PCH)
	public final static String CLUSTERHEAD = "CH";						// cluster head (CH)
	
	
	// Define channel numbers
	public final static int COMMON_CHANNEL = 149;						// common channel (CC) on IF1
	public final static int[] CHANNEL_SEQUENCE = {						// cluster channel pool
			36, 40, 44, 48, 153, 157 // , 161, 165
	};
	
	// Define channel frequencies
	public final static int COMMON_CHANNEL_FREQ = 5745;					// common channel (CC) freq on IF1
	public final static int[] CHANNEL_FREQ_SEQUENCE = {					// cluster channel freq pool
			5180, 5200, 5220, 5240, 5765, 5785 // , 5805, 5825
	};
	
	
	// Define periods and timeouts
	public final static int SYNC_BROADCAST_PERIOD = 500; 				// in milliseconds, time between sync broadcasts
	public final static int SYNC_THRESHOLD = 20; 						// threshold for consecutive sync messages sent without receiving one
	//
	public final static int NC_UNICAST_PERIOD = 5000; 					// in milliseconds, time between NC unicasts to neighbours
	//
	public final static int CH_BROADCAST_PERIOD = 5000; 				// in milliseconds, time between CH announcement broadcasts
	public final static int CH_NOTIFICATION_TIMEOUT = 10000; 			// in milliseconds TIM: evtl statt 2*...mit CH_BC_THRESH(4) 
	// -> grace time for CF in phase 0 to detect existing CH to join (time without "new" CH notifications)
	// -> grace time for CH in phase 3 to announce themselves to master CH
	//
	public final static int MASTER_PHASE_BROADCAST_PERIOD = 500;		// in milliseconds, time between master CH phase message broadcasts
	public final static int MASTER_PHASE_BROADCAST_TRIES = 20;			// number of broadcasts per phase sent by master CH
	//
	public final static long TIMESTAMP_RUNNING_OUT = 6000;
	//
	public final static int WAIT_X_ROAMING_CYCLES_BEFORE_REC_ISO = 1;
	//
	// master CH -> initial delay before sending broadcasts of
	// respective phase message @ MASTER_PHASE_BROADCAST_PERIOD:
	public final static int PHASE0_DELAY = 0; 							// in milliseconds	// TODO: currently unused
	public final static int PHASE1_DELAY = 0; 							// in milliseconds	// TODO: currently unused
	public final static int PHASE2_DELAY = 10000; 						// in milliseconds
	public final static int PHASE3_DELAY = 10000; 						// in milliseconds
	public final static int PHASE4_DELAY = 10000; 						// in milliseconds
	public final static int PHASE5_DELAY = 10000; 						// in milliseconds
	public final static int PHASE6_DELAY = 0; 							// in milliseconds	// TODO: currently unused
	public final static int PHASE7_DELAY = 0; 							// in milliseconds	// TODO: currently unused
	//
	public final static int MONITORING_DELAY = 20000; 					// in milliseconds, time before webclient thread starts in phase 7
	public final static int ROAMING_DELAY = 10000;						// in milliseconds, time to test roaming-functionality in phase 7 before exiting the program
	//
	public final static int UPDATE_THREAD_PERIOD = 10000;				// in milliseconds, time period for getting Mesh-Info 
	public final static int NEIGH_INF_THREAD_SLEEP = 10000;				// Thread sleeps ...sec before getting NeighbourInfo for the next time
	public final static int INITIAL_THREAD_SLEEP = 1000;				// wait for ...sec to let the nodes initialize their setup
	public final static int WAIT_MESHNODE_IS_FREE = 100;
	public final static long PINGPONG_TIMEOUT = 5000;
	public final static int ROAMING_THREAD_SLEEP = 5000;
	//
	public final static long ACK_TIMEOUT = 2000;
	
	
	// Define messages
	public final static String MESSAGE_SEPARATOR = "|";
	public final static String MAC_SEPARATOR = "MAC";
	public final static String CHK_SUM_SEPARATOR = "CHKSUM";
	//
	public final static String PHASE0_MESSAGE = "PHASE0";				// TODO: currently unused
	public final static String PHASE1_MESSAGE = "PHASE1";
	public final static String PHASE2_MESSAGE = "PHASE2";
	public final static String PHASE3_MESSAGE = "PHASE3";
	public final static String PHASE4_MESSAGE = "PHASE4";
	public final static String PHASE5_MESSAGE = "PHASE5";
	public final static String PHASE6_MESSAGE = "PHASE6";
	public final static String PHASE7_MESSAGE = "PHASE7";				// TODO: currently unused
	//
	public final static String SYNC_MESSAGE = "SYNC";					// carries metric "centrality": C = ALMmin / ALMmean
	public final static String SYNC_ABORT_MESSAGE = "SYNC_ABORT";		// TODO: is SYNC_ABORT_MESSAGE necessary?
	//
	public final static String NC_MESSAGE = "NC";						// carries metric "neighbour count": NC = # links
	public final static String WNPR_MESSAGE = "WNPR";					// carries metric "weighted neighbour to PCH neighbour ratio": WNPR = (NC / # PCH neighbours ) * centrality
	//
	public final static String PCH_MESSAGE = "PCH";
	//
	public final static String CH_MESSAGE = "CH";						// TODO: should carry current number of CM for future cluster balancing (0 before first join phase)
	//																	// TODO: should also carry channel (if already selected, 0 or CC otherwise)
	public final static String CLUSTERING_DONE = "CDONE";
	//
	public final static String ROAMING_FALSE_MESSAGE = "ROF";			// TODO: used to stop roaming of other nodes while roaming
	public final static String ROAMING_TRUE_MESSAGE = "ROT";			// TODO: used to reactivate roaming-functionality
	//
	public final static String MY_NEXT_HOP_TO_CH = "NXTHP";				// TODO: is used for balancing and isolation recognition
	//
	public final static String CH_LIST_MESSAGE = "CH_LIST";
	//
	public final static String LEAVE_REQUEST_MESSAGE = "LEAVEREQ";
	public final static String LEAVE_ACK_MESSAGE = "LEAVEACK";
	//
	public final static String JOIN_REQUEST_MESSAGE = "JOINREQ";
	public final static String JOIN_ACK_MESSAGE = "JOINACK";
	//
	public final static String JOIN_MESSAGE = "JOIN";					// TODO: announce own CH (decision) to neighbors, check this before join
	public final static String LEAVE_MESSAGE = "LEAVE";
	//
	public final static String PING_MESSAGE = "PING";
	public final static String PONG_MESSAGE = "PONG";
	//
	public final static String GAL40_NEXT_HOP_TO_CH = "GAL40NH2CH";
	//
	public final static String CHANNEL_SELECT_MESSAGE = "CHANNEL_SELECT";
	public final static String CHANNEL_SET_MESSAGE = "CHANNEL_SET";	
	
	
	// Define tool and system parameters
	public final static File toolPath = new File(System.getProperty("user.dir") + "/exec");
	//
	public final static int HWMP_ROOTMODE = 2;							// proactive PREQ with no PREP (activated by CH nodes)
	public final static int PLINK_MAX_INACTIVE_TIME = 10000;			// TODO: in milliseconds, max. inactive time of peer links to be considered
	//
	public final static int CENTRALITY_POWER = 1;						// alpha in WNPR = (NC / (( #PCH neighbours + 1) * NodeCount over all)) + (centrality * (1 / alpha)) 
																		// -> greater means fewer influence of the centrality-metric
																		// state from 17.05.2018
	
	
	// Define internal mesh interface configuration commands
	public final static String CHANNEL_COMMAND = "CHANNEL";
	public final static String ROOTMODE_COMMAND = "ROOTMODE";
	
	
	// Define communication parameters
	public final static int TCP_RECEIVE_PORT = 4710;
	public final static int UDP_RECEIVE_PORT = 4711;
	public final static int TCP_SEND_PORT = 0; // any free port
	public final static int UDP_SEND_PORT = 0; // any free port
	
	public final static int TCP_ETHERNET_RECEIVE_PORT = 39123;
	public final static int TCP_ETHERNET_SEND_PORT = 0;
	
	
}
