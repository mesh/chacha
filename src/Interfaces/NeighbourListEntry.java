package Interfaces;

public interface NeighbourListEntry {
	
	public String getMAC();
	public long getMACSum();
	
	public void setClusterheadMAC(String clusterheadMAC);
	public String getClusterheadMAC();
	
	public void setMembership(String membership);	
	public String getMembership();
	
	public void setNeighbourCount(int nc);
	public int getNeighbourCount();
	
	public void setWNPR(float wnpr);
	public float getWNPR();
	
}