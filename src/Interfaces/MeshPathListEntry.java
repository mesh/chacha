package Interfaces;

public interface MeshPathListEntry {

	// TODO: integrate path info flags (MPP indication, ...) ?
	
	public String getMeshPointLocalInterfaceName();
	
	public void setMeshPointLocalMacAddress(String mac);
	public String getMeshPointLocalMacAddress();
	
	public String getDestinationMacAddress();
	public String getNextHopMacAddress();
	public long getDestinationSequenceNumber();
	public long getMetric();
	public long getExpirationTime();
	
}
