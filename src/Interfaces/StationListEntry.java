package Interfaces;

public interface StationListEntry {

	public String getMeshPointLocalInterfaceName();
	
	public void setMeshPointLocalMacAddress(String mac);
	public String getMeshPointLocalMacAddress();
	
	public String getMeshPointRemoteMacAddress();
	public String getMeshPlinkState();
	public long getInactiveTime();
	public int getSignalStrengthAvg();
	public int getTxBitrate();
	public int getRxBitrate();
	public int getRxBytes();
	public int getTxBytes();
	public int getTxRetries();
	public int getTxFailed();
	public int getMeshLLID();
	public int getMeshPLID();
	
}
