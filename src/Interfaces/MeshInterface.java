package Interfaces;

import java.util.List;

public interface MeshInterface {
	
	public void setChannel(int channel);
	public int getChannel();
	
	public String getIFName();
	public String getMeshID();
	
	public String getMAC();
	public long getMACSum();
	public String getIP();
	
	public int getNeighbourCount();
	
	public List<List<String>> getMeshPeers();
	public List<List<String>> getMeshPaths();
	
	public void enableRootMode(boolean enable);
	
	public void unicast(String destMAC, String command, List<String> data);
	public void unicastEthernet(String destMAC, String command, List<String> data);
	public void broadcast(String message);
	
}
