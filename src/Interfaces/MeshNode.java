package Interfaces;

public interface MeshNode {
	
	public void changeLocalPhase();
	
	public void handleReceivedCommand(String senderAdress, String data);
	
	public boolean isFree(); // TODO: get rid of pseudo mutex "amIFree"
	
}
