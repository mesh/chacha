#!/bin/bash

# detect and export phyNames/devNames as env. var. (works for 1 and 2 phy)
source /home/root/phyNameDetectAndDevRename.sh

IF2_IP=$1
FREQ=$2

# TODO: antenna map not configurable for ralink chipset (driver rt2x00/rt2800usb, fw rt2870.bin)
#./iw phy ${PHYNAME_RAL} set antenna 1 1

./iw phy ${PHYNAME_RAL} set rts off # TODO: off / 500 (bytes)
./iw phy ${PHYNAME_RAL} set retry short 7 long 4 # TODO: short 7 long 4
./iw dev ${DEVNAME_RAL} set type mp
./ifconfig ${DEVNAME_RAL} up
./iw dev ${DEVNAME_RAL} set power_save off
./iw dev ${DEVNAME_RAL} set txpower fixed 100 # TODO: use min tx power of 1dBm
./iw dev ${DEVNAME_RAL} mesh leave

# TODO: MCS0 vs. MCS3 -> use rate that is achievable both by Atheros mPCIe and Ralink USB
./iw dev ${DEVNAME_RAL} mesh join mesh_IF2 freq $FREQ HT20 basic-rates 6 mcast-rate 6
./iw dev ${DEVNAME_RAL} set bitrates ht-mcs-5 0 sgi-5 # TODO: driver always uses short GI
#./iw dev ${DEVNAME_RAL} mesh join mesh_IF2 freq $FREQ HT20 basic-rates 24 mcast-rate 24
#./iw dev ${DEVNAME_RAL} set bitrates ht-mcs-5 3 sgi-5 # TODO: driver always uses short GI

./ifconfig ${DEVNAME_RAL} ${IF2_IP} netmask 255.255.255.0 broadcast 192.168.124.255
./iw dev ${DEVNAME_RAL} set mesh_param mesh_plink_timeout=20

