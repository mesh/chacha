#!/bin/bash

# runs on every CM, needs TestData file (fixed size random data) in path

# detect and export phyNames/devNames as env. var. (works for 1 and 2 phy)
source /home/root/phyNameDetectAndDevRename.sh

IF1_IP=$1
MULTI_INTERFACE=$2
MULTI_CHANNEL=$3
CLUSTER_FREQ=$4

if [[ "${MULTI_INTERFACE}" == true ]]; then
	if [ -n "${DEVNAME_RAL}" ]; then
		IF2_IP="${IF1_IP//.123./.124.}" # derive IF2 IP by replacing IF1 IP substring ".123." with ".124."
		./initIF2.sh ${IF2_IP} 5745 	# init IF2 on CC (149, 5745 MHz)
		# TODO: run blockMyPeerLinks on IF2
		# TODO: script arg: CSL (default) / ASUS / BUFFALO
		#nohup /home/root/blockMyPeerLinks_CHaChA_IF2.sh ASUS &>/dev/null &
	fi
fi

if [[ "${MULTI_CHANNEL}" == true ]]; then
	./setChannelIF1.sh ${IF1_IP} ${CLUSTER_FREQ}
else
	./setChannelIF1.sh ${IF1_IP} 5745
fi

./iw ${DEVNAME_ATH} set mesh_param mesh_hwmp_rootmode=0  # no root node

mkdir -p /run/logs
mkdir -p /run/data

cp TestData /run/data/

echo 3 > /proc/sys/vm/drop_caches

{ tcpserver -vXHlRD -b 50 ${IF1_IP} 8080 bash -c "cat /run/data/TestData" ; } 2>&1 | ts '[%Y-%m-%d %H:%M:%.S]' &>/run/logs/tcpserver_node_$(echo ${IF1_IP}).log &
