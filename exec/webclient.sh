#!/bin/bash

#set -x		# TODO: uncomment for debug

# expects args as follows: own IF1 IP, MULTI_INTERFACE, MULTI_CHANNEL, CLUSTER_FREQ, IF1 IPs of CMs, IF2 IPs of Master CH / "aux" CHs
# -> "IF1_IP" , "false" , "true" , "57xx" , "CM" , ... , ... , "CH" , ... , ...

# ------------------------------------------------------------------------------

RUNS=50						# TODO: target number of successful monitoring cycles
MAX_PARALLEL_DL=0 #0 ... 4	# TODO: max. parallel nc instances that download CM data -> 0=unlimited
MAX_PARALLEL_UL=4 #1 ... 4	# TODO: max. parallel nc instances that upload cluster data (#"aux" CH that Master CH accepts at a time)
SUCCESS_CNT=0				# current count of successful runs
TIMEOUT=60					# netcat TCP connect/idle timeout in seconds

# TODO: file size of TestData in bytes (stat -c%s "TestData", incl. HTTP header)
#TARGET_FILESIZE=25645 		#  25kB
#TARGET_FILESIZE=51242 		#  50kB
#TARGET_FILESIZE=102443		# 100kB
TARGET_FILESIZE=256043		# 250kB
#TARGET_FILESIZE=512043		# 500kB
#TARGET_FILESIZE=1048623 	#   1MB
#TARGET_FILESIZE=5242927 	#   5MB

# TODO: file size of TestData in kBytes (excl. HTTP header), used to generate TestDataCluster
#TARGET_FILESIZE_KB=25 		#  25kB
#TARGET_FILESIZE_KB=50 		#  50kB
#TARGET_FILESIZE_KB=100 	# 100kB
TARGET_FILESIZE_KB=250 		# 250kB
#TARGET_FILESIZE_KB=500 	# 500kB
#TARGET_FILESIZE_KB=1024 	#   1MB
#TARGET_FILESIZE_KB=5120 	#   5MB

# ------------------------------------------------------------------------------

# detect and export phyNames/devNames as env. var. (works for 1 and 2 phy)
source /home/root/phyNameDetectAndDevRename.sh

IF1_IP=$1
MULTI_INTERFACE=$2
MULTI_CHANNEL=$3
CLUSTER_FREQ=$4

CM_IPS=()
CH_IPS=()

startOfCM=false
startOfCH=false

for arg in $@; do

	if [[ "$arg" == "CM" ]] ; then
		startOfCM=true
		continue
	fi

	if [[ "$arg" == "CH" ]] ; then
		startOfCH=true
		startOfCM=false
		continue
	fi

	# read argv elements between "CM" and "CH" into array CM_IPS
	if [[ "$startOfCM" == true ]]; then
		CM_IPS+=($arg)
	fi

	# read argv elements after "CH" into array CH_IPS (own IP not included)
	if [[ "$startOfCH" == true ]]; then
		CH_IPS+=($arg)
	fi

done

if [[ "${MULTI_INTERFACE}" == true ]]; then
	# use IF2 IPs of CH if multiple IF are used
	CH_IPS_TMP=()
	for IP in ${CH_IPS[@]}; do
		IP_NEW="${IP//.123./.124.}"
		CH_IPS_TMP+=(${IP_NEW})
	done
	CH_IPS=("${CH_IPS_TMP[@]}")
fi

CH_CNT=${#CH_IPS[@]}

# ------------------------------------------------------------------------------

ETH_IP="${IF1_IP//.123./.0.}"

IF2_IP=""
if [[ "${MULTI_INTERFACE}" == true ]]; then
	if [ -n "${DEVNAME_RAL}" ]; then
		IF2_IP="${IF1_IP//.123./.124.}"
		./initIF2.sh ${IF2_IP} 5745 # init IF2 on CC (149, 5745 MHz)
		#./iw ${DEVNAME_RAL} set mesh_param mesh_hwmp_rootmode=0	# no root node
		./iw ${DEVNAME_RAL} set mesh_param mesh_hwmp_rootmode=2		# root node, proactive PREQ with no PREP
		# TODO: run blockMyPeerLinks on IF2
		# TODO: script arg: CSL (default) / ASUS / BUFFALO
		#nohup /home/root/blockMyPeerLinks_CHaChA_IF2.sh ASUS &>/dev/null &
	fi
else
	IF2_IP="${IF1_IP}"
fi

if [[ "${MULTI_CHANNEL}" == true ]]; then
	./setChannelIF1.sh ${IF1_IP} ${CLUSTER_FREQ}
else
	./setChannelIF1.sh ${IF1_IP} 5745
fi

#./iw ${DEVNAME_ATH} set mesh_param mesh_hwmp_rootmode=0	# no root node
./iw ${DEVNAME_ATH} set mesh_param mesh_hwmp_rootmode=2		# root node, proactive PREQ with no PREP
#./iw ${DEVNAME_ATH} set mesh_param mesh_hwmp_rootmode=3	# root node, proactive PREQ with PREP
#./iw ${DEVNAME_ATH} set mesh_param mesh_hwmp_rootmode=4	# root node, proactive RANN (heavy-weight! seems buggy)

# ------------------------------------------------------------------------------

mkdir -p /run/logs
mkdir -p /run/data

# "aux" CH: generate new file "TestDataCluster" (rand. payload) of size (#CM_IPS+1) * TARGET_FILESIZE
if (( ${CH_CNT} == 1 )); then
	CM_COUNT=${#CM_IPS[@]}
	CLUSTER_NODE_COUNT=$((1+${CM_COUNT}))
	TARGET_FILESIZE_CLUSTER=$((${CLUSTER_NODE_COUNT}*${TARGET_FILESIZE}))
	TARGET_FILESIZE_CLUSTER_KB=$((${CLUSTER_NODE_COUNT}*${TARGET_FILESIZE_KB}))
	echo -e 'HTTP/1.1 200 OK\r\nContent-Length:${TARGET_FILESIZE_CLUSTER}\r\n\r\n' > /run/data/TestDataCluster
	dd if=/dev/urandom bs=${TARGET_FILESIZE_CLUSTER_KB}K count=1 >> /run/data/TestDataCluster
fi

# "aux" CH: start webserver on Ethernet IF IP
# -> tcpserver reads from stdin ("aux" CH receive cycle start signal from Master)
# -> upon connection, tcpserver will kill dummy background job that the "aux" CH is waiting for -> cycle start condition
PID_CYCLE_START=""
if (( ${CH_CNT} == 1 )); then
	# start new "cycle start" dummy background job and the tcpserver instance that will kill it
	{ tail -f /dev/null ; } &
	PID_CYCLE_START=$!
	{ tcpserver -vXHlRD -c 1 -b 50 ${ETH_IP} 8080 pkill tail ; } 2>&1 | ts '[%Y-%m-%d %H:%M:%.S]' &>/run/logs/tcpserver_CH_$(echo ${ETH_IP}).log &
fi

# Master CH: start webserver on IF2 IP
# -> tcpserver reads from stdin (Master CH receives TestDataCluster from remote "aux" CH)
# "MAX_PARALLEL_UL" limits the number of concurrent "aux" CH upload connections
if (( ${CH_CNT} == 4 )); then
	{ tcpserver -vXHlRD -c ${MAX_PARALLEL_UL} -b 50 ${IF2_IP} 8080 bash -c "cat >/dev/null" ; } 2>&1 | ts '[%Y-%m-%d %H:%M:%.S]' &>/run/logs/tcpserver_CH_$(echo ${IF2_IP}).log &
fi

# remove any previous 'monitoring.done' file
rm /run/monitoring.done 2>/dev/null

sleep 10	# TODO

i=1

while (( ${SUCCESS_CNT} < $RUNS )); do

	echo 3 > /proc/sys/vm/drop_caches

	mkdir -p /run/logs/Run_$(echo $i)

	SUCCESS=true	# sucess of 1st stage (cycle start signalling + CM data retrieval)

	# "aux" CH: wait until Master CH has signaled cycle start
	# -> actually wait on dummy background job PID
	# -> will be killed by tcpserver on incoming Ethernet connection
	if (( ${CH_CNT} == 1 )); then
		wait ${PID_CYCLE_START}
	fi

	# Master CH: signal cycle start to remote "aux" CH
	if (( ${CH_CNT} == 4 )); then

		sleep 10	# TODO

		# nc connect to "aux" CHs' tcpserver (listening on Ethernet IF)
		# -> this triggers sending of a signal to the remote side's webclient script
		# -> signal handler in the script sets cycle start condition to true
		if [[ "${MULTI_INTERFACE}" == true ]]; then
			PATTERN=".124."	# CH_IPS was changed to contain IF2 IPs
		else
			PATTERN=".123." # CH_IPS still contains IF1 IPs as they were passed
		fi
		for IP in ${CH_IPS[@]}; do ETH_IP_TMP="${IP//$PATTERN/.0.}"; echo ${ETH_IP_TMP}; done | xargs -I{} bash -c "
		echo -n 1 | nc -vv -n -q 0 -w $TIMEOUT {} 8080 &>/dev/null;"
		status=$?
		SUCCESS=$SUCCESS
		if (($status > 0)); then
			SUCCESS=false
		fi

	fi

	# take absolute timestamp (date) before CM data retrieval
	TIMESTAMP1=$(date +%Y%m%d_%H-%M-%S-%3N)

	# perform concurrent CM data retrieval using xargs with max-procs limit
	# TODO: per job timestamp of netcat start
	for IP in ${CM_IPS[@]}; do echo $IP; done | xargs -I{} --max-procs ${MAX_PARALLEL_DL} bash -c "
	{ time nc -vv -n -q 0 -w $TIMEOUT {} 8080 &>/dev/null ; } 2>/run/logs/Run_$(echo $i)/time_netcat_node_{}.log;"
	status=$?
	SUCCESS=$SUCCESS
	if (($status > 0)); then
		SUCCESS=false
	fi

	# take absolute timestamp (date) after CM data retrieval
	TIMESTAMP2=$(date +%Y%m%d_%H-%M-%S-%3N)

	if [[ "$SUCCESS" == true ]]; then

		SUCCESS_CNT=$((${SUCCESS_CNT}+1))

		# "aux" CH: start sync of this run's CM data (TestDataCluster) to Master CH
		# TODO: interleaving CM data retrieval and cluster data upload to Master CH in multi-channel case
		# -> in general, upload to Master CH runs on 2nd IF (common channel), independent from cluster traffic
		# -> basically, we could just proceed with the subsequent monitoring run on the cluster IF in parallel
		# -> however, subsequent cycle times would depend on each other and slow/fast CH (dep. on #CM) would continously diverge
		# -> therefore, we start new monitoring cycles synchronously on all CH after Master CH has all cluster info
		# -> this greatly simplifies practical realization and determination of the cycle time, subsequent cycles are independent
		# -> even without interleaving, "slow" CH (still downloading) and "quick" CH (already uploading) can benefit within a cycle
		if (( ${CH_CNT} == 1 )); then

			# TODO: MULTI_INTERFACE == false && MULTI_CHANNEL == true -> IF1 channel switch to CC (CLUSTER_FREQ of Master CH)
			if [[ "${MULTI_INTERFACE}" == false && "${MULTI_CHANNEL}" == true ]]; then
				./setChannelIF1.sh ${IF1_IP} 5745
				./iw ${DEVNAME_ATH} set mesh_param mesh_hwmp_rootmode=2
			fi

			# if Master CH accepts only a limited number of concurrent connections, repeat upload attempt on each TCP connection reset
			# TODO: only try for a max. number of times; Master CH might have failed in CM data retrieval and did not proceed to 2nd stage
			while true; do

				# take absolute timestamp (date) before upload to Master CH
				TIMESTAMP3=$(date +%Y%m%d_%H-%M-%S-%3N)

				# start netcat job to upload our cluster data to Master CH
				# -> read TestDataCluster to netcat stdin, get duration with time command
				# TODO: effect of TIMEOUT on connection attempts that are reset by Master CH due to limited MAX_PARALLEL_UL?
				{ time nc -vv -n -q 0 -w $TIMEOUT $(echo ${CH_IPS[0]}) 8080 < /run/data/TestDataCluster &>/dev/null ; } 2>/run/logs/Run_$(echo $i)/time_netcat_sync_CH_$(echo ${CH_IPS[0]}).log &

				# wait until background job returns
				pid=$!
				wait $pid
				
				# only continue on success, else try again
				status=$?
				if (($status == 0)); then
					break
				fi

				sleep 1		# TODO

			done

			# take absolute timestamp (date) after upload to Master CH
			TIMESTAMP4=$(date +%Y%m%d_%H-%M-%S-%3N)

			# create timestamp files
			touch /run/logs/Run_$(echo $i)/timestamp_1_startClusterDataRetrieve_$(echo $TIMESTAMP1)
			touch /run/logs/Run_$(echo $i)/timestamp_2_endClusterDataRetrieve_$(echo $TIMESTAMP2)
			touch /run/logs/Run_$(echo $i)/timestamp_3_startClusterDataUpload_$(echo $TIMESTAMP3)
			touch /run/logs/Run_$(echo $i)/timestamp_4_endClusterDataUpload_$(echo $TIMESTAMP4)

			# store copy of tcpserver tmp log for this run and clear it for the next run
			pkill tcpserver
			cp /run/logs/tcpserver_CH_$(echo ${ETH_IP}).log /run/logs/Run_$(echo $i)/
			rm /run/logs/tcpserver_CH_$(echo ${ETH_IP}).log

			# start new "cycle start" dummy background job and the tcpserver instance that will kill it
			{ tail -f /dev/null ; } &
			PID_CYCLE_START=$!
			{ tcpserver -vXHlRD -b 50 ${ETH_IP} 8080 pkill tail ; } 2>&1 | ts '[%Y-%m-%d %H:%M:%.S]' &>/run/logs/tcpserver_CH_$(echo ${ETH_IP}).log &

			# TODO: MULTI_INTERFACE == false && MULTI_CHANNEL == true -> IF1 channel switch back to CLUSTER_FREQ
			if [[ "${MULTI_INTERFACE}" == false && "${MULTI_CHANNEL}" == true ]]; then
				./setChannelIF1.sh ${IF1_IP} ${CLUSTER_FREQ}
				./iw ${DEVNAME_ATH} set mesh_param mesh_hwmp_rootmode=2
			fi

		fi

		# Master CH: wait until we received all remote CH's cluster info ...
		# TODO: Master CH -> only wait up to [2*TIMEOUT + X] seconds and handle errors properly
		if (( ${CH_CNT} == 4 )); then

			sleep 20	# TODO

			# -> continuously check tcpserver log file for CH_CNT times occurence of "status 0"
			# -> this tells us that all remote CH have connected and finished uploading successfully
			DONE=false
			CNT=0
			while [[ "$DONE" == false ]]; do

				sleep 10	# TODO

				CNT=$(grep -c 'status 0' /run/logs/tcpserver_CH_$(echo ${IF2_IP}).log)

				if (( $CNT == ${CH_CNT} )); then
					DONE=true
				fi

			done

			# create timestamp files
			touch /run/logs/Run_$(echo $i)/timestamp_1_startClusterDataRetrieve_$(echo $TIMESTAMP1)
			touch /run/logs/Run_$(echo $i)/timestamp_2_endClusterDataRetrieve_$(echo $TIMESTAMP2)

			# store copy of tcpserver tmp log for this run and clear it for the next run
			pkill tcpserver
			cp /run/logs/tcpserver_CH_$(echo ${IF2_IP}).log /run/logs/Run_$(echo $i)/
			rm /run/logs/tcpserver_CH_$(echo ${IF2_IP}).log
			{ tcpserver -vXHlRD -c ${MAX_PARALLEL_UL} -b 50 ${IF2_IP} 8080 bash -c "cat >/dev/null" ; } 2>&1 | ts '[%Y-%m-%d %H:%M:%.S]' &>/run/logs/tcpserver_CH_$(echo ${IF2_IP}).log &

		fi

		# handle cleanup and signaling to measurement control PC after last run
		if (( ${SUCCESS_CNT} == $RUNS )); then

			# all CH: kill detached tcpserver
			pkill tcpserver

			# "aux" CH:
			# -> remove tcpserver tmp log
			# -> delete file "TestDataCluster"
			# -> kill dummy background job
			if (( ${CH_CNT} == 1 )); then
				rm /run/logs/tcpserver_CH_$(echo ${ETH_IP}).log 2>/dev/null
				rm /run/data/TestDataCluster 2>/dev/null
				pkill tail
			fi

			# Master CH:
			# -> remove tcpserver tmp log
			# -> write "monitoring.done" file for measurement control
			if (( ${CH_CNT} == 4 )); then
				rm /run/logs/tcpserver_CH_$(echo ${IF2_IP}).log 2>/dev/null
				touch /run/monitoring.done
			fi

		fi

	else

		# TODO: properly handle errors in per cluster CM data retrieval
		#
		# error on "aux" CH:
		# -> other "aux" CH might have been successful in the same run
		# -> decision for syncing to Master CH would diverge
		# -> Master CH would get stuck waiting for our cluster info
		#
		# error on Master CH:
		# -> "aux" CH might have been successful in the same run
		# -> they would try to upload their data to us but we are not waiting

		# -> "aux" CH
		if (( ${CH_CNT} == 1 )); then

			# create timestamp files
			touch /run/logs/Run_$(echo $i)/timestamp_1_startClusterDataRetrieve_$(echo $TIMESTAMP1)
			touch /run/logs/Run_$(echo $i)/timestamp_2_endClusterDataRetrieve_$(echo $TIMESTAMP2)
			touch /run/logs/Run_$(echo $i)/timestamp_3_startClusterDataUpload_$(echo $TIMESTAMP3)
			touch /run/logs/Run_$(echo $i)/timestamp_4_endClusterDataUpload_$(echo $TIMESTAMP4)

			# store copy of tcpserver tmp log for this run and clear it for the next run
			pkill tcpserver
			cp /run/logs/tcpserver_CH_$(echo ${ETH_IP}).log /run/logs/Run_$(echo $i)/
			rm /run/logs/tcpserver_CH_$(echo ${ETH_IP}).log

			# start new "cycle start" dummy background job and the tcpserver instance that will kill it
			{ tail -f /dev/null ; } &
			PID_CYCLE_START=$!
			{ tcpserver -vXHlRD -b 50 ${ETH_IP} 8080 pkill tail ; } 2>&1 | ts '[%Y-%m-%d %H:%M:%.S]' &>/run/logs/tcpserver_CH_$(echo ${ETH_IP}).log &

		fi
		# -> Master CH
		if (( ${CH_CNT} == 4 )); then

			# create timestamp files
			touch /run/logs/Run_$(echo $i)/timestamp_1_startClusterDataRetrieve_$(echo $TIMESTAMP1)
			touch /run/logs/Run_$(echo $i)/timestamp_2_endClusterDataRetrieve_$(echo $TIMESTAMP2)

			# store copy of tcpserver tmp log for this run and clear it for the next run
			pkill tcpserver
			cp /run/logs/tcpserver_CH_$(echo ${IF2_IP}).log /run/logs/Run_$(echo $i)/
			rm /run/logs/tcpserver_CH_$(echo ${IF2_IP}).log
			{ tcpserver -vXHlRD -c ${MAX_PARALLEL_UL} -b 50 ${IF2_IP} 8080 bash -c "cat >/dev/null" ; } 2>&1 | ts '[%Y-%m-%d %H:%M:%.S]' &>/run/logs/tcpserver_CH_$(echo ${IF2_IP}).log &

		fi

		# rename log folder of unsuccessful run to denote an error
		mv /run/logs/Run_$(echo $i) /run/logs/Run_$(echo $i)_ERROR

	fi

	i=$(($i+1))

done

